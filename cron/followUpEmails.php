<?php

// to be called from cron

require_once __DIR__ . '/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Database\FollowUpEmailDb;

// set default timezone
date_default_timezone_set('America/Denver');

$app = new Silex\Application();

$app->register(new DerAlex\Silex\YamlConfigServiceProvider(__DIR__ . '/../config/config.yaml'));
$app->register(new Silex\Provider\SwiftmailerServiceProvider());
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => $app['config']['database']
));

$app->match('/', function(Request $request) use ($app) {
            followUpEmails($app, $request);
            return new Response();
        });

function followUpEmails($request)
{
    global $app;

    $tablename = FollowUpEmailDb::$tablename;
    
    $grownUpEmails = getGrownUpEmails();

    $subj = $app['config']['followUpEmails']['subject'];
    $from = $app['config']['followUpEmails']['from'];
    $text = $app['config']['followUpEmails']['text'];
    
    if (strtolower($app['config']['followUpEmails']['dev_mode']) === 'yes') {
        foreach ($grownUpEmails as $k => $v) {
            if ($app['config']['followUpEmails']['email_for_testing'] === $v['email']) {
                $message = \Swift_Message::newInstance()
                        ->setSubject($subj)
                        ->setFrom(array($from))
                        ->setTo(array($v['email']))
                        ->setBody($text, 'text/html');
                $app['mailer']->send($message);
            }
            $app['db']->exec("DELETE FROM {$tablename} WHERE id = {$v['id']};");
        }
    } else {
        foreach ($grownUpEmails as $k => $v) {
            $message = \Swift_Message::newInstance()
                    ->setSubject($subj)
                    ->setFrom(array($from))
                    ->setTo(array($v['email']))
                    ->setBody($text, 'text/html');
            $app['mailer']->send($message);
            $app['db']->exec("DELETE FROM {$tablename} WHERE id = {$v['id']};");
        }
    }
}

/**
 * get emails that have last_visit older than some defined time
 */
function getGrownUpEmails()
{
    global $app;

    $tablename = FollowUpEmailDb::$tablename;
    $num_of_rows = $app['config']['followUpEmails']['num_of_batch_emails'];

    $sql = <<<SQL
SELECT * FROM {$tablename}
WHERE last_visit < DATE_ADD(NOW(), INTERVAL -1 MONTH)
LIMIT 0, {$num_of_rows};
SQL;

    return $app['db']->fetchAll($sql);
}

$app->run(Request::create('/'));
