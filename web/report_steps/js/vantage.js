/*
 * mustache.js - Logic-less {{mustache}} templates with JavaScript
 * http://github.com/janl/mustache.js
 */

(function (a, b) {
    if (typeof exports === "object" && exports) {
        module.exports = b
    } else {
        if (typeof define === "function" && define.amd) {
            define(b)
        } else {
            a.Mustache = b
        }
    }
}(this, (function () {
    var w = {};
    w.name = "mustache.js";
    w.version = "0.7.2";
    w.tags = ["{{", "}}"];
    w.Scanner = u;
    w.Context = s;
    w.Writer = q;
    var d = /\s*/;
    var l = /\s+/;
    var h = /\S/;
    var g = /\s*=/;
    var n = /\s*\}/;
    var t = /#|\^|\/|>|\{|&|=|!/;
    var j = RegExp.prototype.test;
    var v = Object.prototype.toString;

    function o(z, y) {
        return j.call(z, y)
    }
    function f(y) {
        return !o(h, y)
    }
    var k = Array.isArray || function (y) {
            return v.call(y) === "[object Array]"
        };

    function e(y) {
        return y.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&")
    }
    var c = {
        "&": "&amp;",
        "<": "&lt;",
        ">": "&gt;",
        '"': "&quot;",
        "'": "&#39;",
        "/": "&#x2F;"
    };

    function m(y) {
        return String(y).replace(/[&<>"'\/]/g, function (z) {
            return c[z]
        })
    }
    w.escape = m;

    function u(y) {
        this.string = y;
        this.tail = y;
        this.pos = 0
    }
    u.prototype.eos = function () {
        return this.tail === ""
    };
    u.prototype.scan = function (z) {
        var y = this.tail.match(z);
        if (y && y.index === 0) {
            this.tail = this.tail.substring(y[0].length);
            this.pos += y[0].length;
            return y[0]
        }
        return ""
    };
    u.prototype.scanUntil = function (z) {
        var y, A = this.tail.search(z);
        switch (A) {
            case -1:
                y = this.tail;
                this.pos += this.tail.length;
                this.tail = "";
                break;
            case 0:
                y = "";
                break;
            default:
                y = this.tail.substring(0, A);
                this.tail = this.tail.substring(A);
                this.pos += A
        }
        return y
    };

    function s(y, z) {
        this.view = y;
        this.parent = z;
        this._cache = {}
    }
    s.make = function (y) {
        return (y instanceof s) ? y : new s(y)
    };
    s.prototype.push = function (y) {
        return new s(y, this)
    };
    s.prototype.lookup = function (y) {
        var B = this._cache[y];
        if (!B) {
            if (y == ".") {
                B = this.view
            } else {
                var A = this;
                while (A) {
                    if (y.indexOf(".") > 0) {
                        B = A.view;
                        var C = y.split("."),
                            z = 0;
                        while (B && z < C.length) {
                            B = B[C[z++]]
                        }
                    } else {
                        B = A.view[y]
                    }
                    if (B != null) {
                        break
                    }
                    A = A.parent
                }
            }
            this._cache[y] = B
        }
        if (typeof B === "function") {
            B = B.call(this.view)
        }
        return B
    };

    function q() {
        this.clearCache()
    }
    q.prototype.clearCache = function () {
        this._cache = {};
        this._partialCache = {}
    };
    q.prototype.compile = function (A, y) {
        var z = this._cache[A];
        if (!z) {
            var B = w.parse(A, y);
            z = this._cache[A] = this.compileTokens(B, A)
        }
        return z
    };
    q.prototype.compilePartial = function (z, B, y) {
        var A = this.compile(B, y);
        this._partialCache[z] = A;
        return A
    };
    q.prototype.getPartial = function (y) {
        if (!(y in this._partialCache) && this._loadPartial) {
            this.compilePartial(y, this._loadPartial(y))
        }
        return this._partialCache[y]
    };
    q.prototype.compileTokens = function (A, z) {
        var y = this;
        return function (B, D) {
            if (D) {
                if (typeof D === "function") {
                    y._loadPartial = D
                } else {
                    for (var C in D) {
                        y.compilePartial(C, D[C])
                    }
                }
            }
            return p(A, y, s.make(B), z)
        }
    };
    q.prototype.render = function (A, y, z) {
        return this.compile(A)(y, z)
    };

    function p(F, z, y, I) {
        var C = "";
        var A, G, H;
        for (var D = 0, E = F.length; D < E; ++D) {
            A = F[D];
            G = A[1];
            switch (A[0]) {
                case "#":
                    H = y.lookup(G);
                    if (typeof H === "object") {
                        if (k(H)) {
                            for (var B = 0, K = H.length;
                            B < K; ++B) {
                                C += p(A[4], z, y.push(H[B]), I)
                            }
                        } else {
                            if (H) {
                                C += p(A[4], z, y.push(H), I)
                            }
                        }
                    } else {
                        if (typeof H === "function") {
                            var J = I == null ? null : I.slice(A[3], A[5]);
                            H = H.call(y.view, J, function (L) {
                                return z.render(L, y)
                            });
                            if (H != null) {
                                C += H
                            }
                        } else {
                            if (H) {
                                C += p(A[4], z, y, I)
                            }
                        }
                    }
                    break;
                case "^":
                    H = y.lookup(G);
                    if (!H || (k(H) && H.length === 0)) {
                        C += p(A[4], z, y, I)
                    }
                    break;
                case ">":
                    H = z.getPartial(G);
                    if (typeof H === "function") {
                        C += H(y)
                    }
                    break;
                case "&":
                    H = y.lookup(G);
                    if (H != null) {
                        C += H
                    }
                    break;
                case "name":
                    H = y.lookup(G);
                    if (H != null) {
                        C += w.escape(H)
                    }
                    break;
                case "text":
                    C += G;
                    break
            }
        }
        return C
    }
    function x(E) {
        var z = [];
        var D = z;
        var F = [];
        var B;
        for (var A = 0, y = E.length;
        A < y; ++A) {
            B = E[A];
            switch (B[0]) {
                case "#":
                case "^":
                    F.push(B);
                    D.push(B);
                    D = B[4] = [];
                    break;
                case "/":
                    var C = F.pop();
                    C[5] = B[2];
                    D = F.length > 0 ? F[F.length - 1][4] : z;
                    break;
                default:
                    D.push(B)
            }
        }
        return z
    }
    function a(D) {
        var A = [];
        var C, z;
        for (var B = 0, y = D.length; B < y; ++B) {
            C = D[B];
            if (C) {
                if (C[0] === "text" && z && z[0] === "text") {
                    z[1] += C[1];
                    z[3] = C[3]
                } else {
                    z = C;
                    A.push(C)
                }
            }
        }
        return A
    }
    function r(y) {
        return [new RegExp(e(y[0]) + "\\s*"), new RegExp("\\s*" + e(y[1]))]
    }
    w.parse = function (O, E) {
        O = O || "";
        E = E || w.tags;
        if (typeof E === "string") {
            E = E.split(l)
        }
        if (E.length !== 2) {
            throw new Error("Invalid tags: " + E.join(", "))
        }
        var I = r(E);
        var A = new u(O);
        var G = [];
        var F = [];
        var D = [];
        var P = false;
        var N = false;

        function M() {
            if (P && !N) {
                while (D.length) {
                    delete F[D.pop()]
                }
            } else {
                D = []
            }
            P = false;
            N = false
        }
        var B, z, H, J, C;
        while (!A.eos()) {
            B = A.pos;
            H = A.scanUntil(I[0]);
            if (H) {
                for (var K = 0, L = H.length; K < L; ++K) {
                    J = H.charAt(K);
                    if (f(J)) {
                        D.push(F.length)
                    } else {
                        N = true
                    }
                    F.push(["text", J, B, B + 1]);
                    B += 1;
                    if (J == "\n") {
                        M()
                    }
                }
            }
            if (!A.scan(I[0])) {
                break
            }
            P = true;
            z = A.scan(t) || "name";
            A.scan(d);
            if (z === "=") {
                H = A.scanUntil(g);
                A.scan(g);
                A.scanUntil(I[1])
            } else {
                if (z === "{") {
                    H = A.scanUntil(new RegExp("\\s*" + e("}" + E[1])));
                    A.scan(n);
                    A.scanUntil(I[1]);
                    z = "&"
                } else {
                    H = A.scanUntil(I[1])
                }
            }
            if (!A.scan(I[1])) {
                throw new Error("Unclosed tag at " + A.pos)
            }
            C = [z, H, B, A.pos];
            F.push(C);
            if (z === "#" || z === "^") {
                G.push(C)
            } else {
                if (z === "/") {
                    if (G.length === 0) {
                        throw new Error('Unopened section "' + H + '" at ' + B)
                    }
                    var y = G.pop();
                    if (y[1] !== H) {
                        throw new Error('Unclosed section "' + y[1] + '" at ' + B)
                    }
                } else {
                    if (z === "name" || z === "{" || z === "&") {
                        N = true
                    } else {
                        if (z === "=") {
                            E = H.split(l);
                            if (E.length !== 2) {
                                throw new Error("Invalid tags at " + B + ": " + E.join(", "))
                            }
                            I = r(E)
                        }
                    }
                }
            }
        }
        var y = G.pop();
        if (y) {
            throw new Error('Unclosed section "' + y[1] + '" at ' + A.pos)
        }
        F = a(F);
        return x(F)
    };
    var b = new q();
    w.clearCache = function () {
        return b.clearCache()
    };
    w.compile = function (z, y) {
        return b.compile(z, y)
    };
    w.compilePartial = function (z, A, y) {
        return b.compilePartial(z, A, y)
    };
    w.compileTokens = function (z, y) {
        return b.compileTokens(z, y)
    };
    w.render = function (A, y, z) {
        return b.render(A, y, z)
    };
    w.to_html = function (B, z, A, C) {
        var y = w.render(B, z, A);
        if (typeof C === "function") {
            C(y)
        } else {
            return y
        }
    };
    return w
}())));

Array.prototype.clean = function (b) {
    var a = 0;
    while (a < this.length) {
        if (this[a] == b) {
            this.splice(a, 1);
            a--
        }
        a++
    }
    return this
};
if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (c, d) {
        for (var b = (d || 0), a = this.length;
        b < a; b++) {
            if (this[b] === c) {
                return b
            }
        }
        return -1
    }
}
VM = window.VM || {};
VM.namespace = function (d) {
    var c = d.split("."),
        b = VM,
        a;
    if (c[0] === "VM") {
        c = c.slice(1)
    }
    for (a = 0; a < c.length;
    a += 1) {
        if (b[c[a]] === undefined) {
            b[c[a]] = {}
        }
        b = b[c[a]]
    }
    return b
};
VM.namespace("util");
VM.Utils = VM.Utils || (function () {
    var h = {}, k = Object.prototype.toString,
        r = '<style>#fountainG{position:relative;width:150px;height:18px;margin: 50px auto 25px;}#fountainG span{position:absolute;left: 60px;bottom: -30px;}.fountainG{position:absolute;top:0;background-color:#999999;width:19px;height:19px;-moz-animation-name:bounce_fountainG;-moz-animation-duration:1.3s;-moz-animation-iteration-count:infinite;-moz-animation-direction:linear;-moz-transform:scale(.3);-moz-border-radius:12px;-webkit-animation-name:bounce_fountainG;-webkit-animation-duration:1.3s;-webkit-animation-iteration-count:infinite;-webkit-animation-direction:linear;-webkit-transform:scale(.3);-webkit-border-radius:12px;-ms-animation-name:bounce_fountainG;-ms-animation-duration:1.3s;-ms-animation-iteration-count:infinite;-ms-animation-direction:linear;-ms-transform:scale(.3);-ms-border-radius:12px;-o-animation-name:bounce_fountainG;-o-animation-duration:1.3s;-o-animation-iteration-count:infinite;-o-animation-direction:linear;-o-transform:scale(.3);-o-border-radius:12px;animation-name:bounce_fountainG;animation-duration:1.3s;animation-iteration-count:infinite;animation-direction:linear;transform:scale(.3);border-radius:12px;}#fountainG_1{left:0;-moz-animation-delay:0.52s;-webkit-animation-delay:0.52s;-ms-animation-delay:0.52s;-o-animation-delay:0.52s;animation-delay:0.52s;}#fountainG_2{left:19px;-moz-animation-delay:0.65s;-webkit-animation-delay:0.65s;-ms-animation-delay:0.65s;-o-animation-delay:0.65s;animation-delay:0.65s;}#fountainG_3{left:38px;-moz-animation-delay:0.78s;-webkit-animation-delay:0.78s;-ms-animation-delay:0.78s;-o-animation-delay:0.78s;animation-delay:0.78s;}#fountainG_4{left:56px;-moz-animation-delay:0.91s;-webkit-animation-delay:0.91s;-ms-animation-delay:0.91s;-o-animation-delay:0.91s;animation-delay:0.91s;}#fountainG_5{left:75px;-moz-animation-delay:1.04s;-webkit-animation-delay:1.04s;-ms-animation-delay:1.04s;-o-animation-delay:1.04s;animation-delay:1.04s;}#fountainG_6{left:94px;-moz-animation-delay:1.17s;-webkit-animation-delay:1.17s;-ms-animation-delay:1.17s;-o-animation-delay:1.17s;animation-delay:1.17s;}#fountainG_7{left:113px;-moz-animation-delay:1.3s;-webkit-animation-delay:1.3s;-ms-animation-delay:1.3s;-o-animation-delay:1.3s;animation-delay:1.3s;}#fountainG_8{left:131px;-moz-animation-delay:1.43s;-webkit-animation-delay:1.43s;-ms-animation-delay:1.43s;-o-animation-delay:1.43s;animation-delay:1.43s;}@-moz-keyframes bounce_fountainG{0%{-moz-transform:scale(1);background-color:#999999;}100%{-moz-transform:scale(.3);background-color:#FFFFFF;}}@-webkit-keyframes bounce_fountainG{0%{-webkit-transform:scale(1);background-color:#999999;}100%{-webkit-transform:scale(.3);background-color:#FFFFFF;}}@-ms-keyframes bounce_fountainG{0%{-ms-transform:scale(1);background-color:#999999;}100%{-ms-transform:scale(.3);background-color:#FFFFFF;}}@-o-keyframes bounce_fountainG{0%{-o-transform:scale(1);background-color:#999999;}100%{-o-transform:scale(.3);background-color:#FFFFFF;}}@keyframes bounce_fountainG{0%{transform:scale(1);background-color:#999999;}100%{transform:scale(.3);background-color:#FFFFFF;}}</style><div id="fountainG"><div id="fountainG_1" class="fountainG"></div><div id="fountainG_2" class="fountainG"></div><div id="fountainG_3" class="fountainG"></div><div id="fountainG_4" class="fountainG"></div><div id="fountainG_5" class="fountainG"></div><div id="fountainG_6" class="fountainG"></div><div id="fountainG_7" class="fountainG"></div><div id="fountainG_8" class="fountainG"></div></div><p style="color: #CCCCCC;font-family: arial; margin: 0; padding: 0; text-align: center; ">Loading...</p>',
        w = 0,
        z = 1,
        q = 10,
        n = function (K, J) {
            for (var I in J) {
                try {
                    if (J[I].constructor == Object) {
                        K[I] = n(K[I], J[I])
                    } else {
                        K[I] = J[I]
                    }
                } catch (H) {
                    K[I] = J[I]
                }
            }
            return K
        }, d = function (J) {
            var H = "";
            J = J.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var I = "[\\?&]" + J + "=([^&#]*)",
                L = new RegExp(I, "i"),
                K = L.exec(window.location.href);
            if (K !== null) {
                H = K[1]
            }
            return H
        }, A = function (J) {
            var H = 0,
                I;
            for (I in J) {
                if (J.hasOwnProperty(I)) {
                    H++
                }
            }
            return H
        }, e = function (H) {
            return l(H) === "function"
        }, l = function (H) {
            return H == null ? String(H) : h[k.call(H)] || "object"
        }, c = function (M, N, J) {
            var I, K = 0,
                L = (typeof M == "undefined") ? {}.length : M.length,
                H = L === undefined || e(M);
            if (J) {
                if (H) {
                    for (I in M) {
                        if (N.apply(M[I], J) === false) {
                            break
                        }
                    }
                } else {
                    for (; K < L;) {
                        if (N.apply(M[K++], J) === false) {
                            break
                        }
                    }
                }
            } else {
                if (H) {
                    for (I in M) {
                        if (N.call(M[I], I, M[I]) === false) {
                            break
                        }
                    }
                } else {
                    for (; K < L;) {
                        if (N.call(M[K], K, M[K++]) === false) {
                            break
                        }
                    }
                }
            }
            return M
        }, B = function (H, J) {
            var I = false,
                K = J.toLowerCase();
            c(H, function (L, M) {
                if (K == L.toLowerCase()) {
                    I = M;
                    return false
                }
            });
            return I
        }, p = function (H, J) {
            var I = false,
                K = J.toLowerCase();
            c(H, function (L, M) {
                if (K == L.toLowerCase()) {
                    I = L;
                    return false
                }
            });
            return I
        }, u = function (L) {
            var K = L.split("|"),
                H = "",
                J, I = "";
            for (J in K) {
                if (K.hasOwnProperty(J)) {
                    H = d(K[J]);
                    if (H !== "") {
                        I = H
                    }
                }
            }
            return I
        }, F = function (J, N) {
            var M = document.getElementsByTagName("link");
            for (var H in M) {
                if (M[H].href != null && M[H].href.indexOf(J) != -1) {
                    return true
                }
            }
            var K = document.getElementsByTagName("head")[0];
            var L = document.createElement("link"),
                I = false;
            L.setAttribute("rel", "stylesheet");
            L.setAttribute("type", "text/css");
            L.setAttribute("href", J);
            K.appendChild(L);
            L.onload = L.onreadystatechange = function () {
                if (!I && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
                    I = true;
                    L.onload = L.onreadystatechange = null
                }
            };
            return false
        }, j = {}, y = function (I, Q, L) {
            var O = document.getElementsByTagName("script"),
                J, H, K = false,
                M;
            for (var P in O) {
                H = O[P];
                if (H.src != null && H.src.indexOf(I) != -1 && typeof Q === "function") {
                    if (H.readyState === "loaded" || H.readyState === "complete") {
                        Q();
                        return true
                    } else {
                        break
                    }
                }
                H = null
            }
            if (H === null) {
                J = document.getElementsByTagName("head")[0];
                H = document.createElement("script");
                H.setAttribute("type", "text/javascript");
                H.setAttribute("src", I);
                try {
                    J.appendChild(H)
                } catch (N) {
                    if (debug) {
                        console.log("Debug: Warning! Request for " + I + " failed. Reason: " + N)
                    }
                }
            }
            M = j[H.src];
            if (M == null) {
                M = Array();
                j[H.src] = M
            }
            if (typeof Q === "function") {
                M.push(Q)
            }
            H.onload = H.onreadystatechange = function () {
                var T, S, R;
                if (!K && (!H.readyState || H.readyState === "loaded" || H.readyState === "complete")) {
                    K = true;
                    H.onload = H.onreadystatechange = null;
                    if (H && H.parentNode) {
                        R = j[H.src];
                        for (S = R.length; S--;) {
                            R[S]()
                        }
                    }
                }
            };
            return false
        }, b = function (I, K, H) {
            var J = (/[.]/.exec(I)) ? /[^.]+$/.exec(I) : undefined;
            if (J == "css") {
                F(I);
                if (typeof K === "function") {
                    K(callbackParams)
                }
            } else {
                if (J == "js") {
                    y(I, K, H)
                }
            }
        }, f = (function () {
            return ("withCredentials" in new XMLHttpRequest())
        })(),
        m = function (H) {
            return encodeURIComponent(H)
        }, v = function (J, I, H) {
            if (H == undefined) {
                H = ""
            }
            if (J != null && I != null) {
                document.cookie = J + "=" + I + ";" + H
            }
        }, E = function (K) {
            var J = document.cookie.split(";");
            var I;
            for (var H = 0; H < J.length; H++) {
                I = J[H].replace(/^\s+/, "");
                if (K == I.substr(0, K.length)) {
                    return decodeURIComponent(I.substr(K.length + 1))
                }
            }
            return ""
        }, t = function () {
            if (!(/Mobile/i.test(navigator.userAgent))) {
                if ((/Android/i.test(navigator.userAgent)) || (/iPad/i.test(navigator.userAgent))) {
                    return "tablet"
                } else {
                    return "web"
                }
            } else {
                if ((/Android/i.test(navigator.userAgent)) || (/iPhone/i.test(navigator.userAgent)) || (/windows phone/i.test(navigator.userAgent)) || (/blackberry/i.test(navigator.userAgent))) {
                    return "phone"
                } else {
                    if (/iPad/i.test(navigator.userAgent)) {
                        return "tablet"
                    } else {
                        if (/iPod/i.test(navigator.userAgent)) {
                            return "ipod"
                        } else {
                            return "web"
                        }
                    }
                }
            }
        }, o = 0,
        G = function (H, L, I, K) {
            var J = "vm_aux_" + ((new Date).getTime());
            if (window[J] != null) {
                J = J + "_" + o;
                o++
            }
            window[J] = function (M) {
                K(M)
            };
            I = I == null ? "" : I;
            I = I + ((I.length == 0) ? "" : "&") + "callback=" + J;
            H = H + ((H.indexOf("?") >= 0) ? "&" : "?") + I;
            y(H, J)
        }, x = function () {
            if (window.XMLHttpRequest && f) {
                s = new XMLHttpRequest()
            } else {
                if (window.ActiveXObject) {
                    try {
                        s = new ActiveXObject("Msxml2.XMLHTTP")
                    } catch (H) {
                        try {
                            s = new ActiveXObject("Microsoft.XMLHTTP")
                        } catch (H) {}
                    }
                }
            }
            if (!s) {
                alert("Giving up :( Cannot create an XMLHTTP instance");
                return false
            }
            return s
        }, s = function (I, M, J, L, K) {
            var H = x();
            H.onreadystatechange = function () {
                if (H.readyState == 4 && H.status == 200) {
                    var N = null;
                    try {
                        N = JSON.parse(H.responseText)
                    } catch (O) {
                        if (window.console != undefined) {
                            console.log("Exception while parsing " + O);
                            console.log(H.responseText)
                        }
                    }
                    if (N != null) {
                        L(N)
                    }
                }
            };
            M = !f ? "GET" : M;
            if (M == "GET" && J.length > 0) {
                I = I + ((I.indexOf("?") >= 0) ? "&" : "?") + J
            }
            H.open(M, I, true);
            H.setRequestHeader("Content-type", K ? K : "application/x-www-form-urlencoded");
            if (!J) {
                J = ""
            }
            H.send(J)
        }, D = function (H) {
            var I = "";
            c(H, function (J, K) {
                I += J + "=" + K + "&"
            });
            I = I.slice(0, I.length - 1);
            return I
        }, a = function (I) {
            var H = I;
            if (typeof I === "function") {
                H = Array(I)
            } else {
                if (I == null || I === undefined) {
                    H = Array()
                }
            }
            return H
        }, g = function (H, I) {
            if (H && window.console !== undefined) {
                return function (J) {
                    var K, L;
                    if (J === undefined || J === null) {
                        console.log(I + ">> ")
                    } else {
                        if (Object.prototype.toString.call(J) === "[object String]") {
                            console.log(I + ">> " + J)
                        } else {
                            if (Object.prototype.toString.call(J) === "[object Object]") {
                                console.log(J)
                            } else {
                                if (Object.prototype.toString.call(J) === "[object Array]") {
                                    L = J.length;
                                    for (K = 0; K < L; K++) {
                                        console.log(I + ">>[" + K + "] " + J[K])
                                    }
                                } else {
                                    console.log(I + ">> " + J)
                                }
                            }
                        }
                    }
                }
            } else {
                return function (J) {}
            }
        }, C = function C(I, K) {
            var H = K ? K : Array(),
                J;
            for (J in I) {
                H[J] = I[J]
            }
            return H
        };
    return {
        LOADING_ANIMATION: r,
        SCENARIO_LISTINGS: w,
        SCENARIO_SEARCHBOX: z,
        SCENARIO_CALLBACK: q,
        findElement: B,
        findKey: p,
        gup: u,
        len: A,
        forEach: c,
        encode: m,
        getCookie: E,
        setCookie: v,
        include: b,
        deviceType: t(),
        include_js: y,
        mergeRecursive: n,
        ajaxCall: f ? s : G,
        canWeCors: f,
        objectToQueryString: D,
        thisMustBeAnArray: a,
        getURLParameters: d,
        getDebugFunction: g,
        copy: C
    }
}());
/*
 * Validations - validation functions
 */
/*
 * Validations - validation functions
 */
VM.Validations = (function () {
    var q = ["AK", "AL", "AR", "AZ", "CA", "CO", "CT", "DC", "DE", "FL", "GA", "HI", "IA", "ID", "IL", "IN", "KS", "KY", "LA", "MA", "MD", "ME", "MI", "MN", "MO", "MS", "MT", "NC", "ND", "NE", "NH", "NJ", "NM", "NV", "NY", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VA", "VI", "VT", "WA", "WI", "WV", "WY"],
        j = /^([0-9]{5})(?:[\-\s]*([0-9]{4}))?$/,
        r = /^([a-zA-Z0-9 ]{6,})$/,
        n = function (v, u) {
            return (v === undefined) ? u : v
        }, d = function (v, u) {
            return (v === undefined || v === "") ? u : v
        }, s = function (u) {
            if (u === undefined) {
                u = ""
            }
            return q.indexOf(u.toUpperCase()) !== -1
        }, h = function (u) {
            return j.test(u)
        }, p = function (u) {
            return r.test(u)
        }, e = function (u) {
            var v = "";
            v = /\S+@\S+\.\S+/;
            return v.test(u)
        }, k = function (u) {
            return undefined
        }, c = function (u) {
            var v = "";
            v = /^[a-zA-Z ]*$/;
            return v.test(u)
        }, b = function (u) {
            return u === undefined
        }, a = function (u) {
            return b(u) || u === ""
        }, o = function (u) {
            var v = u;
            v = v.replace(/_/g, "");
            v = v.replace(/\(/g, "");
            v = v.replace(/\)/g, "");
            v = v.replace(/-/g, "");
            v = v.replace(/\s/g, "");
            return v
        }, t = function () {
            var u = $("#edit-field-phone-number-frm-und-0-value").val();
            u = u.replace(/_/g, "");
            u = u.replace(/\(/g, "");
            u = u.replace(/\)/g, "");
            u = u.replace(/-/g, "");
            u = u.replace(/\s/g, "");
            if (!u.length) {
                return "Please enter your phone number."
            } else {
                if (u.length < 10) {
                    return "Please enter a valid phone number."
                } else {
                    if (f(u) != 1) {
                        return "Please enter a valid phone number."
                    } else {
                        return 1
                    }
                }
            }
        }, f = function (C) {
            var x = (/[2-9][0-8][0-9]/g);
            var v = (/[2-9][0-9][0-9]/g);
            var B = (/[0-9][0-9][0-9][0-9]/g);
            var A = (/(555|789|987|654|456|777)/g);
            var u = (/555/g);
            x.compile(x);
            v.compile(v);
            B.compile(B);
            A.compile(A);
            u.compile(u);
            var y = C.substring(0, 3);
            var z = C.substring(3, 6);
            var w = C.substring(6, 10);
            if (y.length != 3 || z.length != 3 || w.length != 4) {
                return "0"
            }
            if (!x.test(y)) {
                return "0"
            }
            if (!v.test(z)) {
                return "0"
            }
            if (!B.test(w)) {
                return "0"
            }
            if (A.test(y)) {
                return "0"
            }
            if (u.test(z)) {
                return "0"
            }
            return "1"
        }, l = function (u) {
            return (!u || /^\s*$/.test(u))
        }, m = function (u) {
            return !isNaN(u - 0) && u !== null && u.replace(/^\s\s*/, "") !== "" && u !== false
        }, g = function (y, w, z) {
            var x, v = y.length,
                D, u, B, C, E = false,
                A;
            for (x = v; x--;) {
                D = y.slice(x, x + 1);
                u = D.attr("name");
                if (u !== undefined && u != "") {
                    B = u.toLowerCase();
                    C = D.val();
                    A = l(C);
                    if (B == "state") {
                        A |= !s(C)
                    } else {
                        if (B == "zip" || B == "zipcode" || B == "location") {
                            A |= !h(C)
                        }
                    }
                    if (B == "phone") {
                        if (C == "Phone") {
                            A |= !k(C)
                        } else {
                            C = o(C);
                            response = f(C);
                            if (response == 0) {
                                A |= !k(C)
                            }
                        }
                    }
                    if (B == "email") {
                        if (C == "Email") {
                            A |= !k(C)
                        } else {
                            response = e(C);
                            if (response == false) {
                                A |= !k(C)
                            }
                        }
                    }
                    if (B == "first_name" && C == "First Name") {
                        A |= !k(C);
                        A |= !c(C)
                    }
                    if (B == "last_name" && C == "Last Name") {
                        A |= !k(C);
                        A |= !c(C)
                    }
                    if (B == "address" && C == "Adress") {
                        A |= !k(C)
                    }
                    if (A) {
                        if (typeof w === "function") {
                            w(y.slice(x, x + 1))
                        }
                        E |= A
                    }
                }
            }
            if (E && (typeof z == "function")) {
                z()
            }
            return !E
        };
    return {
        isValidState: s,
        validateNANP: f,
        removeFormat: o,
        isValidPostalCode: h,
        isBlank: l,
        isValidProduct: function (u) {
            if (!isNaN(parseFloat(u)) && isFinite(u)) {
                return true
            }
            return false
        },
        isUndefined: b,
        isUndefinedOrEmpty: a,
        valueIfUndefined: n,
        valueIfUndefinedOrEmpty: d,
        isNumber: m,
        isValidLocation: function (z, x, y) {
            var u = true,
                w = this.isValidPostalCode(x),
                v = this.isValidState(y);
            if (w || v) {
                u = true
            } else {
                if (z === undefined || z !== "US") {
                    u = false
                }
            }
            return u
        },
        isValidGeoLocation: function () {
            var w, x, u, v;
            if (VM_GEOLOCATION != undefined) {
                w = VM_GEOLOCATION.getLocationData();
                x = w.country;
                u = w.zipCode;
                v = w.state;
                return this.isValidLocation(x, u, v)
            }
            return false
        },
        validateFieldCollection: g
    }
}());
VM.UT = VM.UT || function () {
    var g = "//marketplaces.vantagemedia.com/ut/json?callback=VM.UT.set",
        b = "userid",
        c = "interactionid",
        j = "_VMU",
        a = "_VMI",
        l = VM.Utils,
        f, m, o, e = function () {
            l.setCookie(j, f, o);
            l.setCookie(a, m, o)
        }, n = function () {
            f = l.getCookie(j);
            m = l.getCookie(a)
        }, k = function (p) {
            var r, q;
            f = p.UserId;
            m = p.InteractionId;
            o = p.Expiration;
            e()
        }, d = function () {
            if (f == null || m == null) {
                return null
            }
            return {
                user: f,
                interaction: m
            }
        }, h = function (p) {
            VM.Utils.include_js(g, p)
        };
    return {
        set: k,
        get: d,
        retrieve: n,
        fetchUT: h,
        INTERACTION_ID_PARAM: c,
        USER_ID_PARAM: b
    }
}();
VM = VM || {};
VM.Assets = VM.Assets || (function () {
    var s = VM.Utils,
        p = "//images.vantage-media.net/p/scripts/jquery-1.8.1.min.js",
        e = "//images.vantage-media.net/p/scripts/bootstrap/bootstrap.min.js",
        c = "//images.vantage-media.net/p/scripts/bootstrap/bootstrap.min.css",
        r = "//images.vantage-media.net/p/scripts/bootstrap-full/bootstrap.min.js",
        d = "//images.vantage-media.net/p/scripts/bootstrap-full/bootstrap.min.css",
        b = "//images.vantage-media.net/p/scripts/jquery.inputmask.js",
        q = "//images.vantage-media.net/p/scripts/jquery-validate/jquery.validate.min.js",
        j = "//images.vantage-media.net/p/scripts/jquery-validate/additional-methods.min.js",
        f = "//images.vantage-media.net/p/scripts/jquery-validate/jquery.validate.vm-methods.js",
        u = "//images.vantage-media.net/p/scripts/vm_jQ.js",
        m = "//images.vantage-media.net/p/scripts/sizzle.min.js",
        h = false,
        o = Array(),
        w = function (x) {
            if (VM.jQuery === undefined && window.jQuery === undefined) {
                VM.Utils.include(p, function () {
                    jQuery.noConflict();
                    x()
                })
            } else {
                x()
            }
        }, l = function () {}, t = function () {}, n = function () {
            if (window.Sizzle === undefined) {
                s.include(m)
            }
        }, v = function (x) {}, a = function (x) {
            if (x.sizzle) {
                n()
            }
            if (x.DEFAULT) {
                if (!h) {
                    s.include(u, function () {
                        var y, A, z = o.length;
                        h = true;
                        for (y = 0; y < z; y++) {
                            o[y]()
                        }
                    })
                }
            } else {
                if (x.jQuery) {
                    w(function () {
                        if (x.bootstrap) {
                            l()
                        }
                        if (x.inputmask) {
                            t()
                        }
                    })
                } else {
                    if (x.bootstrap) {
                        l()
                    }
                    if (x.inputmask) {
                        t()
                    }
                }
                if (x.jQueryValidateExtra) {
                    v(true)
                } else {
                    if (x.jQueryValidate) {
                        v(false)
                    }
                }
            }
        }, k = function (x, y) {
            if (x.DEFAULT) {
                if (h) {
                    y()
                } else {
                    if (typeof y === "function") {
                        o.push(y)
                    }
                }
            }
        }, g = function g(x) {
            if (x.DEFAULT) {
                return h
            }
            return null
        };
    return {
        include: a,
        runWhenLoaded: k,
        isLoaded: g,
        displays: {},
        listing: {},
        content: {},
        fas: {},
        feature: {},
        searchbox: {},
        advSearch: {},
        leads: {},
        experience: {},
        helpers: {},
        templates: {}
    }
})();
VM = window.VM || {};
VM.Vertical = window.VM.Vertical || Array();
VM.Vertical[3] = VM.Vertical[3] || (function () {
    var d = VM.Utils,
        a = {
            LoanType: "loanType",
            LoanAmount: "loanAmount",
            InterestRate: "interestRate",
            CreditScore: "creditScore",
            PropertyType: "propertyType",
            PropertyUse: "propertyUse"
        }, c = {
            REQUIRED: {},
            ALL: {}
        }, b = (function () {
            var e = {
                REQUIRED: {
                    engagement_id: "engagement_id",
                    propertyZip: "zipCode",
                    creditScore: "creditScore",
                    propertyType: "propertyType",
                    propertyUse: "propertyUse",
                    propertyValue: "estimated_Property_Value",
                    downPayment: "estimated_Down_Payment",
                    mortgageBalance: "existing_Mortgage_Balance",
                    first_Name: "first_Name",
                    last_Name: "last_Name",
                    email: "email",
                    phone: "phone",
                    address: "address",
                    city: "city",
                    state: "state"
                },
                ALL: {
                    leadId: "leadId",
                    isTest: "isTest"
                }
            };
            d.mergeRecursive(e.ALL, e.REQUIRED);
            return e
        })();
    return {
        LISTINGS_PARAMETERS: a,
        ODP_PARAMETERS: c,
        CDP_PARAMETERS: b
    }
})();
VM.Renderer = (function (h) {
    var e = h.resources,
        m = VM.Utils,
        l = h.data,
        b = m.SCENARIO_LISTINGS,
        j = m.SCENARIO_SEARCHBOX,
        o = m.SCENARIO_CALLBACK;
    if (l == null || l.Header == null) {
        return
    }
    switch (h.scenario) {
        case b:
            if (!e || !VM.DAL.areAssetsComplete(h)) {
                return
            }
            break;
        case o:
            if (typeof h.callback !== "function") {
                return
            }
            break
    }
    var a = h.debug != null ? h.debug : false,
        k = h.log,
        r = (function () {
            return Array()
        })(),
        p = h.flags && h.flags.preRender && h.flags.preRender ? Array() : (function () {
            var v = h.log,
                w = function (B, z) {
                    var y, A = B.Description.length,
                        x = Array();
                    if (z.params.preRender) {
                        return
                    }
                    for (y = 0; y < A;
                    y++) {
                        if (B.Description[y]) {
                            x.push(B.Description[y])
                        }
                    }
                    B.Description = x
                }, s = function (y, x) {
                    if (x.params.preRender) {
                        return
                    }
                    y._adNumber = x.index + 1;
                    if (y.Title != null && y.HeadLine == undefined) {
                        y.HeadLine = y.Title
                    }
                    if (y.Title == undefined && y.HeadLine != null) {
                        y.Title = y.HeadLine
                    }
                }, u = function (B, A) {
                    var z, x;
                    var y = "";
                    if (B.ExtraAttributes !== undefined) {
                        for (z = B.ExtraAttributes.length;
                        z--;) {
                            y = "_" + (B.ExtraAttributes[z].VerticalAttribute.Name).replace(/\s/g, "");
                            if (B[y] === undefined) {
                                B[y] = Array()
                            }
                            B[y].push(B.ExtraAttributes[z].Value.Value)
                        }
                    }
                    if (h.md == 1) {
                        if (B._CampusType !== undefined && B._CampusType.length == 1) {
                            x = B._CampusType[0].toLowerCase();
                            if (x.toLowerCase() == "both" || x == "no preference") {
                                B._CampusType = ["Campus", "Online"]
                            }
                        }
                        switch (h.scenario) {
                            case b:
                                B._areasOfStudy = B._SubAreaOfStudy;
                                B._degrees = B._DegreeLevel;
                                B._modalities = B._CampusType;
                                break;
                            case j:
                            case o:
                            default:
                                break
                        }
                    }
                }, t = function (z, y) {
                    var x;
                    if (z.Products !== undefined) {
                        for (x = z.Products.length;
                        x--;) {
                            if (z.Products[x].Type == "Click") {
                                z._click = z.Products[x]
                            } else {
                                if (z.Products[x].Type == "Call") {
                                    if (z.Products[x].CallNumber !== undefined) {
                                        z._call = z.Products[x]
                                    }
                                } else {
                                    if (z.Products[x].Type == "Lead") {
                                        z._lead = z.Products[x]
                                    }
                                }
                            }
                        }
                    }
                    z._actionUrl = z._click == undefined ? null : z._click.ActionUrl;
                    if (z._call != undefined) {
                        if (z._call.CallNumber.length == 10) {
                            z._call.CallNumber = z._call.CallNumber.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3")
                        } else {
                            if (z._call.CallNumber.length == 12 && z._call.CallNumber.substr(0, 2) == "+1") {
                                z._call.CallNumber = z._call.CallNumber.substr(2, 12).replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3")
                            }
                        }
                    }
                };
            h.flags = h.flags || {};
            h.flags.preRender = true;
            return Array(s, u, t, w)
        })(),
        g = (function () {
            return Array()
        })(),
        f = (function () {
            return Array()
        })(),
        d = function (y, v, u) {
            var z = r.concat(u.helper.preRenders),
                C = p.concat(u.helper.preRendersByItem),
                s = C.length,
                B = y.Results,
                t = B.length,
                x, w;
            for (x = 0; x < t; x++) {
                var D = B[x];
                for (w = 0; w < s; w++) {
                    try {
                        C[w](D, {
                            data: y,
                            list: B,
                            params: v,
                            index: x
                        })
                    } catch (A) {
                        k("Exception found in preRender by item " + x + ": " + A);
                        k({
                            fn: C[w]
                        })
                    }
                }
            }
            s = z.length;
            for (x = 0; x < s; x++) {
                try {
                    z[x](y)
                } catch (A) {
                    if (a && window.console !== undefined) {
                        k("Exception found in preRender " + x + ": " + A);
                        k({
                            fn: z[x]
                        })
                    }
                }
            }
        }, n = function (y, v, t) {
            var u = g.concat(t.helper.postRenders),
                z = f.concat(t.helper.postRendersByItem),
                B = y.Results,
                s = B.length,
                C;
            v.layout.setAttribute("style", "display: none");
            C = z.length;
            if (C > 0) {
                for (var x = s; x--;) {
                    var D = B[x];
                    for (var w = 0; w < C; w++) {
                        try {
                            z[w](D, {
                                list: B,
                                params: v,
                                index: x,
                                data: y
                            })
                        } catch (A) {
                            k("Exception found in postRender by item " + x + ": " + A);
                            k({
                                fn: z[w]
                            })
                        }
                    }
                }
            }
            C = u.length;
            for (var x = 0; x < C;
            x++) {
                try {
                    u[x](y)
                } catch (A) {
                    k("Exception found in postRender " + x + ": " + A);
                    k({
                        fn: u[x]
                    })
                }
            }
            v.layout.setAttribute("style", "display: block");
            v.layout.setAttribute("style", "")
        }, q = function (s) {
            if (s.helper === undefined) {
                s.helper = {
                    preRenders: Array(),
                    preRendersByItem: Array(),
                    postRenders: Array,
                    postRendersByItem: Array()
                }
            } else {
                s.helper.preRenders = m.thisMustBeAnArray(s.helper.preRenders);
                s.helper.preRendersByItem = m.thisMustBeAnArray(s.helper.preRendersByItem);
                s.helper.postRenders = m.thisMustBeAnArray(s.helper.postRenders);
                s.helper.postRendersByItem = m.thisMustBeAnArray(s.helper.postRendersByItem)
            }
        }, c = function () {
            var s = h.layout,
                t, u = "No results found.",
                v = false;
            k("Initializing Renderer");
            if (l != null) {
                if (typeof l.error == "undefined") {
                    if (l.ResponseStatus.Status == "Ok") {
                        if (h.callback != null) {
                            e = {};
                            e.helper = {};
                            if (h.scriptParams.renders != null) {
                                if (typeof h.scriptParams.renders == "function") {
                                    e.helper.preRenders = new Array(h.scriptParams.renders)
                                } else {
                                    e.helper.preRenders = h.scriptParams.renders
                                }
                            }
                            if (h.scriptParams.rendersByItem != null) {
                                if (typeof h.scriptParams.rendersByItem == "function") {
                                    e.helper.preRendersByItem = new Array(h.scriptParams.rendersByItem)
                                } else {
                                    e.helper.preRendersByItem = h.scriptParams.rendersByItem
                                }
                            }
                        } else {
                            if (h.split) {
                                if (e.helper.postRenders) {
                                    e.helper.postRenders = e.helper.postRenders.concat(VM.Splits.getEventTrackerTNTPostRender(h))
                                } else {
                                    e.helper.postRenders = VM.Splits.getEventTrackerTNTPostRender(h)
                                }
                            }
                        }
                        q(e);
                        d(l, h, e);
                        if (h.callback != null) {
                            h.callback(l);
                            return
                        } else {
                            t = e.template;
                            k("Rendering:");
                            k({
                                template: t,
                                data: l
                            });
                            s.innerHTML = Mustache.to_html(t, l).replace(/^\s*/mg, "");
                            v = true;
                            if (h.split) {
                                h.split.rendered = true
                            }
                            n(l, h, e)
                        }
                    } else {
                        if (l.ResponseStatus.Status == "NoResults") {
                            if (h.callback != null) {
                                h.callback(l);
                                return
                            }
                            if (h.configParams.apiParamsFallback && !h.fallbackRun) {
                                h.fallbackRun = true;
                                k("No results. Falling back with new parameters.");
                                m.mergeRecursive(h.apiParams, h.configParams.apiParamsFallback);
                                VM.Listing(h)
                            } else {
                                k("No results. Sorry :(");
                                if (e && e.helper && typeof e.helper.noResults === "function") {
                                    e.helper.noResults();
                                    v = true
                                }
                            }
                        } else {
                            if (h.callback != null) {
                                h.callback(l);
                                return
                            }
                            if (l.ResponseStatus != undefined && l.ResponseStatus.Status != undefined) {
                                k("No data to render. Service response status is: " + l.ResponseStatus.Status + ". With message: " + l.ResponseStatus.Message)
                            } else {
                                k("No data to render. Data is: ");
                                k(l)
                            }
                        }
                    }
                } else {
                    if (l.ResponseStatus != undefined && l.ResponseStatus.ErrorCode != undefined) {
                        k("Data source returned error code " + l.ResponseStatus.ErrorCode + '. With message: "' + l.ResponseStatus.Message + "\". You may be in trouble (don't make it double).")
                    } else {
                        k("Data source returned an unknown error, but here, see the response and think about it.");
                        k(l)
                    }
                }
                if (!v) {
                    s.innerHTML = u
                }
            } else {
                k("Data response is undefined. Please check data source.")
            }
        };
    c()
});
VM.DAL = (function () {
    var z = VM.Utils,
        v = VM.Renderer,
        E = VM.Leads,
        u = VM.Validations,
        K = "Helper",
        k = "Template",
        h = z.SCENARIO_LISTINGS,
        y = z.SCENARIO_SEARCHBOX,
        f = z.SCENARIO_CALLBACK,
        c = [null, "edu", "dhs", "mo"],
        o = function (P) {
            var O = P.resources;
            try {
                log("Rendering search box");
                P.layout.innerHTML = Mustache.to_html(O.template, P.scriptParams).replace(/^\s*/mg, "");
                O.helper(P)
            } catch (N) {
                log("Searchbox failed to load! Reason: " + N)
            }
        }, G = /\b((custom)|([0-9]{4,5}))(-[0-9]+)?\b/i,
        M = /\bcustom\b/,
        C = "listing",
        s = "content",
        I = "fas",
        L = "feature",
        A = "advSearch",
        q = "leads",
        d = "searchbox",
        p = "styles",
        H = "helper",
        J = "template",
        g = "styles",
        e = 0,
        r = 1,
        t = [".css", ".js"],
        n = function n(O, P, N) {
            this.layoutType = O ? O : "";
            this.asset = N ? N : "";
            this.layout = P ? P : "";
            this.functionName = null;
            this.reference = null;
            this.url = null;
            this.toString = function Q() {
                return this.layoutType + " " + this.layout + " " + this.asset
            }
        }, w = function x(Q, S) {
            var R = S.resources,
                N = R.params,
                O = G.exec(Q.layout),
                P, T = Q.asset.toLowerCase() === g ? e : r;
            if (O) {
                P = O[O.length - 1];
                if (!P) {
                    P = ""
                }
                O = M.test(O[0]) ? S.displayId : O[0];
                Q.layout = "_" + O + P;
                switch (Q.layoutType) {
                    case C:
                    case L:
                    case s:
                    case I:
                        switch (Q.asset) {
                            case H:
                                Q.functionName = "vm_" + K + "_" + O;
                                break;
                            case J:
                                Q.functionName = "vm_" + k + "_" + O;
                                break;
                            default:
                        }
                        Q.url = N.customPath + O + "/" + Q.asset + P + N.mobile + t[T];
                        break;
                    case d:
                        switch (Q.asset) {
                            case H:
                                Q.functionName = "vm_sbHelper_" + O;
                                break;
                            case J:
                                Q.functionName = "vm_sbTemplate_" + O;
                                break;
                            case g:
                                Q.url = N.customPath + O + "/" + Q.layoutType + "-" + Q.asset + P + N.mobile + t[T];
                            default:
                        }
                    case A:
                    case q:
                        Q.url = Q.url ? Q.url : N.customPath + O + "/" + Q.layoutType + P + N.mobile + t[T];
                        break;
                    default:
                        S.log("Loader: Unidentified layout: " + Q.layoutType)
                }
            } else {
                switch (Q.layoutType) {
                    case C:
                    case L:
                    case s:
                    case I:
                        switch (Q.asset) {
                            case H:
                                Q.functionName = "vm_" + Q.layout + "_" + K;
                                break;
                            case J:
                                Q.functionName = "vm_" + Q.layout + "_" + k;
                                break;
                            default:
                        }
                        Q.url = N.defaultPath + c[S.md] + "/" + Q.layoutType + "/" + Q.layout + "/" + Q.asset + N.mobile + t[T];
                        break;
                    case d:
                        switch (Q.asset) {
                            case H:
                                Q.functionName = "vm_sbHelper_" + Q.layout;
                                break;
                            case J:
                                Q.functionName = "vm_sbTemplate_" + Q.layout;
                                break;
                            default:
                        }
                    case A:
                    case q:
                        Q.url = N.defaultPath + c[S.md] + "/" + Q.layoutType + "/" + Q.layout + "/" + Q.layoutType + N.mobile + t[T];
                        break;
                    default:
                        S.log("DAL: Unidentified layout: " + Q.layoutType)
                }
            }
            if (T === r) {
                R[Q.asset].push(Q);
                if (m(Q)) {
                    l(S)
                } else {
                    R.pending.push(Q)
                }
            }
            return Q
        }, a = function a(N, O) {
            if (N.asset.toLowerCase() != g) {
                if (m(N)) {
                    l(O)
                } else {
                    z.include(N.url, function () {
                        b(O)
                    })
                }
            } else {
                z.include(N.url)
            }
        }, m = function m(N, O) {
            N.reference = (VM.Assets[N.layoutType][N.layout] && VM.Assets[N.layoutType][N.layout][N.asset]) || (N.functionName && window[N.functionName]);
            N.reference = (typeof N.reference === "function") ? N.reference : null;
            return N.reference
        }, b = function b(Q) {
            var P = Q.resources,
                N = P.pending.length,
                O;
            if (N == 0) {
                return
            }
            for (; N--;) {
                O = P.pending[N];
                if (m(O)) {
                    Q.log("DAL: Asset loaded: " + O.layoutType + " " + O.asset + " " + O.layout);
                    P.pending.splice(N, 1)
                }
            }
            l(Q)
        }, l = function l(O) {
            var N = O.resources;
            if (N.pending.length > 0) {
                return
            }
            if (O.split && O.split.winner && !O.split.complete) {
                VM.Splits.updateAssetsTNT(O);
                O.log("Splits: Restarting render for winner. We had already finished, thou");
                O.split.complete = true;
                VM.Loader(O);
                return
            }
            if (VM.Assets.isLoaded({
                DEFAULT: true
            })) {
                j(O)
            } else {
                VM.Assets.runWhenLoaded({
                    DEFAULT: true
                }, function () {
                    j(O)
                })
            }
        }, j = function j(U) {
            var S = U.resources,
                T, P, R, Q, O;
            switch (U.scenario) {
                case y:
                    S.helper = (S.helper.length > 0) ? S.helper[0].reference : null;
                    S.template = (S.template.length > 0) ? S.template[0].reference(U) : null;
                    o(U);
                    U.log("Loader complete.");
                    break;
                case h:
                    T = S.helper;
                    Q = T.length;
                    S.helper = {
                        preRenders: Array(),
                        preRendersByItem: Array(),
                        postRenders: Array(),
                        postRendersByItem: Array()
                    };
                    for (P = 0; P < Q; P++) {
                        R = T[P];
                        try {
                            O = R.reference(U);
                            switch (R.layoutType) {
                                case C:
                                case s:
                                case I:
                                case L:
                                    if (O.preRenders) {
                                        S.helper.preRenders = S.helper.preRenders.concat(O.preRenders)
                                    }
                                    if (O.preRendersByItem) {
                                        S.helper.preRendersByItem = S.helper.preRendersByItem.concat(O.preRendersByItem)
                                    }
                                    if (O.postRendersByItem) {
                                        S.helper.postRendersByItem = S.helper.postRendersByItem.concat(O.postRendersByItem)
                                    }
                                    if (O.postRenders) {
                                        S.helper.postRenders = S.helper.postRenders.concat(O.postRenders)
                                    }
                                    U.log("DAL: Loaded helper " + R.layoutType + " " + R.layout);
                                    break;
                                case q:
                                case A:
                                    if (O) {
                                        U[R.layoutType] = VM.Assets[R.layoutType][R.layout];
                                        S.helper.postRenders = S.helper.postRenders.concat(U[R.layoutType][R.asset](U));
                                        U.log("DAL: Loaded helper " + R.layoutType + " " + R.layout)
                                    }
                                    break;
                                default:
                                    U.log("DAL: Unrecognized helper for " + R.layoutType + " " + R.layout);
                                    break
                            }
                        } catch (N) {
                            U.log("DAL: Could not load asset for " + R.toString())
                        }
                    }
                    T = S.template;
                    Q = T.length;
                    S.template = "";
                    for (P = 0;
                    P < Q; P++) {
                        R = T[P];
                        try {
                            if (U.scenario === y) {
                                S.template = S.template + R.reference(U)
                            } else {
                                S.template = S.template + R.reference({
                                    templateParams: U.configParams.templateParams,
                                    apiParams: U.apiParams,
                                    params: U
                                })
                            }
                            U.log("DAL: Loaded template " + R.layoutType + " " + R.layout)
                        } catch (N) {
                            U.log("DAL: Could not load template " + R.layoutType + " " + R.layout + " because: \n" + N)
                        }
                    }
                    U.log("Loader complete.");
                    v(U);
                    break;
                default:
                    break
            }
        }, F = false,
        B = function (U) {
            if (U.configParams && U.configParams.split && U.configParams.split.tracking == "t&t" && !F) {
                F = true;
                U.log("Holding DAL.");
                window.setTimeout(function () {
                    U.log("Resuming DAL.");
                    B(U)
                }, 200);
                return
            }
            U.resources = {
                helper: Array(),
                template: Array(),
                pending: Array()
            };
            debug = U.debug != null ? U.debug : debug;
            log = U.log;
            log("Initializing Display Assets Loader");
            if (U.split && U.split.winner && !U.split.complete) {
                VM.Splits.updateAssetsTNT(U);
                U.log("Splits: Winner received. Good thing we don't have to restart anything.");
                U.split.complete = true
            }
            if (U.configParams == null) {
                return
            }
            var Y = U.configParams,
                S = U.apiParams,
                P = U.scriptParams.IMPLEMENTATIONS_ASSETS_PATH_BASE,
                O = (Y.mobile !== undefined && Y.mobile && z.deviceType != "web") ? "-" + z.deviceType : "",
                V, N, X, R, Q, Z, W, T = c[U.md];
            U.resources.params = {
                mobile: O,
                customPath: (U.serverPath + U.displayIdDirectory).replace(U.displayId + "/", ""),
                defaultPath: P
            };
            switch (U.scenario) {
                case h:
                    if (Object.prototype.toString.call(Y.listings) === "[object String]") {
                        Y.listings = {
                            helper: Y.listings,
                            template: Y.listings,
                            styles: Y.listings
                        }
                    }
                    Y.listings = Y.listings || {};
                    Y.listings.helper = Y.helper ? Y.helper : Y.listings.helper;
                    Y.listings.template = Y.template ? Y.template : Y.listings.template;
                    Y.listings.styles = Y.styles ? Y.styles : Y.listings.styles;
                    X = Y.listings.styles;
                    if (X != null) {
                        if (Object.prototype.toString.call(X) === "[object String]") {
                            N = X.split(",")
                        } else {
                            if (Object.prototype.toString.call(X) === "[object Array]") {
                                N = X
                            } else {
                                log("Unreadable styles parameter.");
                                return
                            }
                        }
                        for (W = N.length; W--;) {
                            X = N[W];
                            if (X != null && X != "") {
                                X = w(new n(Y.display, X, g), U);
                                a(X, U)
                            }
                        }
                    }
                    U.resources.pending = new Array();
                    U.resources.helper = new Array();
                    X = Y.listings.helper;
                    if (X != null) {
                        if (Object.prototype.toString.call(X) === "[object String]") {
                            N = X.split(",")
                        } else {
                            if (Object.prototype.toString.call(X) === "[object Array]") {
                                N = X
                            } else {
                                log("Unreadable styles parameter.");
                                return
                            }
                        }
                        for (W = N.length; W--;) {
                            X = N[W];
                            if (X != null && X != "") {
                                X = w(new n(Y.display, X, H), U);
                                a(X, U)
                            }
                        }
                    }
                    U.resources.template = new Array();
                    X = Y.listings.template;
                    if (X != null) {
                        if (Object.prototype.toString.call(X) === "[object String]") {
                            N = X.split(",")
                        } else {
                            if (Object.prototype.toString.call(X) === "[object Array]") {
                                N = X
                            } else {
                                log("Unreadable styles parameter.");
                                return
                            }
                        }
                        U.resources.templates = {
                            readyCount: 0,
                            items: Array()
                        };
                        for (W = N.length; W--;) {
                            X = N[W];
                            if (X != null && X != "") {
                                X = w(new n(Y.display, X, J), U);
                                a(X, U)
                            }
                        }
                    }
                    switch (true) {
                        case /1/.test(U.apiParams.engagementOption):
                        case /2/.test(U.apiParams.engagementOption):
                        case /4/.test(U.apiParams.engagementOption):
                            X = undefined;
                            break;
                        default:
                            X = Y.leads;
                            break
                    }
                    X = (X === undefined) ? null : X;
                    U.leads = X;
                    if (X) {
                        if (Object.prototype.toString.call(X) === "[object String]") {
                            U.leads = X = {
                                helper: X,
                                template: X
                            }
                        } else {
                            if (Object.prototype.toString.call(X) === "[object Object]") {
                                X.helper = X.helper || X.leads;
                                X.template = X.template || X.leads
                            }
                        }
                        if (U.leads.template) {
                            a(w(new n(q, X.template, J), U), U)
                        }
                        if (U.leads.helper) {
                            X = w(new n(q, X.helper, H), U);
                            if (U.leads.helper != U.leads.template) {
                                a(X, U)
                            }
                        }
                    }
                    X = (Y.advSearch) ? Y.advSearch : null;
                    U.advSearch = X;
                    if (X) {
                        if (Object.prototype.toString.call(X) === "[object String]") {
                            U.advSearch = X = {
                                helper: X,
                                template: X
                            }
                        } else {
                            if (Object.prototype.toString.call(X) === "[object Object]") {
                                X.helper = X.helper || X.advSearch;
                                X.template = X.template || X.advSearch
                            }
                        }
                        if (U.advSearch.template) {
                            a(w(new n(A, X.template, J), U), U)
                        }
                        if (U.advSearch.helper) {
                            X = w(new n(A, X.helper, H), U);
                            if (U.advSearch.helper != U.advSearch.template) {
                                a(X, U)
                            }
                        }
                    }
                    break;
                case y:
                    defaultPath = P + T + "/searchbox/", X = Y.searchBox;
                    if (X != null) {
                        if (Object.prototype.toString.call(X) === "[object String]") {
                            U.searchBox = X = {
                                helper: X,
                                template: X,
                                styles: X
                            }
                        } else {
                            if (Object.prototype.toString.call(X) === "[object Object]") {
                                X.helper = X.helper || X.searchBox;
                                X.template = X.template || X.searchBox;
                                X.styles = X.styles || X.searchBox
                            }
                        }
                        a(w(new n(d, X.styles, g), U), U);
                        a(w(new n(d, X.template, J), U), U);
                        X = w(new n(d, X.helper, H), U);
                        if (X.helper != X.template) {
                            a(X, U)
                        }
                    }
                    break;
                default:
                    log("Something has gone wrong. You shouldn't be fetching assets.");
                    break
            }
        }, D = function D(N) {
            return N.resources.pending.length == 0
        };
    return {
        VERTICAL_NAME: c,
        load: B,
        areAssetsComplete: D
    }
})();
VM.Loader = VM.DAL.load;
VM.namespace("Listing");
VM.Listing = (function (e) {
    var b = VM.Renderer,
        k = VM.Utils,
        j = e.scriptParams.MARKETPLACES_URL,
        g = e.scriptParams.HEADER_URL,
        a = e.debug != null ? e.debug : false,
        f = e.log,
        d = function () {
            e.flags = e.flags || {};
            e.flags.preRender = false;
            e.log("Listings complete.");
            b(e)
        }, h = function (m) {
            e.data = m;
            if (e.header != null) {
                e.data.Header = e.header;
                if (e.debug && m.OpenMatchListings != null && m.OpenMatchListings.length > 0 && m.OpenMatchListings[0].AdvertiserName != null) {
                    alert("The ODP data will be sent to " + m.OpenMatchListings[0].AdvertiserName + " in a few minutes if the user doesn't submits it to any other advertiser")
                }
                d()
            }
        }, c = function (m) {
            if (e.data != null) {
                e.data.Header = m;
                d()
            } else {
                e.header = m
            }
        }, l = function () {
            e.data = null;
            e.header = null;
            if (e.layout != undefined) {
                try {
                    e.layout.innerHTML = VM.Utils.LOADING_ANIMATION
                } catch (n) {
                    e.layout.text = VM.Utils.LOADING_ANIMATION;
                    return
                }
            }
            if (e.configParams == null || !e.geo) {
                return
            }
            f("Fetching listings");
            var m = k.objectToQueryString(e.apiParams),
                o = "POST";
            k.ajaxCall(g, o, m, c);
            if (e.apiParams.st != null) {
                m = k.objectToQueryString(e.apiParams)
            }
            k.ajaxCall(j, o, k.canWeCors ? JSON.stringify(e.apiParams) : m, h, "application/json")
        };
    l()
});
VM.Splits = VM.Splits || (function () {
    var l = VM.Utils,
        h = VM.Validations,
        m = "//images.vantage-media.net/p/scripts/mbox-vm.js",
        c = "Layout A",
        e = "Layout ",
        g = {}, k = function (p) {
            var q = p.log,
                o = p.configParams,
                u = "vm" + o.displayId + "spl",
                s = null,
                r, v, w;
            p.split = l.getCookie(u);
            if (p.split == "0") {} else {
                if (p.split != "") {
                    try {
                        s = o.split.options[p.split - 1]
                    } catch (t) {}
                }
                if (!s) {
                    r = o.split.options.length + 1;
                    w = Math.log(r) / Math.log(2);
                    if (w % 2 === 0) {
                        r = (new Date()).getTime() & (r - 1)
                    } else {
                        r = Math.floor(Math.random() * r)
                    }
                    p.split = r;
                    if (r > 0) {
                        s = o.split.options[r - 1]
                    }
                    l.setCookie(u, r)
                }
                if (s) {
                    o.template = h.valueIfUndefinedOrEmpty(s.template, o.template);
                    o.helper = h.valueIfUndefinedOrEmpty(s.helper, o.helper);
                    o.styles = h.valueIfUndefinedOrEmpty(s.styles, o.styles)
                }
            }
            p.split = {
                id: p.split,
                name: e + String.fromCharCode("A".charCodeAt(0) + parseInt(p.split))
            };
            if (o.split.tracking == "adSource") {
                p.apiParams.adSource = p.split.name
            } else {
                if (o.split.tracking == "publisherTrackingId") {
                    p.apiParams.publisherTrackingId = p.split.name
                }
            }
            q("Split test set to " + p.split)
        }, d = function (s, u) {
            var q = s.displayId,
                r = s.configParams,
                t, o, p = false;
            if (!r.split.mbox) {
                s.log("Splits: No mbox data for displays. Breaking.");
                return
            }
            t = document.createElement("div");
            t.setAttribute("id", "vmTnT_" + s.displayId);
            s.layout.parentElement.appendChild(t);
            g[s.displayId] = s;
            s.split = {
                rendered: false,
                winner: null,
                complete: false
            };
            l.include(m, function () {
                var v = s,
                    w = u;
                if (window.console) {
                    console.log(s.configParams.split.mbox)
                }
                mboxDefine(t.id, s.configParams.split.mbox);
                mboxUpdate(s.configParams.split.mbox)
            })
        }, b = function b(q) {
            var p = VM.jQuery,
                o = function () {
                    var r, s, t;
                    if (!q.configParams.split || !q.configParams.split.trackDefault || q.configParams.split.trackDefault.length == 0) {
                        return
                    }
                    r = p(q.layout);
                    for (s = q.configParams.split.trackDefault.length;
                    s--;) {
                        t = q.configParams.split.trackDefault[s];
                        if (t.type && t.selector) {
                            p(t.selector).attr("tntEvent", "click");
                            p(t.selector).attr("tntType", t.type)
                        } else {
                            q.log("Split: Invalid tracking entry: " + t)
                        }
                    }
                    r.find("[tntEvent][tntType]").click(function () {
                        var u = p(this).attr("tntEvent"),
                            v = p(this).attr("tntType");
                        mboxUpdate(q.configParams.split.mbox, u + "=" + v)
                    })
                };
            return Array(o)
        }, n = function n(u) {
            var q = u.configParams,
                p = [null, "edu", "icp", "mo"],
                s = (q.mobile !== undefined && q.mobile && Utils.deviceType != "web") ? "-" + Utils.deviceType : "",
                r = u.scriptParams.IMPLEMENTATIONS_ASSETS_PATH_BASE + p[u.md] + "/" + q.display + "/",
                o = u.serverPath + u.displayIdDirectory,
                t = (q.styles === undefined) ? null : q.styles;
            if (t != null) {
                if (Object.prototype.toString.call(t) === "[object String]") {
                    elements = t.split(",")
                } else {
                    if (Object.prototype.toString.call(t) === "[object Array]") {
                        elements = t
                    } else {
                        log("Unreadable styles parameter.");
                        return
                    }
                }
                for (i = elements.length; i--;) {
                    t = elements[i];
                    if (t != null && t != "") {
                        if (t === "custom") {
                            t = o + "styles" + s + ".css"
                        } else {
                            if (/[0-9]{5}/.test(t)) {
                                t = o.replace(u.displayId, t) + "styles" + s + ".css"
                            } else {
                                t = r + t + "/styles" + s + ".css"
                            }
                        }
                        jQuery('link[rel=stylesheet][href="' + t + '"]').remove()
                    }
                }
            }
        }, a = function a(p) {
            var o = p.split.winner - 1;
            p.split.winner = null;
            if (o < 0 || o > p.configParams.split.options.length) {
                p.log("Split tests. Invalid winner value returned from test and target: " + o);
                return
            }
            p.configParams.helper = p.configParams.split.options[o].helper ? p.configParams.split.options[o].helper : p.configParams.helper;
            p.configParams.template = p.configParams.split.options[o].template ? p.configParams.split.options[o].template : p.configParams.template;
            n(p);
            p.configParams.styles = p.configParams.split.options[o].styles ? p.configParams.split.options[o].styles : p.configParams.styles;
            if (p.configParams.split.options[o].track) {
                p.configParams.split.trackDefault = p.configParams.split.options[o].track
            }
        }, f = function f(o, p) {
            var q = null;
            if (g[o]) {
                q = g[o];
                q.split.winner = p;
                if (q.split) {
                    if (q.split.winner && q.split.rendered && !q.split.complete) {
                        q.log("Splits: Relaunching render for winner");
                        q.split.complete = true;
                        VM.jQuery(q.layout).html(VM.Utils.LOADING_ANIMATION);
                        VM.Splits.updateAssetsTNT(q);
                        VM.Loader(q)
                    }
                }
            }
        }, j = function (q, r) {
            var p = q.configParams,
                o = q.log;
            if (!p.split) {
                return false
            }
            if (!p.split.options || p.split.options.length === 0) {
                q.log("Splits: No split options. Breaking.");
                return false
            }
            switch (p.split.tracking) {
                case "adSource":
                case "publisherTrackingId":
                    k(q);
                    q.log("Initializing split test using " + p.split.tracking);
                    return true;
                    break;
                case "t&t":
                    q.log("Initializing split test using Test and Target");
                    d(q, r);
                    return true;
                    break;
                default:
                    q.log("Splits: Unrecognized split method or not implemented yet: " + cp.split.tracking);
                    d(q, r)
            }
        };
    return {
        splits: j,
        runTNT: f,
        updateAssetsTNT: a,
        getEventTrackerTNTPostRender: b
    }
})();
VM.namespace("AppParams");
VM.AppParams = (function (a) {
    var H = VM.Utils,
        O = VM.Vertical,
        r = VM.Validations,
        u = VM.Loader,
        v = VM.Listing,
        A = VM.UT,
        L = VM.Splits,
        n = {
            zipCode: "location",
            st: "st",
            state: "st",
            location: "location",
            edudegreetype: "degreeLevel",
            educampus: "modality",
            bwapsadsource: "bwapsadsource"
        }, B = {
            target: "target"
        }, b = {
            md: "md",
            location: "location",
            zipCode: "location",
            state: "state",
            interactionId: "interactionId",
            userId: "userId",
            adSource: "adSource",
            publisherTrackingId: "publisherTrackingId",
            campaignId: "campaign",
            campaign: "campaign",
            maxResults: "maxResults",
            engagementType: "engagementOption",
            engagementOption: "engagementOption",
            openDataPost: "openDataPost",
            testTransaction: "testTransaction"
        }, K = {
            AK: ["99501", "99950", "99662"],
            AL: ["35004", "36925", "36006"],
            AR: ["71601", "72959", "72120"],
            AZ: ["85001", "86556", "85327"],
            CA: ["90001", "96162", "92101"],
            CO: ["80001", "81658", "80820"],
            CT: ["06001", "06928", "06537"],
            DC: ["20001", "56972", "20202"],
            DE: ["19701", "19980", "19938"],
            FL: ["32003", "34997", "33199"],
            GA: ["30002", "39901", "31017"],
            HI: ["96701", "96898", "96811"],
            IA: ["50001", "52809", "50055"],
            ID: ["83201", "83877", "83705"],
            IL: ["60001", "62999", "61736"],
            IN: ["46001", "47997", "47043"],
            KS: ["66002", "67954", "67444"],
            KY: ["40003", "42788", "40150"],
            LA: ["70001", "71497", "70760"],
            MA: ["01001", "05544", "02120"],
            MD: ["20588", "21930", "21536"],
            ME: ["03901", "04992", "04555"],
            MI: ["48001", "49971", "48617"],
            MN: ["55001", "56763", "56027"],
            MO: ["63001", "65899", "64001"],
            MS: ["38601", "39776", "39094"],
            MT: ["59001", "59937", "59633"],
            NC: ["27006", "28909", "27306"],
            ND: ["58001", "58856", "58620"],
            NE: ["68001", "69367", "68853"],
            NH: ["03031", "03897", "03804"],
            NJ: ["07001", "08989", "08816"],
            NM: ["87001", "88439", "87581"],
            NV: ["88901", "89883", "89823"],
            NY: ["10001", "14925", "10007"],
            OH: ["43001", "45999", "44822"],
            OK: ["73001", "74966", "74117"],
            OR: ["97001", "97920", "97446"],
            PA: ["15001", "19640", "17575"],
            RI: ["02801", "02940", "02864"],
            SC: ["29001", "29945", "29135"],
            SD: ["57001", "57799", "57532"],
            TN: ["37010", "38589", "37722"],
            TX: ["73301", "88595", "77381"],
            UT: ["84001", "84791", "84408"],
            VA: ["20101", "24658", "23607"],
            VT: ["05001", "05907", "05748"],
            WA: ["20001", "56972", "20004"],
            WI: ["53001", "54990", "54479"],
            WV: ["24701", "26886", "25922"],
            WY: ["82001", "83414", "82640"]
        }, o = H.SCENARIO_LISTINGS,
        G = H.SCENARIO_SEARCHBOX,
        g = H.SCENARIO_CALLBACK,
        k = "geoip",
        d = "userId",
        m = "interactionId",
        x = "1",
        t = "6604",
        M = "1",
        h = "7",
        s = "1",
        w = "1",
        p = function p(V) {
            var R = H.copy(b, {}),
                S = H.findElement(l(V), "OpenDataPost"),
                T, U;
            S = S != "" ? S : (V.scriptParams ? H.findElement(V.scriptParams, "OpenDataPost") : H.findElement(l(V), "OpenDataPost"));
            if (O[V.md] && O[V.md].LISTINGS_PARAMETERS) {
                H.copy(O[V.md].LISTINGS_PARAMETERS, R)
            }
            if ((S == "true" || S === true) && (O[V.md] && O[V.md].ODP_PARAMETERS)) {
                H.copy(O[V.md].ODP_PARAMETERS.ALL, R)
            }
            if (V.configParams && V.configParams.customParamNames) {
                H.copy(V.configParams.customParamNames, R)
            }
            return R
        }, E = function E(V, R) {
            var T = new Array(),
                S, U;
            for (S = R.length; S--;) {
                U = R[S];
                if (!V[U]) {
                    T.push(U)
                }
            }
            return T
        }, l = function () {
            var R = {}, S = {};
            window.location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (V, T, U) {
                R[T] = U
            });
            return R
        }, C = function (U) {
            var T = U.configParams.customParamValues,
                S, R;
            if (T != null) {
                for (R in T) {
                    if (U.apiParams[R] != null) {
                        S = T[R][U.apiParams[R]];
                        if (S != null) {
                            U.apiParams[R] = S
                        }
                    }
                }
            }
        }, J = function (V, S, R) {
            var U = {}, T;
            R = (R == undefined) ? false : R;
            H.forEach(V, function (W, X) {
                var Y;
                if (R) {
                    Y = S[W]
                } else {
                    Y = H.findElement(S, W)
                }
                if (!r.isUndefinedOrEmpty(Y) && !r.isUndefinedOrEmpty(X)) {
                    if (R) {
                        T = W
                    } else {
                        T = H.findElement(S, W)
                    }
                    if (T) {
                        U[T] = X
                    }
                }
            });
            return U
        }, q = function (W) {
            var U, Y, R = p(W);
            var S = l(W),
                T = J(W.scriptParams, R),
                V = W.configParams.apiParamsBase ? J(W.configParams.apiParamsBase, R) : {}, X = W.configParams.apiParamsOverride ? J(W.configParams.apiParamsOverride, R) : {};
            if (W.backwardCompatibility) {
                S = J(S, n);
                if ((U = window.bwapsadsource) !== undefined || (U = window.adsource) !== undefined) {
                    W.apiParams.adSource = U;
                    W.apiParams.publisherTrackingId = U
                }
            } else {
                S = J(S, R);
                if (S.propertyZip) {
                    S.location = S.propertyZip
                }
            }
            W.apiParams = H.mergeRecursive(W.apiParams, V);
            W.apiParams = H.mergeRecursive(W.apiParams, T);
            W.apiParams = H.mergeRecursive(W.apiParams, S);
            W.apiParams = H.mergeRecursive(W.apiParams, X);
            if (W.backwardCompatibility && !r.isUndefinedOrEmpty(W.apiParams.st) && r.isUndefinedOrEmpty(W.apiParams.location)) {
                W.apiParams.location = W.apiParams.st;
                delete W.apiParams.st
            }
            if (!r.isUndefinedOrEmpty(W.apiParams.location)) {
                if (typeof W.apiParams.location === "number") {
                    W.apiParams.location = "" + W.apiParams.location
                }
                switch (W.apiParams.location.length) {
                    case 5:
                        W.apiParams.zipCode = W.apiParams.location;
                        break;
                    case 2:
                        W.apiParams.st = W.apiParams.location.toUpperCase();
                        if (r.isValidState(W.apiParams.st)) {
                            W.apiParams.zipCode = K[W.apiParams.st][(new Date()) % 3]
                        }
                        delete W.apiParams.st;
                    default:
                        break
                }
            }
            delete W.apiParams.location;
            if (W.md == 1) {
                if (!W.apiParams.areaOfStudy) {
                    if (W.apiParams.subAreaOfStudy) {
                        W.apiParams.areaOfStudy = VM.Vertical[W.md].getAOS(W.apiParams.subAreaOfStudy)
                    } else {
                        if (W.scenario == o) {
                            W.apiParams.areaOfStudy = h
                        }
                    }
                }
            } else {
                if (W.md == 2) {
                    if (!W.apiParams.typeOfService) {
                        W.apiParams.typeOfService = w
                    }
                } else {
                    if (W.md == 3) {
                        if (!W.apiParams.loanType) {
                            W.apiParams.loanType = s
                        }
                        if (W.apiParams.loanType || W.apiParams.loanType != 3) {
                            if (W.apiParams.creditScore == "4" || W.apiParams.creditScore == "5") {
                                W.apiParams.loanType = 3
                            }
                        }
                    }
                }
            }
            if ((!W.debug || (W.apiParams.campaign != null && W.apiParams.campaign != t)) && W.publisherId != "30392" && W.publisherId != "30151") {
                W.apiParams.engagementOption = x
            }
            if (W.debug || (W.apiParams.campaign && W.apiParams.campaign == t) || W.publisherId == "30151") {
                W.apiParams.testTransaction = "true"
            }
            C(W)
        }, z = function (S) {
            var R = S.apiParams;
            S.geo = !((R.st == null && R.zipCode == null) || (R.st != undefined && R.st.toLowerCase() == k) || (R.zipCode != undefined && R.zipCode.toLowerCase() == k));
            if (!S.geo) {
                S.geo = true;
                if (window.geoip_postal_code() != undefined && !r.isUndefinedOrEmpty(geoip_postal_code())) {
                    S.apiParams.zipCode = geoip_postal_code()
                } else {
                    delete S.apiParams.zipCode
                }
            }
            if (S.apiParams.zipCode != undefined && !VM.Validations.isValidPostalCode(S.apiParams.zipCode)) {
                delete S.apiParams.zipCode
            }
        }, P = function (R) {
            switch (R.scenario) {
                case o:
                    v(R);
                    VM.DAL.load(R);
                    break;
                case G:
                    VM.DAL.load(R);
                    break;
                case g:
                    v(R);
                    break;
                default:
                    break
            }
        }, Q = function (S) {
            var R = A.get();
            if (R != null && (S.apiParams[m] == null || S.apiParams[m] == "")) {
                S.apiParams[d] = R.user;
                S.apiParams[m] = R.interaction
            }
            P(S)
        }, c = function (R) {
            R.md = R.scriptParams.md;
            R.configParams = R.configParams || {};
            R.configParams.apiParamsBase = R.configParams.apiParamsBase || {};
            R.configParams.apiParamsOverride = R.configParams.apiParamsOverride || {};
            R.apiParams = R.apiParams || {};
            if (R.configParams.md && R.configParams.md != R.md) {
                R.log("HALT: Vertical mismatch between configuration file and script parameters. Please check.");
                return
            }
            q(R);
            z(R);
            if (R.dismissUT == undefined || !R.dismissUT) {
                if (A.get() == null) {
                    A.fetchUT(function () {
                        Q(R)
                    })
                } else {
                    Q(R)
                }
            } else {
                A.retrieve();
                Q(R)
            }
        }, D = function D(S) {
            var R = S.configParams;
            switch (S.scenario) {
                case o:
                    if (!R.template && !R.listings && !R.experience) {
                        S.log("Error: Listings parametrized but no template data found on config file:");
                        S.log(R);
                        return
                    }
                    break;
                case G:
                    if (!R.searchBox) {
                        S.log("Error: Searchbox parametrized but no searchbox data found on config file:");
                        S.log(R);
                        return
                    }
                    break;
                default:
                    break
            }
            c(S)
        }, j = function j(U) {
            var T = "display_" + U.displayId,
                R;
            try {
                R = U.configParams = VM.Assets.displays["_" + U.displayId] || window[T]();
                if (R.displayId != null) {
                    U.displayId = R.displayId
                }
            } catch (S) {
                U.log("Params: Warning, no configuration function was found!\n\tReason: " + S)
            }
            if (R.templateParams === undefined) {
                R.templateParams = {}
            }
            if (U.scenario == o) {
                L.splits(U, u)
            }
            if (R.experience) {
                H.include(U.scriptParams.IMPLEMENTATIONS_ASSETS_PATH_BASE + VM.DAL.VERTICAL_NAME[U.scriptParams.md] + "/experience/" + R.experience + ".js", function () {
                    var V = VM.Assets.experience[R.experience];
                    if (V) {
                        switch (U.scenario) {
                            case o:
                                R.display = R.display ? R.display : V.display;
                                R.template = R.template ? R.template : V.template;
                                R.helper = R.helper ? R.helper : V.helper;
                                R.styles = R.styles ? R.styles : V.styles;
                                R.advSearch = R.advSearch ? R.advSearch : V.advSearch;
                                R.leads = R.leads ? R.leads : V.leads;
                                break;
                            case G:
                                R.display = R.display ? R.display : R.display;
                                R.searchBox = R.searchBox ? R.searchBox : V.searchBox;
                                break;
                            default:
                                break
                        }
                    } else {
                        U.log("Params: Experience not found: " + R.experience)
                    }
                    D(U)
                })
            } else {
                D(U)
            }
        }, N = function (W) {
            var S = W.scriptParams.IMPLEMENTATIONS_PATH_BASE,
                T = 100,
                V = Math.floor(W.publisherId / T) * T + "/" + W.publisherId + "/" + W.displayId + "/",
                U = W.displayId + ".js",
                R = S + V + U;
            W.serverPath = S;
            W.displayIdDirectory = V;
            if (typeof (window["display_" + W.displayId] || VM.Assets.displays["_" + W.displayId]) === "function") {
                j(W)
            } else {
                H.include_js(R, function () {
                    j(W)
                })
            }
        }, f = function (R) {
            R.apiParams = {};
            R.geo = false;
            if (R.scriptParams !== undefined && R.scriptParams.testTransaction !== undefined && R.scriptParams.testTransaction !== null && R.scriptParams.testTransaction === "false") {
                delete R.scriptParams.testTransaction
            }
            if (R.scriptParams !== undefined && R.scriptParams.testTransaction !== undefined && R.scriptParams.testTransaction !== null && R.scriptParams.testTransaction === "false") {
                delete R.scriptParams.testTransaction
            }
        }, e = function (S) {
            var T = J({}, S.leadsParams),
                R;
            T = J(S.leadsParams, odpParams);
            for (R = requiredODPParams.length;
            R-- && T;) {
                if (Validations.isUndefinedOrEmpty(T[requiredODPParams[R]])) {
                    T = false
                }
            }
            return T
        }, F = function F(R) {
            f(R);
            N(R)
        }, y = function y(R) {
            f(R);
            N(R)
        }, I = function I(R) {
            f(R);
            c(R)
        };
    return {
        filterParameters: J,
        getQueryStringParameters: l,
        listingParams: F,
        searchboxParams: y,
        callbackParams: I,
        leadsParams: e
    }
})();
VM.namespace("Leads");
VM.Leads = (function () {
    var f = VM.Utils,
        c = VM.AppParams,
        h = "//marketplaces.vantagemedia.com/lead?format=json",
        e = function (m, n) {
            if (!VM.Vertical[m.md]) {
                m.log('Leads: Halt. Appropiate vertical functions have not been loaded for vertical "' + m.md + '"');
                return
            }
            var k = VM.Vertical[m.md],
                j = k.CDP_PARAMETERS.ALL,
                l = c.filterParameters(m.leadsAPIParams, j, false);
            f.ajaxCall(h, "GET", f.objectToQueryString(l), function (q) {
                var s, p, o;
                if (q.Ok != undefined) {
                    m.log("OK! " + q.Ok)
                } else {
                    if (q.Error != undefined) {
                        m.log("Service returned error: " + q.Error)
                    } else {
                        if (q.Errors != undefined) {
                            s = null;
                            try {
                                s = JSON.parse(q.Errors)
                            } catch (r) {}
                            if (s == null) {
                                m.log("Service returned errors. See console please.")
                            } else {
                                m.log(s);
                                p = "";
                                for (o = s.length; o--;) {
                                    p = s[o];
                                    "\n" + p
                                }
                            }
                        } else {
                            m.log("Unknown leads response. It was " + q)
                        }
                    }
                }
                if (typeof n === "function") {
                    n(q)
                }
            })
        }, d = {}, a = function a() {
            return d
        }, b = function b(j) {
            f.mergeRecursive(d, j)
        }, g = function (l) {
            var k = l || document;
            var o = "xxTrustedFormCertUrl";
            var j = false;
            var n = k.createElement("script");
            n.type = "text/javascript";
            n.async = true;
            n.src = "http" + ("https:" == k.location.protocol ? "s" : "") + "://api.trustedform.com/trustedform.js?provide_referrer=" + escape(j) + "&field=" + escape(o) + "&l=" + new Date().getTime() + Math.random();
            var m = k.getElementsByTagName("script")[0];
            m.parentNode.insertBefore(n, m)
        };
    return {
        getData: a,
        setData: b,
        doTF: g,
        submit: e
    }
})();
VM.App = (function () {
    var d = VM.AppParams,
        c = VM.Utils,
        b = function (g) {
            var e, f = g.scriptParams;
            if (f.displayId != undefined) {
                if (f.targetElement != null) {
                    g.targetElement = f.targetElement;
                    delete f.targetElement;
                    e = "vmDisplay" + f.displayId + (g.scenario === c.SCENARIO_SEARCHBOX ? "sb" : "");
                    if ((layout = document.getElementById(e)) == null) {
                        layout = document.createElement("div");
                        layout.setAttribute("id", e);
                        if (g.targetElement == null) {
                            scripts = document.getElementsByTagName("script");
                            g.targetElement = scripts[scripts.length - 1].parentElement
                        }
                        g.targetElement.parentNode.insertBefore(layout, g.targetElement)
                    }
                    g.layout = layout
                }
            }
        }, a = function (g) {
            var h = {}, e = false,
                f;
            h.vm = {};
            h.scriptParams = g;
            h.id = 0;
            if (g.callback) {
                h.scenario = c.SCENARIO_CALLBACK;
                h.callback = g.callback;
                delete g.callback;
                if (g.dismissUT != undefined && g.dismissUT) {
                    h.dismissUT = g.dismissUT;
                    delete g.dismissUT
                }
            } else {
                if (g.action) {
                    h.scenario = c.SCENARIO_SEARCHBOX
                } else {
                    h.scenario = c.SCENARIO_LISTINGS
                }
                if (h.scriptParams.displayId !== undefined) {
                    h.displayId = h.scriptParams.displayId
                }
                if (h.scriptParams.publisherId !== undefined) {
                    h.publisherId = h.scriptParams.publisherId
                }
                b(h)
            }
            h.debug = h.scriptParams.debug == null ? e : h.scriptParams.debug;
            h.debug = h.debug && window.console !== undefined;
            e = h.debug;
            if (e) {
                f = h.displayId === undefined ? Math.floor(Math.random() * 1000) : h.displayId;
                h.instanceId = f
            }
            h.log = c.getDebugFunction(e, f);
            h.log("Initializing. Debugging enabled");
            if (g.backwardCompatibility != undefined) {
                h.backwardCompatibility = g.backwardCompatibility;
                delete g.backwardCompatibility
            } else {
                h.backwardCompatibility = false
            }
            switch (h.scenario) {
                case c.SCENARIO_LISTINGS:
                    VM.Assets.include({
                        DEFAULT: true
                    });
                    d.listingParams(h);
                    break;
                case c.SCENARIO_SEARCHBOX:
                    VM.Assets.include({
                        DEFAULT: true
                    });
                    d.searchboxParams(h);
                    break;
                case c.SCENARIO_CALLBACK:
                    d.callbackParams(h);
                    break;
                default:
                    log("No scenario seem to fit the parameters. Aborting.");
                    break
            }
        };
    return {
        initialize: a
    }
})();

var vm_load = function (t) {
    var l = "//images.vantage-media.net/p/ads/vm_app.js",
        f = "//images.vantage-media.net/p/ads/vm_app_base.js",
        m = "//images.vantage-media.net/p/scripts/jquery-1.8.1.min.js",
        s = "//images.vantage-media.net/p/scripts/json2.js",
        h = "//geo.vantagemedia.com/vm_geo.php",
        j = "//images.vantage-media.net/p/src/testApis/vm_geo.php",
        k = "//marketplaces.vantagemedia.com/search/?format=json",
        g = [null, null, null, null, ],
        a = "//publishers.vantagemedia.com/header.php",
        n = [null,
            "//images.vantage-media.net/p/ads/vm_app_edu.js",
            "//images.vantage-media.net/p/ads/vm_app_edu.js",
            "//images.vantage-media.net/p/ads/vm_app_mo.js"],
        q = "//images.vantage-media.net/p/",
        r = q + "ly/",
        c = 3,
        o = t.callback === undefined ? l : f,
        b = false,
        p = false,
        e = h,
        d,
        v = function (y, A) {
            var x = document.getElementsByTagName("head")[0],
                z = document.createElement("script"),
                w = false;
            z.setAttribute("type", "text/javascript");
            z.setAttribute("src", y);
            x.appendChild(z);
            z.onload = z.onreadystatechange = function () {
                if (!w && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
                    w = true;
                    z.onload = z.onreadystatechange = null;
                    if (z && z.parentNode && typeof A === "function") {
                        A();
                        return true
                    }
                }
            };
            return false
        },
        u = function (z) {
            var y, w = window.VM != undefined && window.VM.App != undefined,
                x = window.geoip_country_code != undefined;
            if (b) {
                return
            }
            if (!x) {
                v(e, function () {
                    u(z)
                });
                return
            }
            if (!w) {
                if (n[z.md]) {
                    v(n[z.md])
                }
                v(o, function () {
                    u(z)
                });
                return
            }
            b = true;
            y = geoip_country_code();
            if (y == "US" || y == "GT" || y == "") {
                VM.App.initialize(z)
            } else {
                if (p) {
                    console.log("Warning. No Geo IP data was found. Errors may ocurr from here on. Please check.")
                }
            }
        };
    if (t.targetElement == null) {
        d = document.getElementsByTagName("script");
        t.targetElement = d[d.length - 1]
    }
    if (window.JSON === undefined) {
        v(s)
    }
    v(m);
    t.debug = t.debug == null ? p : t.debug;
    if (!t.md) {
        t.md = c
    }
    t.HEADER_URL = a;
    if (p && g[t.md]) {
        t.MARKETPLACES_URL = g[t.md]
    } else {
        t.MARKETPLACES_URL = k
    }
    if (t.callback == undefined) {
        t.IMPLEMENTATIONS_ASSETS_PATH_BASE = r;
        t.IMPLEMENTATIONS_PATH_BASE = q
    }
    u(t)
};