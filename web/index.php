<?php
umask(0002);

if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $_SERVER['REMOTE_ADDR'] = $_SERVER['HTTP_X_FORWARDED_FOR'];
}

session_set_cookie_params(30000);

$app = include __DIR__ . '/../src/app.php';

// Zillow Web Service Identifier
$app['zwsid'] = 'X1-ZWz1djegosg7bf_4234r';

$app->run(\Cloud\CloudFlareRequest::createFromGlobals());
