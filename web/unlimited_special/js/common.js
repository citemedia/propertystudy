function cm_add_days(myDate, days) {
    return new Date(myDate.getTime() + days*24*60*60*1000);
}

function cm_number_format(number, decimals, dec_point, thousands_sep) {
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);            return '' + Math.round(n * k) / k;
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');    }
    return s.join(dec);
}

function cm_initialize_checkbox_list_menu(selector) { 
    $(selector).each(function(){
        var item = $(this);
        var input = item.find('input');
        
        // Pre-load
        if (input.attr('checked')) { 
            item.addClass('selected');
            cm_products_set_product(input);
        }
        item.click(function(e) { 
            $(selector).each(function(){
                var i = $(this);
                
                if (i.hasClass('selected')) { 
                    i.removeClass('selected');
                    
                    return;
                }
            });
            
            if (!item.hasClass('selected')) { 
                input.attr('checked', 'checked');
                item.addClass('selected'); 
                cm_products_set_product(input);
            }
        });
        
    });
}

function cm_products_set_product(product) { 
    var total_price = $(price_total_selector);
    var cart_item_price = $(price_selector);
    var subtotal_price = $(price_subtotal_selector);
    var shipping_price = $(price_shipping_selector);
    var terms_price = $('.order-total');
    var checkout_price = $('#checkout-price');
    var total = total_price.html().replace('$', '') * 1;
    var cart_item = cart_item_price.html().replace('$', '') * 1;
    var subtotal = subtotal_price.html().replace('$', '') * 1;
    var shipping = shipping_price.val().replace('$', '') * 1;
    cart_item = 0;
    total = 0; // Reset total because only one product at a time.
    subtotal = 0;

    if (product.attr('checked')) {
        total+= (product.val())*1;
        subtotal+= (product.val())*1;
        cart_item+= (product.val())*1;
    } 
    else {
        total-= (product.val())*1;
        subtotal-= (product.val())*1;
        cart_item+= (product.val())*1;
    }
    if (total > 0) total+=shipping;
    
    cart_item_price.html('$'+cm_number_format(cart_item, 2,'.',''));
    subtotal_price.html('$'+cm_number_format(subtotal, 2,'.',''));
    total_price.html('$'+cm_number_format(total, 2,'.',''));
    terms_price.html('$'+cm_number_format(total, 2,'.',''));
    checkout_price.val(cm_number_format(total, 2,'.',''));
}

products_selector = '.productInfoCont .checkbox input';
price_selector = '.price-item';
price_subtotal_selector = '.price-subtotal';
price_shipping_selector = '.price-shipping select';
price_total_selector = '.price-total';

function cm_init_products() { 
    $(products_selector).change(function(){
        cm_products_set_product($(this));
    });

    var product = $('#product3');
    product.attr('checked', 'checked');
    product.parents('.productInfoCont').addClass('selected'); 
    cm_products_set_product(product);
}

function cm_init_products_shipping(price_shipping_selector, price_subtotal_selector, price_total_selector) { 
    $(price_shipping_selector).change(function(){
        var total_price = $(price_total_selector);
        var shipping_price = $(price_shipping_selector);
        var subtotal_price = $(price_subtotal_selector);
        var total = total_price.html().replace('$', '') * 1;
        var shipping = shipping_price.val().replace('$', '') * 1;
        var subtotal = subtotal_price.html().replace('$', '') * 1;
        total = subtotal;
        total+= ($(this).val())*1;
        total_price.html('$'+cm_number_format(total, 2,'.',''));
    });
}

message = true; 
function cm_checkout_reset_message() { 
    message = true; 
}

function cm_init_checkout_form() { 
    var form = $('#cm-checkout-purchase-form');
    $(form).submit(function(e) { 
        if ($('#checkout-price').val()) { if (message) { alert('Please select a product.'); message = false; setTimeout(function() { cm_checkout_reset_message(); }, 5000); } return false; }
    });
}

// Browser Instructions
if ($('#browser_instructions').length) {
    var str1 = navigator.appVersion;
    var str2 = "Mac";
    var str3 = "Windows NT 5.1";
    var className = "none";
    if (str1.indexOf(str2) != -1) {
        className = 'mac';
    } else if (str1.indexOf(str3) != -1) {
        className = 'xp';
    }
    $("#browser_instructions").addClass(className);
}
