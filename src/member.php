<?php

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;

$app->match('/member', function(Request $request) use ($app) {
    if (!$app['security']->isGranted('ROLE_USER')) {
        return $app->redirect($app['url_generator']->generate('login'));
    }
    return member($app, $request, ['nextUrl' => 'homepage']);
})->bind('member');

$app->match('/search-history', function(Request $request) use ($app) {
    if (!$app['security']->isGranted('ROLE_USER')) {
        return $app->redirect($app['url_generator']->generate('login'));
    }
    return search_history($app, $request, ['nextUrl' => 'homepage']);
})->bind('search-history');

$app->match('/search-history-report/{id}', function($id, Request $request) use ($app) {
    if (!$app['security']->isGranted('ROLE_USER')) {
        return $app->redirect($app['url_generator']->generate('login'));
    }
    return search_history_report($app, $request, ['nextUrl' => 'homepage', 'id' => $id]);
})->bind('search-history-report');

$app->match('/order-history', function(Request $request) use ($app) {
    if (!$app['security']->isGranted('ROLE_USER')) {
        return $app->redirect($app['url_generator']->generate('login'));
    }
    return order_history($app, $request, ['nextUrl' => 'homepage']);
})->bind('order-history');

function member(Application $app, Request $request, $options)
{    
    //return $app['twig']->render('member/member.html.twig', ['options' => $options]);

    $m = new Database\MemberOrders($app);
    $u = $app['security']->getToken()->getUser()->getUsername();
    $rows = $m->getRowsByUser($u);
    return $app['twig']->render('member/search-history.html.twig', ['rows' => $rows, 'options' => $options]);
}

function search_history(Application $app, Request $request, $options)
{
    $m = new Database\MemberOrders($app);
    $u = $app['security']->getToken()->getUser()->getUsername();
    $rows = $m->getRowsByUser($u);
    return $app['twig']->render('member/search-history.html.twig', ['rows' => $rows, 'options' => $options]);
}

function search_history_report(Application $app, Request $request, $options)
{
    $m = new Database\MemberOrders($app);

    if ($report_row = $m->getRowById($options['id'])) {
        $app['session']->set('searchData', unserialize($report_row['report_data'])['data']);
        return $app->redirect($app['url_generator']->generate('report'));
    } else {
        return $app->redirect($app['url_generator']->generate('search-history'));
    }
}

function order_history(Application $app, Request $request, $options)
{
        
    return $app['twig']->render('member/order-history.html.twig', ['options' => $options]);
}