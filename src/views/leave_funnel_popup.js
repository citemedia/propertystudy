// http://stackoverflow.com/questions/10616437/javascript-how-to-call-a-function-when-we-choose-stay-on-page-in-chrome
var message = "\
***********************************************\n\n\
                         WAIT BEFORE YOU GO!\n\n\
***********************************************\n\n\
         CLICK THE *STAY ON THIS PAGE* BUTTON\n\n\
                    TO STAY ON THE CURRENT PAGE\n\n\
             ========> 80% DISCOUNT <========\n\n\
** Get A Property Report For Just $0.25... Now! **\n\n\
***********************************************\n\n";
var vals = 0;
var t = null;
var doredirect = true;
$(window).bind('beforeunload', function(ev) {
    if (typeof ev === 'undefined') {
        ev = window.event;
    }

    if (/Firefox[\/\s](\d+)/.test(navigator.userAgent) && new Number(RegExp.$1) >= 4) {
        message = message + 'Are you sure you want to leave?\n';
        if (confirm(message)) {
            doredirect = false;
        } else {
            doredirect = true;
            timed();
            ev.returnValue = message;
            return message;
        }
    } else {
        doredirect = true;
        timed();
        return message;
    }
});
function timed() {
    t = setTimeout("timed()", 100);
    if (vals > 0)
    {
        clearTimeout(t);
        to_discount();
    } else {
        vals++;
    }
}

function to_discount() {
    $(window).unbind('beforeunload');
    $.post("/to-discount", {});
    if (doredirect) {
        window.location.replace("{{ app.request.getSchemeAndHttpHost() ~ '/checkout-discount' }}");
    }
}

$(document).ready(function() {
    $('.unbind_unload').click(function(ev) {
        $(window).unbind('beforeunload');
    });
});