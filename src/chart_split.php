<?php

// templates views/chart_split

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use CM\Form\SearchForm;

$app->get('/searching-cs', function (Request $request) use ($app) {
    return searching_cs($app, $request, ['nextUrl' => 'requesting-cs']);
})->bind('searching-cs');

$app->get('/requesting-cs', function (Request $request) use ($app) {
    return requesting_cs($app, $request, ['nextUrl' => 'download-cs']);
})->bind('requesting-cs');

$app->match('/download-cs', function (Request $request) use ($app) {
    return download_cs($app, $request, ['nextUrl' => 'finished-cs']);
})->bind('download-cs');

$app->get('/finished-cs', function (Request $request) use ($app) {
    return finished_cs($app, $request, ['nextUrl' => 'cost-cs']);
})->bind('finished-cs');

$app->match('/cost-cs', function (Request $request) use ($app) {
    return cost_cs($app, $request, ['nextUrl' => 'checkout', 'type' => 'cost']);
})->bind('cost-cs');

$app->match('/warning-cs', function (Request $request) use ($app) {
    return warning_cs($app, $request, ['nextUrl' => 'select-payment-cs']);
})->bind('warning-cs');

$app->match('/select-payment-cs', function (Request $request) use ($app) {
    return select_payment_cs($app, $request, ['nextUrl' => 'new-or-existing-cs']);
})->bind('select-payment-cs');

$app->match('/new-or-existing-cs', function (Request $request) use ($app) {
    return new_or_existing_cs($app, $request, ['nextUrl' => 'feedback-cs']);
})->bind('new-or-existing-cs');

$app->match('/feedback-cs', function (Request $request) use ($app) {
    return feedback_cs($app, $request, ['nextUrl' => 'checkout']);
})->bind('feedback-cs');

function feedback_cs(Application $app, Request $request, Array $options)
{
    if (!$app['session']->get('searchData')) {
        return redirectHome($app);
    }

    if ('POST' == $request->getMethod()) {
        return $app->redirect($options['nextUrl']);
    }

    return $app['twig']->render('chart_split/feedback.html.twig', ['options' => $options]);
}

function new_or_existing_cs(Application $app, Request $request, Array $options)
{
    if (!$app['session']->get('searchData')) {
        return redirectHome($app);
    }

    if ('POST' == $request->getMethod()) {
        return $app->redirect($options['nextUrl']);
    }

    $app['session']->set('cardtype', $request->get('cardTypeR'));

    return $app['twig']->render('chart_split/new_or_existing.html.twig', ['options' => $options]);
}

function select_payment_cs(Application $app, Request $request, Array $options)
{
    if (!$app['session']->get('searchData')) {
        return redirectHome($app);
    }

    if ('POST' == $request->getMethod()) {
        return $app->redirect($options['nextUrl']);
    }

    return $app['twig']->render('chart_split/select_payment.html.twig', ['options' => $options]);
}

function warning_cs(Application $app, Request $request, Array $options)
{
    if (!$app['session']->get('searchData')) {
        return redirectHome($app);
    }

    if ('POST' == $request->getMethod()) {
        return $app->redirect($options['nextUrl']);
    }

    return $app['twig']->render('chart_split/warning.html.twig', ['options' => $options]);
}

function cost_cs(Application $app, Request $request, Array $options)
{
    if ($app['security']->isGranted('ROLE_USER')) {
        $app['session']->set('order', true);
        return $app->redirect('report');
    }

    if ('POST' == $request->getMethod()) {
        return $app->redirect('/warning-cs');
    }

    $searchData = $app['session']->get('searchData');

    $options['nextUrl'] = 'warning';
    return $app['twig']->render('chart_split/cost_of_report.html.twig', ['options' => $options, 'zip' => $searchData['zip']]);
}

function finished_cs(Application $app, Request $request, Array $options)
{
    if (!$app['session']->get('searchData')) {
        return redirectHome($app);
    }

    return $app['twig']->render('chart_split/finished_downloading.html.twig', ['options' => $options]);
}

function download_cs(Application $app, Request $request, Array $options)
{
    if (!$app['session']->get('searchData')) {
        return redirectHome($app);
    }

    return $app['twig']->render('chart_split/download.html.twig', ['options' => $options]);
}

function requesting_cs(Application $app, Request $request, Array $options)
{
    if (!$app['session']->get('searchData')) {
        return redirectHome($app);
    }

    return $app['twig']->render('chart_split/requesting_to_download.html.twig', ['options' => $options]);
}

function searching_cs(Application $app, Request $request, Array $options)
{
    if (!$app['session']->get('searchData')) {
        return redirectHome($app);
    }

    return $app['twig']->render('chart_split/searching.html.twig', ['options' => $options]);
}
