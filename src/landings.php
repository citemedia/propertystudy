<?php

use Silex\Application;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGenerator;
use CM\Form\SearchForm;

$app->match('/', function (Request $request) use ($app) {
    $app['session']->set('landing', null);
    if ($app['security']->isGranted('ROLE_USER')) {
        $url = 'report';
    } else {
        //$url = 'searching1';
        $url = 'upsell2bb';
    }
    return homepage($app, $request, ['nextUrl' => $url, 'index' => false, 'funnel' => 'root']);
})->bind('homepage1');

$app->match('/affiliate1', function (Request $request) use ($app) {
    $app['session']->set('landing', null);
    //return affiliate1($app, $request, ['nextUrl' => 'searching', 'index' => true, 'funnel' => 'propertyrecord77']);
    $affid = $request->get('affid');
    return affiliate1($app, $request, ['nextUrl' => 'searching', 'index' => true, 'funnel' => 'affiliate1', 'affid' => $affid]);
})->bind('affiliate1');

$app->match('/affiliate1/{affid}', function (Request $request, $affid) use ($app) {
    $app['session']->set('landing', null);
    //return affiliate1($app, $request, ['nextUrl' => 'searching', 'index' => true, 'funnel' => 'propertyrecord77']);
    return affiliate1($app, $request, ['nextUrl' => 'searching', 'index' => true, 'funnel' => 'affiliate1', 'affid' => $affid]);
})->bind('affiliate1_url');

$app->match('/propertyrecord77', function (Request $request) use ($app) {
    $app['session']->set('landing', null);
    return split_homepage($app, $request, ['nextUrl' => 'upsell2bb', 'index' => true, 'funnel' => 'propertyrecord77']);
})->bind('homepage');

$app->match('/checkout43', function (Request $request) use ($app) {
    $app['session']->set('landing', null);
    return split_homepage($app, $request, ['nextUrl' => 'searching', 'index' => true, 'funnel' => 'checkout43']);
})->bind('checkout43');

$app->match('/chart-split', function (Request $request) use ($app) {
    $app['session']->set('landing', null);
    return chart_split($app, $request, ['nextUrl' => 'searching-cs', 'index' => true, 'funnel' => 'chart-split']);
})->bind('chart-split');

$app->match('/emailcamp', function (Request $request) use ($app) {
    $app['session']->set('landing', 'emailcamp');
    return split_homepage($app, $request, ['nextUrl' => 'checkout', 'index' => true, 'funnel' => 'propertyrecord77']);
})->bind('emailcamp');

function affiliate1(Application $app, Request $request, Array $options)
{
    $s = $app['session'];

    $s->set('funnel', $options['funnel']);
    $s->set('property', null);
    $s->set('searchData', null);
    $s->set('searchString', null);
    $s->set('order', null);
    $s->set('upsellOrder', null);
    
    $s->set('upsella2_purchased', null);
    $s->set('upsella_purchased', null);

    $s->set('affid', $options['affid']);

    $form = $app['form.factory']->createBuilder(new SearchForm())->getForm();

    if ('POST' == $request->getMethod()) {

        $form->submit($request);

        if ($form->isValid()) {
            $data = $form->getData();
            $searchString = $data['street'] . ' ' . $data['city'] . ' ' . $data['state'] . ' ' . $data['zip'];

            $s->set('searchData', $data);
            $s->set('searchString', $searchString);
            
            return $app->redirect($app['url_generator']->generate($options['nextUrl']));
        }
    }

    return $app['twig']->render(
        'split/homepage.html.twig', [
            'searchForm' => $form->createView(),
            'options' => $options
        ]
    );
}

function split_homepage(Application $app, Request $request, Array $options)
{
    $session = $app['session'];

    $session->set('funnel', $options['funnel']);
    $session->set('property', null);
    $session->set('searchData', null);
    $session->set('searchString', null);
    $session->set('order', null);
    $session->set('upsellOrder', null);
    
    $form = $app['form.factory']->createBuilder(new SearchForm())->getForm();
    
    if ('POST' == $request->getMethod()) {

        //$form->submit($request);
        $r = $form->handleRequest($request);
        
        if ($form->isValid()) {
            $data = $form->getData();
            $searchString = $data['street'] . ' ' . $data['city'] . ' ' . $data['state'] . ' ' . $data['zip'];

            $session->set('searchData', $data);
            $session->set('searchString', $searchString);

            /* if ($app['security']->isGranted('ROLE_USER')) { */
            /*     $token = $app['security']->getToken(); */
            /*     $user = $token->getUser(); */
            /*     $provider = new \Database\UserProvider($app['db']); */
                
            /*     try { */
            /*         $provider->lastSearchUser($user->getUsername()); */
            /*     } catch (\Exception $e) {} */
            /* } */ 
            
            return $app->redirect($app['url_generator']->generate($options['nextUrl']));
        }
    }

    return $app['twig']->render(
        'split/homepage.html.twig', [
            'searchForm' => $form->createView(),
            'options' => $options
        ]
    );
}

function homepage(Application $app, Request $request, Array $options)
{
    $session = $app['session'];

    $app['session']->set('to_discount', false);

    $session->set('funnel', $options['funnel']);
    $session->set('property', null);
    $session->set('searchData', null);
    $session->set('searchString', null);
    $session->set('order', null);
    $session->set('upsellOrder', null);

    $form = $app['form.factory']
        ->createBuilder(new SearchForm())
        ->getForm();

    if ('POST' == $request->getMethod()) {

        $form->submit($request);

        if ($form->isValid()) {
            $data = $form->getData();
            $searchString = $data['street'] . ' ' . $data['city'] . ' ' . $data['state'] . ' ' . $data['zip'];

            $session->set('searchData', $data);
            $session->set('searchString', $searchString);

            if ($app['security']->isGranted('ROLE_USER')) {
                return $app->redirect('report');
            } else {
                return $app->redirect($options['nextUrl']);
            }
        }
    }

    $html = $app['twig']->render('cs_homepage.html.twig', [
        'searchForm' => $form->createView(),
        'options' => $options
    ]
    );
    $r = new Response($html);
    $r->headers->clearCookie('upsell_split_visitor');
    return $r;
}

function chart_split(Application $app, Request $request, $options)
{
    $session = $app['session'];

    $session->set('funnel', $options['funnel']);
    $session->set('property', null);
    $session->set('searchData', null);
    $session->set('searchString', null);
    $session->set('order', null);
    $session->set('upsellOrder', null);

    $form = $app['form.factory']
            ->createBuilder(new SearchForm())
            ->getForm();

    if ('POST' == $request->getMethod()) {

        $form->submit($request);

        if ($form->isValid()) {
            $data = $form->getData();
            $searchString = $data['street'] . ' ' . $data['city'] . ' ' . $data['state'] . ' ' . $data['zip'];

            $session->set('searchData', $data);
            $session->set('searchString', $searchString);

            return $app->redirect($options['nextUrl']);
        }
    }

    return $app['twig']->render('chart_split/main.html.twig', [
        'searchForm' => $form->createView(),
        'options' => $options
    ]);
}