<?php

use Silex\Application;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGenerator;
use CM\Form\OrderForm;
use CM\Service\TriangleApi;

function checkout_affid(Application $app, Request $request, Array $options)
{
    $s = $app['session'];
    if ($app['security']->isGranted('ROLE_USER')) {
        $s->set('order', true);
        return $app->redirect('report');
    }
    if (!$s->get('searchData')) {
        return redirectHome($app);
    }
    if ($s->get('order')) {
        return $app->redirect($options['nextUrl']);
    }
    
    $s->set('checkout_discount', null);
    
    $form = $app['form.factory']->createBuilder(new OrderForm())->getForm();

    if ('POST' == $request->getMethod()) {

        $form->submit($request);

        if ($form->isValid()) {

            $data = $form->getData();
            $s->set('checkout_data', $data);

            $api = new TriangleApi($app);
            // if successful, sets 'prospectID' session var
            $api->setProspectID($data);
            $result = $api->simpleOrder($data, $s->get('searchString'));

            if ($result->State == 'Error') {

                crm_fail('/checkout', $result);
                    
                return $app['twig']->render('checkout.html.twig', ['order_form' => $form->createView(), 'error' => 'Something went wrong', 'options' => $options]);
            }

            $s->set('customerData', $data);
            $s->set('order', $result);
            $s->set('upsell_decline', false);

            //return $app->redirect($options['nextUrl']);
            //return $app->redirect('upsella.1');
            return $app->redirect('upsella.2');
        }
    }

    return $app['twig']->render('checkout.html.twig', ['order_form' => $form->createView(), 'options' => $options]);
}