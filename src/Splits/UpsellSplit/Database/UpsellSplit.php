<?php

namespace Splits\UpsellSplit\Database;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;

class UpsellSplit
{

    public $app;
    public $conn;
    public $tablename = 'upsell_split_visitors4';

    function __construct(\Silex\Application $app)
    {
        $this->app = $app;
        $this->conn = $app['db'];
    }

    public function logVisitor($upsell2a_price, $upsell2a_visit, $upsell2b_price, $upsell2b_visit)
    {
        $vwo_ips = [ '89.28.47.80',
        '184.173.187.125',
        '184.173.187.124',
        '184.173.170.150',
        '184.154.216.54',
        '184.154.191.166',
        '184.154.216.57',
        '66.219.167.38',
        '184.154.216.62'];
        $ip = $this->app['request']->getClientIp();
        if (in_array($ip, $vwo_ips)) {
            return;
        }

        $tablename = $this->tablename;

        $sql = "INSERT INTO {$tablename} (upsell2a_price, upsell2a_visit, upsell2b_price, upsell2b_visit, ip)
VALUES ('{$upsell2a_price}', '{$upsell2a_visit}', '{$upsell2b_price}', '{$upsell2b_visit}', '{$ip}');"

        try {
            $this->conn->exec($sql);
        } catch (\Exception $e) {
            $this->handleErrors($e->getMessage());
            return false;
        }
        return true;
    }

    private function createTable()
    {
        $tablename = $this->tablename;

        $sql ="CREATE TABLE IF NOT EXISTS `{$tablename}` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`upsell2a_price` varchar(6) DEFAULT NULL,
`upsell2a_visit` datetime DEFAULT NULL,
`upsell2b_price` varchar(6) DEFAULT NULL,
`upsell2b_visit` datetime DEFAULT NULL,
`ip` varchar(16) NOT NULL,
PRIMARY KEY (`id`),
KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1;"

            try {
            $this->conn->exec($sql);
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    private function handleErrors($msg)
    {
        //SQLSTATE error codes
        if (strripos($msg, '42s02') !== false) {
            // table not found
            $this->createTable();
        }
        if (strripos($msg, '42000') !== false) {
            // syntax error or access rule violation
            // incorrect /config/config.yaml
        }
    }
}
