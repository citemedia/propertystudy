<?php

namespace Database;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Doctrine\DBAL\Connection;
use Symfony\Component\Security\Core\User\User;

class UserProvider implements UserProviderInterface
{

    private $conn;
    public static $tablename = 'users';

    public function __construct(Connection $conn)
    {
        $this->conn = $conn;
        if (!$this->tableExists()) {
            $this->createTable();
        }
    }

    public function loadUserByUsername($username)
    {
        $stmt = $this->conn->executeQuery('SELECT * FROM users WHERE username = ?', array(strtolower($username)));

        if (!$user = $stmt->fetch()) {
            throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $username));
        }

        return new User($user['username'], $user['password'], explode(',', $user['roles']), true, true, true, true);
    }

    public function lastLoginUser($username)
    {
        $date = new \DateTime();
        $date->setTimestamp(time());
        $last_login = $date->format('Y-m-d H:i:s');

        $stmt = $this->conn->executeQuery("UPDATE users SET last_login='{$last_login}' WHERE username = ?", array(strtolower($username)));

        if (!$stmt->fetch()) {
            throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $username));
        }
    }

    public function lastSearchUser($username)
    {
        $date = new \DateTime();
        $date->setTimestamp(time());
        $last_login = $date->format('Y-m-d H:i:s');

        $stmt = $this->conn->executeQuery("UPDATE users SET last_visit='{$last_login}' WHERE username = ?", array(strtolower($username)));

        if (!$stmt->fetch()) {
            throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $username));
        }
    }

    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    public function supportsClass($class)
    {
        return $class === 'Symfony\Component\Security\Core\User\User';
    }
    
    public function createUser($data, $prospectId)
    {
        $date = new \DateTime();
        $date->setTimestamp(time());
        $last_visit = $date->format('Y-m-d H:i:s');
        
        $tablename = self::$tablename;
        $sql = <<<SQL
INSERT INTO `{$tablename}` (`id`, `username`, `roles`, `password`, `email`, `is_active`, `salt`, `last_visit`, `last_login`) VALUES (NULL, '{$data['emailAddress']}', 'ROLE_USER', '{$data['password']}', '{$data['emailAddress']}', '1', NULL, '{$last_visit}', '{$last_visit}');
SQL;
        try {
            $rows = $this->conn->exec($sql);
            if ($rows > 0) { return true; }
        } catch (\Exception $e) {}
        
        return false;
    }
    
    private function tableExists()
    {
        $tablename = self::$tablename;
        $sql = <<<SQL
SHOW TABLES LIKE `{$tablename}`;
SQL;
        try {
            $rows = $this->conn->exec($sql);
            if ($rows > 0) { return true; }
        } catch (\Exception $e) {}
        
        return false;
    }
    
    private function createTable()
    {
        $tablename = self::$tablename;
        
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `{$tablename}` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(127) NOT NULL,
  `roles` varchar(255) DEFAULT 'ROLE_USER',
  `password` varchar(127) NOT NULL,
  `email` varchar(127) NOT NULL,
  `is_active` TINYINT(1) NOT NULL,
  `salt` varchar(127) DEFAULT NULL,
  `last_visit` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;
SQL;
        try {
            $this->conn->exec($sql);
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }
}
