<?php

namespace Database;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;

class MemberOrders
{    
    public $app;
    public $conn;
    private $tablename = 'member_orders';

    function __construct(\Silex\Application $app)
    {
        $this->app = $app;
        if (!isset($app['db'])) {
            $app->register(new \Silex\Provider\DoctrineServiceProvider(), array(
                'db.options' => $app['config']['database']
            ));
        }
        $this->conn = $app['db'];
    }

    public function getRowById($id)
    {
        $sql = "SELECT * FROM {$this->tablename} WHERE `id`='{$id}';";
        try {
            $result = $this->conn->fetchAssoc($sql);
        } catch (\Exception $e) {
            $this->handleErrors($e->getMessage());
            return false;
        }
        return $result;
    }

    public function getRowsByUser($username)
    {
        $sql = "SELECT * FROM {$this->tablename} WHERE `username`='{$username}';";
        try {
            $result = $this->conn->fetchAll($sql);
        } catch (\Exception $e) {
            $this->handleErrors($e->getMessage());
            return false;
        }
        return $result;
    }

    public function getRowByUserAndAddress($username, $data)
    {
        $sql = "SELECT * FROM {$this->tablename} WHERE `username`='{$username}' AND `address`='{$data['street']}' AND `city`='{$data['city']}' AND `state`='{$data['state']}' AND `zip`='{$data['zip']}' LIMIT 1;";
        try {
            return $this->conn->fetchAssoc($sql);
        } catch (\Exception $e) {
            $this->handleErrors($e->getMessage());
            return false;
        }
    }
    
    public function addRow($page, $report_data, $report_version, $product = NULL)
    {
        $date = new \DateTime();
        $date->setTimestamp(time());
        $added_on = $date->format('Y-m-d H:i:s');
        $tablename = $this->tablename;

        $report_data = serialize($report_data);

        $searchData = $this->app['session']->get('searchData');
        $address = $searchData['street'];
        $city = $searchData['city'];
        $state = $searchData['state'];
        $zip = $searchData['zip'];

        if ($this->app['security']->isGranted('ROLE_USER')) {
            $user = $this->app['security']->getToken()->getUser();
            $username = $user->getUsername(); 
        } else {
            $username = $this->app['request']->getClientIp();
        }

        $funnel = $this->app['session']->get('funnel');
        $sql = "INSERT INTO {$tablename} (username, page, funnel, address, city, state, zip, product, report_data, report_version, added_on) VALUES ('{$username}', '{$page}', '{$funnel}', '{$address}', '{$city}', '{$state}', '{$zip}', '{$product}', '{$report_data}', '{$report_version}', '{$added_on}');";
        
        try {
            $this->conn->exec($sql);
        } catch (\Exception $e) {
            $this->handleErrors($e->getMessage());
            return false;
        }
        return true;
    }

    private function createTable()
    {
        $sql = "
CREATE TABLE IF NOT EXISTS `{$this->tablename}` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(63) DEFAULT NULL,
  `page` varchar(63) NOT NULL,
  `funnel` varchar(63) NOT NULL,
  `address` varchar(127) NOT NULL,
  `city` varchar(127) NOT NULL,
  `state` varchar(127) NOT NULL,
  `zip` varchar(127) NOT NULL,
  `product` varchar(31) DEFAULT NULL,
  `report_data` TEXT NOT NULL,
  `report_version` int(2) NOT NULL,
  `added_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1;";
        try {
            $this->conn->exec($sql);
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }
    
    private function handleErrors($msg)
    {
        //SQLSTATE error codes
        if (strripos($msg, '42s02') !== false) {
            // table not found
            $this->createTable();
        }
        if (strripos($msg, '42000') !== false) {
            // syntax error or access rule violation
            // incorrect /config/config.yaml
        }
    }
}