<?php

namespace Database;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;

class TriangleFailures
{    
    public $app;
    public $conn;
    private $tablename = 'triangle_failures';

    function __construct(\Silex\Application $app)
    {
        $this->app = $app;
        if (!isset($app['db'])) {
            $app->register(new \Silex\Provider\DoctrineServiceProvider(), array(
                'db.options' => $app['config']['database']
            ));
        }
        $this->conn = $app['db'];
    }

    public function getRows()
    {
        $sql = "SELECT `id`, `username`, `page`, `error_text`, `mid`, `type`, `added_on` FROM {$this->tablename};";
        try {
            $result = $this->conn->fetchAll($sql);
        } catch (\Exception $e) {
            $this->handleErrors($e->getMessage());
            return false;
        }
        return $result;
    }

    public function getResponse($id)
    {
        $sql = "SELECT `response` FROM {$this->tablename} WHERE `id`='{$id}';";
        try {
            $result = $this->conn->fetchArray($sql);
        } catch (\Exception $e) {
            $this->handleErrors($e->getMessage());
            return false;
        }
        return $result;
    }
    
    public function addRow($page, $request_data, $result)
    {
        $date = new \DateTime();
        $date->setTimestamp(time());
        $added_on = $date->format('Y-m-d H:i:s');
        $tablename = $this->tablename;
        if (isset($result->ReturnValue->TrialCharge)) {
            $mid = $result->ReturnValue->TrialCharge->MID;
            $type = 'subscription';
        } else {
            $mid = $result->ReturnValue->MID;
            $type = 'sale';
        }

        if (!property_exists($result, 'ReturnValue') && property_exists($result, 'ErrorMessage')) {
            $error_text = $result->ErrorMessage;
        } else {
            parse_str($result->ErrorMessage, $vars);
            $error_text = $vars['responsetext'];
        }
        
        $response = serialize($result);

        if ($this->app['security']->isGranted('ROLE_USER')) {
            $user = $this->app['security']->getToken()->getUser();
            $username = $user->getUsername(); 
        } else {
            $username = $this->app['request']->getClientIp();
        }
        $sql = "INSERT INTO {$tablename} (username, page, error_text, request_data, response, mid, type, added_on) VALUES ('{$username}', '{$page}', '{$error_text}', '{$request_data}', '{$response}', '{$mid}', '{$type}', '{$added_on}');";
        
        try {
            $this->conn->exec($sql);
        } catch (\Exception $e) {
            $this->handleErrors($e->getMessage());
            return false;
        }
        return true;
    }

    private function createTable()
    {
        $sql = "
CREATE TABLE IF NOT EXISTS `{$this->tablename}` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(63) DEFAULT NULL,
  `page` varchar(63) NOT NULL,
  `error_text` varchar(255) NOT NULL,
  `mid` varchar(63) DEFAULT NULL,
  `request_data` varchar(511) NOT NULL,
  `type` varchar(14) NOT NULL,
  `response` varchar(1023) NOT NULL,
  `added_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1;";
        try {
            $this->conn->exec($sql);
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }
    
    private function handleErrors($msg)
    {
        //SQLSTATE error codes
        if (strripos($msg, '42s02') !== false) {
            // table not found
            $this->createTable();
        }
        if (strripos($msg, '42000') !== false) {
            // syntax error or access rule violation
            // incorrect /config/config.yaml
        }
    }
}