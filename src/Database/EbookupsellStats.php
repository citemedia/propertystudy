<?php

namespace Database;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;

class EbookupsellStats
{
    
    public $app;
    public $conn;
    public static $tablename = 'ebookupsell_visitors'; // static is important, used in cron script
    public $cookie_name = 'ebook_visitor';

    function __construct(\Silex\Application $app)
    {
        $this->app = $app;
//        if (!isset($app['db'])) {
//            $app->register(new \Silex\Provider\DoctrineServiceProvider(), array(
//                'db.options' => $app['config']['database']
//            ));
//        }
        $this->conn = $app['db'];
    }
    
    private function updateRow($cookie_id, $bought_ebook)
    {
        $date = new \DateTime();
        $date->setTimestamp(time());
        $last_visit = $date->format('Y-m-d H:i:s');
        $tablename = self::$tablename;
        $ip = $this->app['request']->getClientIp();
        
        $sql = <<<SQL
INSERT INTO {$tablename} (cookie_id, bought_ebook, last_visit, ip)
VALUES ('{$cookie_id}', '{$bought_ebook}', '{$last_visit}', '$ip')
ON DUPLICATE KEY UPDATE cookie_id='{$cookie_id}', bought_ebook='{$bought_ebook}', last_visit='{$last_visit}', ip='{$ip}';
SQL;
        try {
            $this->conn->exec($sql);
        } catch (\Exception $e) {
            $this->handleErrors($e->getMessage());
            return false;
        }
        return $cookie_id;
    }

    private function createTable()
    {
        $tablename = self::$tablename;
        
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `{$tablename}` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bought_ebook` tinyint(1) DEFAULT 0,
  `last_visit` datetime NOT NULL,
  `cookie_id` varchar(23) NOT NULL,
  `ip` varchar(16) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cookie_id` (`cookie_id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;
SQL;
        try {
            $this->conn->exec($sql);
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }
    
    private function handleErrors($msg)
    {
        //SQLSTATE error codes
        if (strripos($msg, '42s02') !== false) {
            // table not found
            $this->createTable();
        }
        if (strripos($msg, '42000') !== false) {
            // syntax error or access rule violation
            // incorrect /config/config.yaml
        }
    }
    
    public function updateVisitor(Response $response, $bought_ebook)
    {
        $cookie_id = $this->app['request']->cookies->get($this->cookie_name);
        if (!$cookie_id) {
            $cookie_id = uniqid();
        }
        //$cookie_id = uniqid();
        $this->updateRow($cookie_id, $bought_ebook);
        $expire = new \DateTime();
        $expire->setTimestamp(time() + 2764800); // current timestamp + 32 days in seconds 
        $cookie = new Cookie($this->cookie_name, $cookie_id, $expire);
        $response->headers->setCookie($cookie);
        
        return $response;
    }

}