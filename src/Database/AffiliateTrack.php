<?php

namespace Database;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;

class AffiliateTrack
{

    public $app;
    public $conn;
    public $tablename = 'affiliate_track';

    function __construct(\Silex\Application $app)
    {
        $this->app = $app;
        $this->conn = $app['db'];
    }

    public function logVisitor()
    {
        $s = $this->app['session'];
        $affid = $s->get('affid');
        if (!$affid) {return false;}
        $tablename = $this->tablename;
        $checkout = $s->get('order')->ReturnValue->Amount;
        $specialoffer_upsell = (boolean)$s->get('upsellOrder');
        $upsella2 = $s->get('upsella2_purchased');
        $upsella = $s->get('upsella_purchased');
        $ebookspecial = $s->get('purchased_ebookspecial');
        $date = new \DateTime();
        $date->setTimestamp(time());
        $report_view_time = $date->format('Y-m-d H:i:s');
        $ip = $this->app['request']->getClientIp();

        $sql = "
INSERT INTO {$tablename} (affid, checkout, specialoffer_upsell, upsella2, upsella, ebookspecial, report_view_time, ip)
VALUES ('{$affid}', '{$checkout}', '{$specialoffer_upsell}', '{$upsella2}', '{$upsella}', '{$ebookspecial}', '{$report_view_time}', '{$ip}');";
        try {
            $this->conn->exec($sql);
        } catch (\Exception $e) {
            $this->handleErrors($e->getMessage());
            return false;
        }
        $s->set('upsella2_purchased', null);
        $s->set('upsella_purchased', null);
        return true;
    }

    private function createTable()
    {
        $tablename = $this->tablename;

        $sql = "
CREATE TABLE IF NOT EXISTS `{$tablename}` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`affid` varchar(3) NOT NULL,
`checkout` varchar(4) DEFAULT NULL,
`specialoffer_upsell` varchar(4) DEFAULT NULL,
`upsella2` varchar(4) DEFAULT NULL,
`upsella` varchar(4) DEFAULT NULL,
`ebookspecial` varchar(4) DEFAULT NULL,
`report_view_time` datetime DEFAULT NULL,
`ip` varchar(16) NOT NULL,
PRIMARY KEY (`id`),
KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1;";

        try {
            $this->conn->exec($sql);
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    private function handleErrors($msg)
    {
        //SQLSTATE error codes
        if (strripos($msg, '42s02') !== false) {
            // table not found
            $this->createTable();
        }
        if (strripos($msg, '42000') !== false) {
            // syntax error or access rule violation
            // incorrect /config/config.yaml
        }
    }
}
