<?php

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
//use Symfony\Component\HttpFoundation\Response;
use CM\Form\MakePasswordForm;
use CM\Form\UnlimitedSpecialForm;
use CM\Service\UnlimitedSpecialTriangleApi;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

$app->match('/unlimited4.99special', function (Request $request) use ($app) {
    return homepage_checkout($app, $request, ['nextUrl' => 'make-password', 'index' => true]);
})->bind('unlimited_special')->requireHttps();

$app->match('/make-password', function (Request $request) use ($app) {
    return make_password($app, $request, ['nextUrl' => '', 'index' => true]);
})->bind('make-password')->requireHttps();

function homepage_checkout(Application $app, Request $request, $options)
{
    $app['session']->set('funnel', 'unlimited_special');
    
    $form = $app['form.factory']
        ->createBuilder(new UnlimitedSpecialForm())
        ->getForm();
    if ($app['session']->get('unlimited_special_data')) {
        $form->setData($app['session']->get('unlimited_special_data'));
    }

    if ('POST' == $request->getMethod()) {
        $form->submit($request);
        if ($form->isValid()) {
            $data = $form->getData();
            $app['session']->set('unlimited_special_data', $data);
            $api = new UnlimitedSpecialTriangleApi($app);
            $api->setProspectID($data);
            return $app->redirect($app['url_generator']->generate($options['nextUrl']));
        }
    }
    
    return $app['twig']->render('unlimited_special/checkout.html.twig', ['form' => $form->createView(), 'error' => $app['session']->get('unlimited_special_checkout_error')]);
}

function make_password(Application $app, Request $request)
{
    if (!$app['session']->get('unlimited_special_data')) {
        $app->redirect($app['url_generator']->generate('unlimited_special'));
        return;
    }

    $form = $app['form.factory']
        ->createBuilder(new MakePasswordForm())
        ->getForm();
    if ($app['session']->get('unlimited_special_login_data')) {
        $form->setData($app['session']->get('unlimited_special_login_data'));
    }

    if ('POST' == $request->getMethod()) {
        $form->submit($request);

        if ($form->isValid()) {
            $api = new UnlimitedSpecialTriangleApi($app);
            
            $data = array_merge($form->getData(), $app['session']->get('unlimited_special_data'));

            switch ($data['product']) {
                case 1:
                    $result = $api->packageOne($data, 'unlimited_special');
                    break;
                case 2:
                    $result = $api->packageTwo($data, 'unlimited_special');
                    break;
                case 3:
                    $result = $api->packageThree($data, 'unlimited_special');
                    break;
            }
            if ($result->State == 'Error') {
                $app['session']->set('unlimited_special_checkout_error', $result->ErrorMessage);
                $app['session']->set('unlimited_special_login_data', $form->getData());
                return $app->redirect($app['url_generator']->generate('unlimited_special'));
            }

            $app['security.user_provider.subscribed']->createUser($data, $app['session']->get('prospectID'));
            $app['session']->set('unlimited_special_data', false);
            $app['session']->set('unlimited_special_checkout_error', false);
            $app['session']->set('unlimited_special_login_data', false);
            
            // login user
            $user = $app['security.user_provider.subscribed']->loadUserByUsername($data['emailAddress']);
            $token = new UsernamePasswordToken($user, null, "subscribed", $user->getRoles());
            $app['security']->setToken($token); //now the user is logged in
            $event = new InteractiveLoginEvent($request, $token);
            $app['dispatcher']->dispatch("security.interactive_login", $event);

            return $app->redirect($app['url_generator']->generate('homepage1'));
        } else {
            return $app['twig']->render('unlimited_special/makepassword.html.twig', ['form' => $form->createView()]);
        }
    }

    return $app['twig']->render('unlimited_special/makepassword.html.twig', ['form' => $form->createView()]);
}
