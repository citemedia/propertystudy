<?php
namespace Cloud;

// Assetic doesn't define an interface for this, more like a suggestion
// We copied their function names anyways for interchangability
class CloudFilesAssetWriter extends \Assetic\AssetWriter
{
    /**
     * When $quickCheck is true we do not check for individual file existance.  The mere presence
     * of the container is enough to allow a pass.
     */
    public $quickCheck = true;

    /**
     * The number of children to use for parallel tasks
     */
    public $maxChildren = 8;

    public function __construct(CloudExtension $cfExtension, \Silex\Application $app)
    {
        $this->app = $app;
        $this->cfExtension = $cfExtension;
    }

    /**
     *
     */
    public function writeManagerAssets(\Assetic\AssetManager $am)
    {
        // rackspace is slow, so parallelize this per-base directory
        $queue = $this->buildQueue($am);

        $children = array();
        $queueSize = count($queue);

        if (! $queueSize) {
            return;
        }

        // open the connection to share the same auth between all children
        $unused = $this->app['cf.connection'];

        foreach ($queue as $name => $assets) {
            --$queueSize;

            $pid = pcntl_fork();
            if ($pid === -1) {
                die('could not fork');
            } elseif ($pid) {
                $children[$pid] = $name;
            } else {
                echo "Starting dump for $name\n";
                $container = $this->cfExtension->getCdnContainer($assets[0]->getTargetPath(), true);
                $assets = $this->filterAssets($container, $assets);
                $assetsLength = count($assets);
                foreach ($assets as $asset) {
                    --$assetsLength;
                    $this->writeAsset2($asset, $container);
                    echo "$name: $assetsLength assets remain\n";
                }
                exit;
            }

            while (count($children) >= $this->maxChildren) {
                echo "Waiting on " . count($children) . " children.  $queueSize directories remain.\n";
                $pid = $this->waitChildren($children);
                unset($children[$pid]);
            }
        }
        while ($children) {
            echo "Waiting on " . count($children) . " children.  No directories remain.\n";
            $pid = $this->waitChildren($children);
            unset($children[$pid]);
        }
    }

    /**
     *
     */
    protected function waitChildren($children)
    {
        if (! $children) {
            return false;
        }

        while (true) {
            $pid = pcntl_wait($status);
            if (pcntl_wifsignaled($status)) {
                echo "Signaled";
            } elseif (pcntl_wifexited($status)) {
                echo $children[$pid] . " Completed\n";

                return $pid;
            } else {
                echo "Other status ($status)\n";
            }
        }
    }

    public function writeAsset2(\Assetic\Asset\AssetInterface $asset, \CF_Container $container)
    {
        $path = $asset->getTargetPath();
        $cdnFile = $container->create_object($path);
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        // TODO: beter content type detection
        switch (strtolower($ext)) {
            case 'css':
                $cdnFile->content_type = 'text/css';
                break;
            case 'js':
                $cdnFile->content_type = 'application/javascript';
                break;
            case 'swf':
                $cdnFile->content_type = 'application/x-shockwave-flash';
                break;
            case 'ttf':
                $cdnFile->content_type = 'application/x-font-ttf';
                break;
            case 'woff':
                $cdnFile->content_type = 'application/font-woff';
                break;
            case 'eot':
                $cdnFile->content_type = 'application/vnd.ms-fontobject';
                break;
            case 'svg':
                $cdnFile->content_type = 'image/svg+xml';
                break;
            case 'jpg':
            case 'jpeg':
                $cdnFile->content_type = 'image/jpeg';
                break;
            case 'png':
                $cdnFile->content_type = 'image/png';
                break;
        }
        echo "Dumping: $path container '{$cdnFile->container->name}' : {$cdnFile->public_uri()}\n";
        try {
            list($handle, $filesize) = $this->assetDataFileHandle($asset);
            $cdnFile->write($handle, $filesize);
        } catch (\BadContentTypeException $e) {
            fclose($handle);
            throw new \Exception("No content-type detected for {$asset->getTargetPath()}");
        }
        fclose($handle);
    }

    protected function assetDataFileHandle(\Assetic\Asset\AssetInterface $asset)
    {
        $data = $asset->dump();
        $handle = fopen('php://temp', 'rw+');
        fwrite($handle, $data);
        rewind($handle);

        return array($handle, strlen($data));;
    }

    /**
     *
     */
    protected function buildQueue(\Assetic\AssetManager $am)
    {
        $queue = array();
        $child = 0;
        foreach ($am->getNames() as $name) {
            if (++$child > $this->maxChildren) {
                $child = 0;
            }
            $queue["child $child"][] = $am->get($name);
        }

        return $queue;
    }

    /**
     *
     */
    protected function filterContainers(\CF_Connection $connection, array $containers)
    {
        if (! $containers) {
            return;
        }

        echo "Pre-filtering containers\n";
        $length = count($containers);
        foreach ($this->app['cf']->list_containers() as $name) {
            if (isset($containers[$name])) {
                echo "Skipped Container: $name\n";
                unset($containers[$name]);
            }
        }
        echo ($length - count($containers)) . " containers filtered\n";

        return $containers;
    }

    /**
     *
     */
    protected function filterAssets(\CF_Container $container, array $assets)
    {
        if (! $assets) {
            return;
        }
        echo "Pre-filtering assets\n";
        // array flip so we can search name by key
        $files = array_flip($container->list_objects());

        $length = count($assets);
        foreach ($assets as $name => $asset) {
            if (isset($files[$asset->getTargetPath()])) {
                unset($assets[$name]);
            }
        }
        echo ($length - count($assets)) . " assets filtered\n";

        return $assets;
    }
}
