<?php

namespace Cloud;

use Silex\Application,
    Silex\ServiceProviderInterface,
    Symfony\Component\HttpFoundation\RedirectResponse,
    Symfony\Component\Finder\Finder,
    Assetic\Filter\Yui,
    Assetic\Asset\GlobAsset,
    Assetic\Asset\FileAsset;

/**
 * An Extension importing various cloud services
 * Currently:
 *    rackspace cloud files + assetic integration
 *    statsd
 *    mixpanel
 */
class CloudExtension implements ServiceProviderInterface
{
    private $cdnContainers = array();

    public function register(Application $app)
    {
        $this->registerVisualWebsiteOptimizer($app);
        $this->registerStatsD($app);
        $this->registerMixPanel($app);
        $this->registerCloudFiles($app);
    }

    public function registerMixPanel($app)
    {
        if (isset($app['mixPanel'])) {
            return;
        }

        $app['mixPanel.token'] = '300958933f9b2d6d06bf1e36eeda8b7d';
        $app['mixPanel'] = $app->share(function() use ($app) {
            return new MixPanelTracker($app['mixPanel.token']);
        });
    }

    public function registerStatsD($app)
    {
        if (isset($app['statsd'])) {
            return;
        }

        $app['statsd.host'] = '10.179.38.6';
        $app['statsd.port'] = 8125;
        $app['statsd.prefix'] = $app->share(function() use ($app) {
            // Basically this extracts a unique name for the currently configured
            // website (based on host+url) to use as a stats prefix
            $prefix = strtolower(strtr($app['request']->getHost(), '.', '_'));
            if ($app['dm.config']->urlPrefix !== '/') {
                $prefix .= strtolower(strtr($app['dm.config']->urlPrefix, '/', '_'));
            }

            return 'website.homeincome.prod.' . $prefix;
        });
        $app['statsd'] = $app->share(function() use ($app) {
            return new StatsD($app['statsd.host'], $app['statsd.port'], $app['statsd.prefix']);
        });
    }

    public function registerVisualWebsiteOptimizer($app)
    {
        if (isset($app['vwo'])) {
            return;
        }

        $app['vwo'] = $app->share(function() use ($app) {
            return new VisualWebsiteOptimizer;
        });
        $app['dispatcher']->addListener('kernel.response', function($event) use ($app) {
            $app['vwo']->onKernelResponse($event);
        });
    }

    /**
     * Register rackspace cloud files + assetic integration
     */
    public function registerCloudFiles(Application $app)
    {
        if (isset($app['cf'])) {
            return;
        }
        $this->app = $app;

        $app['cf.extension'] = $this;
        $app['cf.connection'] = $app->share(function() use ($app) {
            require_once $app['cf.include'];

            $retry = 3;
            while ($retry) {
                try {
                    $auth = new \CF_Authentication($app['cf.username'], $app['cf.api_key']);
                    $auth->authenticate();

                    return new \CF_Connection($auth);
                } catch (\InvalidResponseException $e) {
                    --$retry;
                }
            }

            throw $e;
        });
        // $app['cf'] is aliased shortcut
        $app['cf'] = $app->raw('cf.connection');

        // Replace assetic asset write with a cloud files writer
        $app['assetic.asset_writer'] = $app->share(function() use ($app) {
            return new CloudFilesAssetWriter($app['cf.extension'], $app);
        });

        // Add asset filter to twig
        // Can this be done without instantiating twig?
            $app['twig']->addExtension(new TwigAssetExtension($this, $app));
    }

    /**
     * Naming Strategy for converting path -> container name
     */
    public function getCdnContainerName($path, $createVersion=false)
    {
        // For now, single container
        $dir = '';

        /*
        $dir = ltrim(pathinfo($path, PATHINFO_DIRNAME), '/');

        if (empty($dir) || $dir === '.' || substr($dir, 0, 2) === '..') {
            throw new \InvalidArgumentException("No files are allowed in the root: $path");
        }
        */

        $version = $this->getAssetVersion($this->app['cf.assets.base_dir'], $createVersion);
        $cleanDir = strtr($dir, '/', '_');

        return "home_income:$cleanDir:$version";
    }

    /**
     * Applies the naming strategy to $path to get a container name
     * Wraps cf->get_container with retry and caching
     *
     * @param string  $path            The location of the file relative to $app['cf.assets.base_url']
     * @param boolean $createContainer Create the container if it doesn't already exist
     *
     * @throws \InvalidResponseException cdn api failed after multiple retrys
     * @throws \NoSuchContainerException container did not exist, and $createContainer = false
     */
    public function getCdnContainer($path, $createContainer = false)
    {
        $name = $this->getCdnContainerName($path);
        if (isset($this->cdnContainers[$name])) {
            return $this->cdnContainers[$name];
        }

        $cf = $this->app['cf'];

        $retry = 3;
        while ($retry) {
            try {
                try {
                    $container = $cf->get_container($name);
                } catch (\NoSuchContainerException $e) {
                    if (! $createContainer) {
                        throw $e;
                    }
                    $container = $cf->create_container($name);
                    $container->make_public();
                }
                $retry = 0;
            } catch (\InvalidResponseException $e) {
                $container = null;
                --$retry;
            }
        }

        if ($container === null) {
            throw $e;
        }

        return $this->cdnContainers[$name] = $container;
    }

    /**
     * Reads a file named 'assets.version' in the directory.  Only the first line
     * is considered the version
     */
    public function getAssetVersion($dir, $createVersion = false)
    {
        $versionPath = $dir."assets.version";
        $versionFile = @file($versionPath);

        if ($versionFile) {
            $version = trim($versionFile[0]);
        } elseif (! $createVersion) {
            throw new AssetNotVersionedException("Directory is not versioned: " . (realpath($dir) ? : $dir));
        } else {
            $version = 1;
            file_put_contents($versionPath, $version);
        }

        return $version;
    }

    /**
     * Load all applicable assets into the asset manager
     * and return it
     */
    public function getAssetManager()
    {
        $app = $this->app;

        $am = $app['assetic.asset_manager'];
        $fm = $app['assetic.filter_manager'];

        $fm->set('yui_css', new Yui\CssCompressorFilter(
            '/usr/share/yui-compressor/yui-compressor.jar'
        ));

        $fm->set('yui_js', new Yui\JsCompressorFilter(
            '/usr/share/yui-compressor/yui-compressor.jar'
        ));

        // CSS
        if (isset($app['cf.source.css'])) {
            $styles = new GlobAsset($app['cf.source.css']); //, array($fm->get('yui_css')));
            $styles->setTargetPath('compiled/style.css');
            $am->set('styles', $styles);
        }

        // Javascript
        if (isset($app['cf.source.js'])) {
            $js = new GlobAsset($app['cf.source.js'], array($fm->get('yui_js')));
            $js->setTargetPath('compiled/script.js');
            $am->set('js', $js);
        }

        // Inidividual file assets
        if (isset($app['cf.assets'])) {
            $i = 0;
            $finder = new Finder;
            $paths = array();
            $baseDir = $app['cf.assets.base_dir'];
            foreach ($app['cf.assets'] as $path) {
                $paths[] = $path = realpath("$baseDir/$path");
                if (!$path) {
                    throw new \InvalidArgumentException("Directory not found: $baseDir/$path");
                }
            }
            $finder->files()
                ->ignoreVCS(true)
                ->filter(function($file) { return $file->getBasename() !== 'assets.version'; })
                ->in($paths);

            $delimiter = 'resources/public/';
            $len = strlen($delimiter);
            foreach ($finder as $file) {
                $realPath = $file->getRealPath();
                $path = substr($realPath, strrpos($realPath, $delimiter) + $len );
                $name = 'asset_' . ++$i;

                $asset = new FileAsset($realPath);
                $asset->setTargetPath($path);
                $am->set($name, $asset);
            }
        }

        return $am;
    }


    public function boot(Application $app)
    {
    }
}

class AssetNotVersionedException extends \Exception {}
