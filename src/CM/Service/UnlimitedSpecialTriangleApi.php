<?php

namespace CM\Service;

use Silex\Application;
use SoapClient;

class UnlimitedSpecialTriangleApi extends SoapClient
{

    private $app;

    function __construct(Application $app)
    {
        $this->app = $app;
        parent::__construct($this->app['config']['Triangle']['wsdl'], [
            'location' => $this->app['config']['Triangle']['url'],
            'trace' => true,
            'cache_wsdl' => 0,
            'soap_version' => SOAP_1_2
        ]);
    }
    
    public function packageOne($data, $address)
    {
        if(strlen($data['exp_year']) == 2){
            $data['exp_year'] = '20'.$data['exp_year'];
        }
        if(strlen($data['exp_month']) == 1){
            $data['exp_month'] = '0'.$data['exp_month'];
        }
        
        $conf = $this->app['config']['Triangle'];
        
        $var = [
            'username' => $conf['user'],
            'password' => $conf['pass'],
            'planID' => '20',
            'trialPackageID' => 8,
            'chargeForTrial' => true,
            'campaignID' => '',	
            'campaignIDSpecified' => false,
            'firstName' => $data['first_name'],
            'lastName' => $data['last_name'],
            'address1' => 'empty',
            'address2' => '',
            'city' => 'string',
            'state' => 'string',
            'zip' => 'empty',
            'country' => 'USA',
            'phone' => '',
            'email' => $data['emailAddress'],
            'sendConfirmationEmail' => true,
            'ip' => $this->app['request']->getClientIp(),
            'affiliate' => '',
            'subAffiliate' => '',
            //'internalID' => '',
            'prospectID' => $this->app['session']->get('prospectID'),
            'prospectIDSpecified' => true,
            'paymentType' => $data['card_type'],
            'paymentTypeSpecified' => true,
            'creditCard' => $data['card_number'],
            'cvv' => $data['cvv'],
            'expMonth' => $data['exp_month'],
            'expYear' => $data['exp_year'],
            'customField1' => $address
        ];
        
        $r = $this->createSubscriptionCustom($var);
        if ($r) {
            $r = $r->CreateSubscriptionCustomResult;
        }
        //ldd($this->__getLastRequest());
        //ldd($r);

        return $r;
    }
    
    public function packageTwo($data, $address)
    {
        if(strlen($data['exp_year']) == 2){
            $data['exp_year'] = '20'.$data['exp_year'];
        }
        if(strlen($data['exp_month']) == 1){
            $data['exp_month'] = '0'.$data['exp_month'];
        }
        
        $conf = $this->app['config']['Triangle'];
        
        $var = [
            'username' => $conf['user'],
            'password' => $conf['pass'],
            'planID' => '21',
            'trialPackageID' => 8,
            'chargeForTrial' => true,
            'campaignID' => '',	
            'campaignIDSpecified' => false,
            'firstName' => $data['first_name'],
            'lastName' => $data['last_name'],
            'address1' => 'empty',
            'address2' => '',
            'city' => 'string',
            'state' => 'string',
            'zip' => 'empty',
            'country' => 'USA',
            'phone' => '',
            'email' => $data['emailAddress'],
            'sendConfirmationEmail' => true,
            'ip' => $this->app['request']->getClientIp(),
            'affiliate' => '',
            'subAffiliate' => '',
            //'internalID' => '',
            'prospectID' => $this->app['session']->get('prospectID'),
            'prospectIDSpecified' => true,
            'paymentType' => $data['card_type'],
            'paymentTypeSpecified' => true,
            'creditCard' => $data['card_number'],
            'cvv' => $data['cvv'],
            'expMonth' => $data['exp_month'],
            'expYear' => $data['exp_year'],
            'customField1' => 'empty'
        ];
        
        $r = $this->createSubscriptionCustom($var);
        if ($r) {
            $r = $r->CreateSubscriptionCustomResult;
        }
        //ldd($this->__getLastRequest());
        //ldd($r);

        return $r;
    }
    
    public function packageThree($data, $address)
    {
        if(strlen($data['exp_year']) == 2){
            $data['exp_year'] = '20'.$data['exp_year'];
        }
        if(strlen($data['exp_month']) == 1){
            $data['exp_month'] = '0'.$data['exp_month'];
        }
        
        $conf = $this->app['config']['Triangle'];
        
        $var = [
            'username' => $conf['user'],
            'password' => $conf['pass'],
            'amount' => 9.95,
            'shipping' => 0,
            'shippingSpecified' => false,
            'productTypeID' => 1,
            'productTypeIDSpecified' => true,
            'productID' => 97,
            'productIDSpecified' => true,
            'campaignID' => '',	
            'campaignIDSpecified' => false,
            'firstName' => $data['first_name'],
            'lastName' => $data['last_name'],
            'address1' => 'empty',
            'address2' => '',
            'city' => 'empty',
            'state' => 'empty',
            'zip' => 'empty',
            'phone' => '',
            'email' => $data['emailAddress'],
            'ip' => $this->app['request']->getClientIp(),
            'affiliate' => '',
            'subAffiliate' => '',
            //'internalID' => '',
            'prospectID' => $this->app['session']->get('prospectID'),
            'paymentType' => $data['card_type'],
            'creditCard' => $data['card_number'],
            'cvv' => $data['cvv'],
            'expMonth' => $data['exp_month'],
            'expYear' => $data['exp_year'],
            'sendConfirmationEmail' => false,
            'customField1' => 'empty',
        ];
        
        $r = $this->charge($var);
        if ($r) {
            $r = $r->ChargeResult;
        }
        //ldd($this->__getLastRequest());
        //ldd($r);

        return $r;
    }
    
    public function setProspectID($data)
    {
        $prospectID = $this->app['session']->get('prospectID');
        if (!$prospectID) {
            $conf = $this->app['config']['Triangle'];
        
            $var = [
                'username' => $conf['user'],
                'password' => $conf['pass'],
                'firstName' => $data['first_name'],
                'lastName' => $data['last_name'],
                'address1' => 'string',
                'address2' => '',
                'city' => 'string',
                'state' => 'string',
                'zip' => 'empty',
                'country' => 'USA',
                'phone' => '',
                'email' => $data['emailAddress'],
                'ip' => $this->app['request']->getClientIp(),
                'affiliate' => '',
                'subAffiliate' => '',
                'internalID' => ''
            ];

            $r = $this->createProspect($var);
            if ($r->CreateProspectResult->State == "Success") {
                $prospectID = $r->CreateProspectResult->ReturnValue->ProspectID;
                $this->app['session']->set('prospectID', $prospectID);
            } else {
                $prospectID = false;
            }
        }
        
        return $prospectID;
    }
}