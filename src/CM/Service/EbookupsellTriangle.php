<?php

namespace CM\Service;

use Silex\Application;
use SoapClient;

class EbookupsellTriangle extends SoapClient
{

    private $app;

    function __construct(Application $app)
    {
        $this->app = $app;
        parent::__construct($this->app['config']['Triangle']['wsdl'], [
            'location' => $this->app['config']['Triangle']['url'],
            'trace' => true,
            'cache_wsdl' => 1,
            'soap_version' => SOAP_1_2
        ]);
    }
    
    public function ebookOrder($data)
    {
        if(strlen($data['ccExpYear']) == 2){
            $data['ccExpYear'] = '20'.$data['ccExpYear'];
        }
        if(strlen($data['ccExpMonth']) == 1){
            $data['ccExpMonth'] = '0'.$data['ccExpMonth'];
        }
        
        $conf = $this->app['config']['Triangle'];
        
        $var = [
            'username' => $conf['user'],
            'password' => $conf['pass'],
            'amount' => 7,
            'shipping' => 0,
            'shippingSpecified' => false,
            'productTypeID' => 9,
            'productTypeIDSpecified' => true,
            'productID' => 102,
            'productIDSpecified' => true,
            'campaignID' => '',	
            'campaignIDSpecified' => false,
            'firstName' => $data['firstName'],
            'lastName' => $data['lastName'],
            'address1' => 'not required',
            'address2' => '',
            'city' => 'not required',
            'state' => 'not required',
            'zip' => $data['postcode'],
            'phone' => '',
            'email' => $data['emailAddress'],
            'ip' => $this->app['request']->getClientIp(),
            'affiliate' => '',
            'subAffiliate' => '',
            //'internalID' => '',
            'prospectID' => $this->app['session']->get('prospectID'),
            'paymentType' => $data['ccType'],
            'creditCard' => $data['ccNumber'],
            'cvv' => $data['ccCVV'],
            'expMonth' => $data['ccExpMonth'],
            'expYear' => $data['ccExpYear'],
            'sendConfirmationEmail' => false
        ];
        
        $r = $this->charge($var);
        //$r = $this->__soapCall('charge', array('parameters'=>$var));
        if ($r) {
            $r = $r->ChargeResult;
        }
        //ldd($this->__getLastRequest());
        //ldd($r);

        return $r;
    }
    
    public function subscribe($data, $address)
    {
        if(strlen($data['ccExpYear']) == 2){
            $data['ccExpYear'] = '20'.$data['ccExpYear'];
        }
        if(strlen($data['ccExpMonth']) == 1){
            $data['ccExpMonth'] = '0'.$data['ccExpMonth'];
        }
        
        $conf = $this->app['config']['Triangle'];
        $var = [
            'username' => $conf['user'],
            'password' => $conf['pass'],
            'planID' => $conf['subscription_plan_id'],
            'trialPackageID' => $conf['trialPackageID'],
            'chargeForTrial' => true,
            'campaignID' => '',	
            'campaignIDSpecified' => false,
            'firstName' => $data['firstName'],
            'lastName' => $data['lastName'],
            'address1' => 'empty',
            'address2' => '',
            'city' => 'string',
            'state' => 'string',
            'zip' => $data['postcode'],
            'country' => 'USA',
            'phone' => '',
            'email' => $data['emailAddress'],
            'sendConfirmationEmail' => true,
            'ip' => $this->app['request']->getClientIp(),
            'affiliate' => '',
            'subAffiliate' => '',
            'internalID' => '',
            'prospectID' => $this->app['session']->get('prospectID'),
            'paymentType' => $data['ccType'],
            'creditCard' => $data['ccNumber'],
            'cvv' => $data['ccCVV'],
            'expMonth' => $data['ccExpMonth'],
            'expYear' => $data['ccExpYear'],
            'description' => 'order',
            'customField1' => $address,
        ];
        
        $r = $this->createSubscriptionCustom($var);
        
        if ($r) {
            $r = $r->CreateSubscriptionCustomResult;
        }
        //ldd($this->__getLastRequest());
        //ldd($r);

        return $r;
    }
    
    public function setProspectID($data)
    {
        $prospectID = $this->app['session']->get('prospectID');
        if (!$prospectID) {
            $conf = $this->app['config']['Triangle'];
        
            $var = [
                'username' => $conf['user'],
                'password' => $conf['pass'],
                'firstName' => $data['firstName'],
                'lastName' => $data['lastName'],
                'address1' => 'string',
                'address2' => '',
                'city' => 'string',
                'state' => 'string',
                'zip' => $data['postcode'],
                'country' => 'USA',
                'phone' => '',
                'email' => $data['emailAddress'],
                'ip' => $this->app['request']->getClientIp(),
                'affiliate' => '',
                'subAffiliate' => '',
                'internalID' => ''
            ];

            $r = $this->createProspect($var);
            if ($r->CreateProspectResult->State == "Success") {
                $prospectID = $r->CreateProspectResult->ReturnValue->ProspectID;
                $this->app['session']->set('prospectID', $prospectID);
            } else {
                $prospectID = false;
            }
        }
        
        return $prospectID;
    }
}