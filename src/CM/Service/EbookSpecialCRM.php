<?php

namespace CM\Service;

use Silex\Application;
use SoapClient;

class EbookSpecialCRM extends SoapClient
{

    private $app;

    function __construct(Application $app)
    {
        $this->app = $app;
        parent::__construct($this->app['config']['Triangle']['wsdl'], [
            'location' => $this->app['config']['Triangle']['url'],
            'trace' => true,
            'cache_wsdl' => 0,
            'soap_version' => SOAP_1_2
        ]);
    }
    
    public function buyEbook($data, $amount, $sendemail = false)
    {
        if(strlen($data['ccExpYear']) == 2){
            $data['ccExpYear'] = '20'.$data['ccExpYear'];
        }
        if(strlen($data['ccExpMonth']) == 1){
            $data['ccExpMonth'] = '0'.$data['ccExpMonth'];
        }
        
        $conf = $this->app['config']['Triangle'];
        
        $var = [
            'username' => $conf['user'],
            'password' => $conf['pass'],
            'amount' => $amount,
            'shipping' => 0,
            'shippingSpecified' => false,
            'productTypeID' => 9,
            'productTypeIDSpecified' => true,
            'productID' => 100,
            'productIDSpecified' => true,
            'campaignID' => '',	
            'campaignIDSpecified' => false,
            'firstName' => $data['firstName'],
            'lastName' => $data['lastName'],
            'address1' => 'empty',
            'address2' => '',
            'city' => 'City',
            'state' => 'CA',
            'zip' => $data['postcode'],
            'phone' => '',
            'email' => $data['emailAddress'],
            'ip' => $this->app['request']->getClientIp(),
            'affiliate' => '',
            'subAffiliate' => '',
            //'internalID' => '',
            'prospectID' => $this->app['session']->get('prospectID'),
            'paymentType' => $data['ccType'],
            'creditCard' => $data['ccNumber'],
            'cvv' => $data['ccCVV'],
            'expMonth' => $data['ccExpMonth'],
            'expYear' => $data['ccExpYear'],
            'sendConfirmationEmail' => $sendemail,
            'customField1' => $this->app['session']->get('searchString')
        ];
        
        $r = $this->charge($var);
        if ($r) {
            $r = $r->ChargeResult;
        }
        //ldd($this->__getLastRequest());
        //ldd($r);

        return $r;
    }
    
    public function isCCDupe($creditCard)
    {
        $conf = $this->app['config']['Triangle'];
        $var = [
            'creditCard' => $creditCard,
            'productID' => 100
            //'productID' => '92'
        ];
        
        $r = $this->IsCreditCardDupe($var);
        //var_dump($r);die();
        if ($r) {
            $r = $r->IsCreditCardDupeResult;
        }
        //ldd($this->__getLastRequest());
        //ldd($r);

        return $r;
    }
}