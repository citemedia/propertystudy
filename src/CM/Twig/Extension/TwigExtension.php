<?php
namespace CM\Twig\Extension;

class TwigExtension extends \Twig_Extension 
{
    private $app;
    private $options;

    function __construct(\Silex\Application $app, array $options = array())
    {
        $this->app = $app;
        $this->options = $options;
    }

    public function getFilters(){
        return array(
            'camelCaseToWords' => new \Twig_Filter_Method($this, 'camelCaseToWords'),
        );
    }
    public function getFunctions()
    {
        return array(
            'asset'    => new \Twig_Function_Method($this, 'asset'),
        );
    }

    public function asset($url) 
    {
        $assetDir = isset($this->options['asset.directory']) ? 
            $this->options['asset.directory'] : 
            $this->app['request']->getBasePath();

        return sprintf('%s/%s', $assetDir, ltrim($url, '/'));
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'twig_extension';
    }

    public function camelCaseToWords($str){
        $arr = preg_split('/(?=[A-Z])/',$str);
        $out = implode(' ', $arr);

        return str_replace(['A P N','F I P S'],['APN','FIPS'], $out);
    }
}