<?php

namespace CM\Form;

use CM\Form\Type\CCVFormType;
use CM\FormChoices;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Email;

class UnlimitedSpecialForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('product', 'choice', [
                'label' => 'Choose package',
                'choices' => [1 => '1', 2 => '2', 3 => '3'],
                'data' => 1,
                'expanded' => true,
                'required' => true,
                'attr' => ['class' => '', 'id' => ''],
                'constraints' => [new NotBlank()],
            ])
            ->add('first_name', 'text', [
                'label' => 'First Name',
                'required' => true,
                'attr' => ['class' => 'form-txtbox'],
                'constraints' => [new NotBlank()],
            ])
            ->add('last_name', 'text', [
                'label' => 'Last Name',
                'required' => true,
                'attr' => ['class' => 'form-txtbox'],
                'constraints' => [new NotBlank()],
            ])
            ->add('emailAddress', 'text', [
                'label' => 'Email',
                'constraints' => [new NotBlank(), new Email()],
                'required' => false,
                //'error_bubbling' => true
            ])
            ->add('card_number', 'text', [
                'label' => 'Card Number',
                'required' => true,
                'attr' => ['class' => 'form-txtbox card', 'id' => 'cc_number' ],
                'constraints' => [new NotBlank()],
            ])
            ->add('card_type', 'choice', [
                'choices' => $this->get_cc_types(),
                'required' => true,
                'label' => 'Card Type',
                'attr' => ['class' => 'form-txt-field'],
                'constraints' => [new NotBlank()],
            ])
            ->add('exp_month', 'choice', [
                'choices' => FormChoices::getMonths(),
                'label' => 'Expiration Date',
                'required' => true,
                'attr' => ['class' => 'form-txtbox-2', 'id' => 'fields_expmonth'],
                'constraints' => [new NotBlank()],
            ])
            ->add('exp_year', 'choice', [
                'choices' => FormChoices::getYears(),
                'required' => true,
                'attr' => ['class' => 'form-txtbox-3', 'id' => 'fields_expyear'],
                'constraints' => [new NotBlank()],
            ])
            ->add('cvv', new CCVFormType(), [
                'label' => 'CVV2 Code',
                'attr' => ['class' => 'form-txtbox-2 digits cvv', 'id' => 'cc_cvv'],
                'constraints' => [new NotBlank()],
            ])
            ->add('agree', 'checkbox', [
                'label' => '"I agree.."',
                'attr' => ['class' => 'form-txtbox-2'],
                'required' => true,
                'constraints' => [new NotBlank()],
            ])
            ->add('understand', 'checkbox', [
                'label' => '"I understand.."',
                'attr' => ['class' => 'form-txtbox-2'],
                'required' => true,
                'constraints' => [new NotBlank()],
            ])
            ;
    }

    private function get_cc_types()
    {
        return [
            '' => 'Choose Type',
            'visa' => 'Visa',
            'master' => 'MasterCard'
        ];
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'billingCountry' => 'US',
            'noCCType' => false,
            'area' => 'US',
            'type' => 0,
            'validation_groups' => [Constraint::DEFAULT_GROUP],
        ));

    }

    public function getName()
    {
        return 'UnlimitedSpecialForm';
    }
}
