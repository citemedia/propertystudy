<?php

namespace CM\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class EbookupsellForm extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder->add('email', 'text', [
        'constraints' => [new NotBlank(), new Email()],
        'required' => true,
    ])
    ;
  }

  public function getName()
  {
    return 'EbookupsellForm';
  }

}
