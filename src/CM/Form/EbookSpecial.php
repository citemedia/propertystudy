<?php

namespace CM\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

class EbookSpecial extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ebooks', 'choice', [
                'choices' => ['1' => 'yes', '2' => 'nothanks'],
                'label' => 'Ebooks',
                'data' => '1',
                'attr' => ['class' => ''],
                'constraints' => [new NotBlank()],
                'expanded' => true,
                'required' => true
            ])
            ->add('emailAddress', 'text', [
                'label' => 'Email:',
                'attr' => ['class' => ''],
                //'constraints' => [new NotBlank()],
                'required' => false
            ])
            ;
    }

    public function getName()
    {
        return 'EbookSpecial';
    }
}
