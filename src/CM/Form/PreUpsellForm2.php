<?php

namespace CM\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\NotBlank;

class PreUpsellForm2 extends AbstractType
{
    private $emailAddress;

    function __construct($emailAddress = null)
    {
        $this->emailAddress = $emailAddress;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('emailAddress', 'text', [
                'constraints' => [new NotBlank()],
                'required' => true,
                'data' => $this->emailAddress
            ])
            ->add('password', 'repeated', [
                'type' => 'password',
                'constraints' => [new NotBlank()],
                'required' => true
            ])
            ->add('terms', 'checkbox', [
                'constraints' => [new NotBlank(['message'=>"You must agree to the terms and conditions"])],
                'required' => true
            ])

        ;
    }

    public function getName()
    {
        return 'PreUpsellForm2';
    }
}
