<?php

namespace CM\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\NotBlank;

class MortgageForm extends AbstractType
{

    private $cost;

    private $percentDown;

    private $loanTypes;

    function __construct($cost = null, $loanTypes = null, $percentDown = null)
    {
        $this->cost = $cost;
        $this->loanTypes = $loanTypes;
        $this->percentDown = $percentDown;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cost', 'money', [
                'constraints' => [new NotBlank()],
                'required' => true,
                'currency' => false,
                'grouping' => true,
                'data' => $this->cost,
            ])
            ->add('percentDown', 'hidden', [
                'constraints' => [new NotBlank()],
                'required' => true,
                'data' => $this->percentDown,
                'attr' => [
                    'data-slider-min' => '0',
                    'data-slider-max' => '100',
                    'data-slider-step' => '1',
                    'data-slider-value' => $this->percentDown,
                    'data-slider-orientation' => 'horizontal',
                    'data-slider-selection' => 'after',
                    'data-slider-tooltip' => 'hide'
                ]
            ])
            ->add('loanType', 'choice', [
                'choices' => $this->loanTypes,
                'required' => true
            ]);
    }

    public function getName()
    {
        return 'UpsellForm';
    }
}
