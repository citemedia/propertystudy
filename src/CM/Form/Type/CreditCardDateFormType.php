<?php
namespace CM\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CreditCardDateFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('month', 'choice', array(
            'choices' => $this->getMonthChoices(),
            'required' => true,
            'attr'=> array('class'=>'CCMonth')
        ))
            ->add('year', 'choice', array(
            'choices' => $this->getYearChoices(),
            'required' => true,
            'attr'=> array('class'=>'CCYear')
        ))
        ;
    }

    public function getParent()
    {
        return 'field';
    }

    public function getName()
    {
        return 'creditcard_date';
    }

    protected function getMonthChoices()
    {
        return array(
            '01' => '01 / Jan',
            '02' => '02 / Feb',
            '03' => '03 / Mar',
            '04' => '04 / Apr',
            '05' => '05 / May',
            '06' => '06 / Jun',
            '07' => '07 / Jul',
            '08' => '08 / Aug',
            '09' => '09 / Sep',
            '10' => '10 / Oct',
            '11' => '11 / Nov',
            '12' => '12 / Dec'
        );
    }

    protected function getYearChoices($length = 10)
    {
        $year = date('Y');
        $short = date('y');

        return array_combine(
            range($short, $short + $length),
            range($year, $year + $length)
        );
    }
}
