<?php

namespace CM\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class MakePasswordForm extends AbstractType
{
    const NAME = 'MakePasswordForm';
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('password', 'text', [
                'constraints' => [new NotBlank()],
                'required' => false,
                //'error_bubbling' => true
            ]);
    }

    public function getName()
    {
        return MakePasswordForm::NAME;
    }
}
