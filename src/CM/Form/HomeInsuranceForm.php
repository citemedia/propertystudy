<?php

namespace CM\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class HomeInsuranceForm extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder->add('zipcode', 'text', [
        'constraints' => [new CM\Form\Zipcode()],
        'required' => true,
    ])
    ;
  }

  public function getName()
  {
    return 'HomeInsuranceForm';
  }

}
