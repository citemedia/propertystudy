<?php

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGenerator;

$app->match('/userstats', function (Request $request) use ($app) {
    return userstats($app, $request, []);
})->bind('userstats');

function userstats($app, $request, $options)
{
    if (!$app['security']->isGranted('ROLE_USER')) {
        return $app->redirect($app['url_generator']->generate('login'));
    }
    
    $now = time();
    $SevenDaysAgo = $now - 604800;
    $nowDate = new \DateTime();
    $nowDate->setTimestamp($now);
    $SevenDaysAgoDate = new \DateTime();
    $SevenDaysAgoDate->setTimestamp($SevenDaysAgo);
    
    $nd = $nowDate->format('Y-m-d');
    $SevenD = $SevenDaysAgoDate->format('Y-m-d');
    
    $stmt = $app['db']->executeQuery("SELECT COUNT(*) FROM users WHERE DATE(last_login) BETWEEN '$SevenD' AND '$nd';");
    //$stmt = $app['db']->executeQuery("SELECT COUNT(*) FROM users WHERE DATE(last_login) BETWEEN '2014-05-29' AND '2014-06-05';");
    //ldd($stmt->fetch());
    $num_of_logins = $stmt->fetch()['COUNT(*)'];

    /* $stmt = $app['db']->executeQuery("SELECT COUNT(*) FROM users WHERE DATE(last_visit) BETWEEN '$SevenD' AND '$nd';"); */
    /* $num_of_visits = $stmt->fetch()['COUNT(*)']; */
    
    return $app['twig']->render('userstats/userstats.html.twig', [
        'options' => ['nextUrl'=>'homepage1'],
        'num_of_logins' => $num_of_logins
    ]);
}