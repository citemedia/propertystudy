<?php

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Database\TriangleFailures;

$app['twig'] = $app->share($app->extend('twig', function (\Twig_Environment $twig, $app) {
    $twig->addExtension(new \Bangpound\Silex\LadybugExtension($app));
    return $twig;
}));

$app->match('/crm-failures', function(Request $request) use ($app) {
    return crm_failures($app, $request, ['nextUrl' => 'homepage1']);
})->bind('crm-failures');

$app->match('/crm-failure/{id}', function($id, Request $request) use ($app) {
    return crm_failure($id, $app, $request, ['nextUrl' => 'homepage1']);
})->bind('crm-failure');

function crm_failures(Application $app, Request $request, $options)
{
    $db = new TriangleFailures($app);
    $rows = $db->getRows();
    
    return $app['twig']->render('crm_failures/crm_failures.html.twig', ['options'=>$options, 'rows'=>$rows]);
}

function crm_failure($id, Application $app, Request $request, $options)
{
    $db = new TriangleFailures($app);
    $row = $db->getResponse($id);
    
    return $app['twig']->render('crm_failures/crm_failure.html.twig', ['options'=>$options, 'row'=>unserialize($row[0]), 'id'=>$id]);
}