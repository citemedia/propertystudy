<?php

use CM\Form\LoginForm;
use CM\Form\ForgotForm;
use CM\Form\MortgageForm;
use CM\Form\ReportMissingInfoForm;
use CM\Form\OrderForm;
use CM\Form\OrderForm2;
use CM\Form\SearchForm;
use CM\Form\TopSearchForm;
use CM\Service\TriangleApi;
use Silex\Application;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Api\ZillowApi\ZillowApi;
use Api\MelissaData\MelissaData;
use Api\GreatSchools\GreatSchoolsAPI;
use Api\PredatorBarrier\PredatorBarrier;
use Api\WalkScore\WalkScore;
use Api\Weather\Weather;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Api\Trulia\Trulia;

require "admin.php";
require "landings.php";
require "unlimited_special.php";
require "ebookspecial.php";
require "upsell_split3.php";
require "userstats.php";
require "member.php";
require "crm_failures.php";
require "chart_split.php"; // changed chart in the funnel
require "affiliate1.php";

$app->match('/{url}', function(Request $request) use ($app) {
    $pathInfo = $request->getPathInfo();
    $requestUri = $request->getRequestUri();

    $url = str_replace($pathInfo, rtrim($pathInfo, ' /'), $requestUri);

    return $app->redirect($url, 301);
})->bind('removeTrailingSlash')->assert('url', '.*/$');

$app->post('/log-error', function (Request $request) use ($app) {
    $rootUrl = $app['url_generator']->generate('homepage1', array(), UrlGenerator::NETWORK_PATH);

    if ($request->get('msg') && stristr($request->get('url'), $rootUrl)) {
        $app['monolog']->crit('JS ERROR: ' . $request->get('msg'), array(
            'extra' => array(
                'line' => $request->get('line'),
                'file' => $request->get('url')
            )
        ));
    }
    return new Response();
})->bind('log-error');

$app->match('/security-homebook', function(Request $request) use ($app) {
    $size = filesize("../web/ebook/HomeSecurityEbook.pdf");

    return $app->stream(function() {
        readfile("../web/ebook/HomeSecurityEbook.pdf");
    }, 200, array(
        'Content-Type' => 'application/pdf',
        'Content-length' => $size,
        'Content-Disposition' => 'attachment; filename="HomeSecurityEbook.pdf"',
        'Content-Transfer-Encoding' => 'binary'
    ));
})->bind('security-homebook');

$app->get('/searching', function (Request $request) use ($app) {
    return searching($app, $request, ['nextUrl' => 'requesting']);
})->bind('searching');

$app->get('/requesting', function (Request $request) use ($app) {
    return requesting($app, $request, ['nextUrl' => 'download']);
})->bind('requesting');

$app->match('/download', function (Request $request) use ($app) {
    return download($app, $request, ['nextUrl' => 'finished']);
})->bind('download');

$app->get('/finished', function (Request $request) use ($app) {
    return finished($app, $request, ['nextUrl' => 'checkout']);
})->bind('finished');

$app->match('/cost', function (Request $request) use ($app) {
    return cost($app, $request, ['nextUrl' => 'checkout', 'type' => 'cost']);
})->bind('cost');

$app->get('/costdiscount', function (Request $request) use ($app) {
    return cost($app, $request, ['nextUrl' => 'checkout', 'type' => 'costdiscount']);
})->bind('costdiscount');

$app->match('/checkout', function (Request $request) use ($app) {
    $s = $app['session'];

    if (in_array($s->get('funnel'), ['propertyrecord77', 'chart-split'])) {
        return checkout($app, $request, ['nextUrl' => 'specialoffer']);
    } elseif ($s->get('funnel') == 'affiliate1') {
        return checkout_affid($app, $request, ['nextUrl' => 'specialoffer']);
    } else {
        return checkout_sup($app, $request, ['nextUrl' => 'specialoffer']);
    }
    
})->bind('checkout')->requireHttps();

$app->match('/specialoffer', function (Request $request) use ($app) {
    return upsell($app, $request, ['template' => 'preupsell2.html.twig', 'form' => 'CM\Form\PreUpsellForm2'], 'crm_upsell2_product_id');
})->bind('specialoffer');

$app->match('/specialoffer2', function (Request $request) use ($app) {
    return upsell($app, $request, ['template' => 'upsell.html.twig', 'form' => 'CM\Form\UpsellForm']);
})->bind('specialoffer2');

$app->match('/activation', function (Request $request) use ($app) {
    return activation($app, $request);
})->bind('activation');

$app->post('/mortgage', function (Request $request) use ($app) {
    return mortgage($app, $request);
})->bind('mortgage');

$app->match('/customer-service', function (Request $request) use ($app) {
    return $app['twig']->render('customer_service.html.twig');
})->bind('customer-service');

$app->match('/faq', function (Request $request) use ($app) {
    return $app['twig']->render('faq.html.twig');
})->bind('faq');

$app->match('/terms', function (Request $request) use ($app) {
    return $app['twig']->render('terms.html.twig');
})->bind('terms');

$app->match('/legal', function (Request $request) use ($app) {
    return $app['twig']->render('legal.html.twig');
})->bind('legal');

$app->match('/privacy', function (Request $request) use ($app) {
    return $app['twig']->render('privacy.html.twig');
})->bind('privacy');

$app->get('/searching1', function (Request $request) use ($app) {
    return searching($app, $request, ['nextUrl' => 'requesting1']);
})->bind('searching1');

$app->get('/requesting1', function (Request $request) use ($app) {
    return requesting($app, $request, ['nextUrl' => 'download1']);
})->bind('requesting1');

$app->get('/download1', function (Request $request) use ($app) {
    return download($app, $request, ['nextUrl' => 'finished1']);
})->bind('download1');

$app->get('/finished1', function (Request $request) use ($app) {
    return finished($app, $request, ['nextUrl' => 'checkout1']);
})->bind('finished1');

$app->match('/checkout1', function (Request $request) use ($app) {
    return checkout($app, $request, ['nextUrl' => 'bonusoffer']);
})->bind('checkout1')->requireHttps();

$app->match('/bonusoffer', function (Request $request) use ($app) {
    return upsell($app, $request, ['template' => 'preupsell.html.twig', 'form' => 'CM\Form\Upsell2Form']);
})->bind('bonusoffer');

$app->match('/login', function (Request $request) use ($app) {
    return login($app, $request);
})->bind('login');

$app->match('/forgot-password', function (Request $request) use ($app) {
    return forgot($app, $request);
})->bind('forgot');

$app->get('/logout', function (Request $request) use ($app) {
    return logout($app, $request);
})->bind('logout');

$app->post('/report-missing-info', function (Request $request) use ($app) {
    return report_missing_info($app, $request);
})->bind('report-missing-info');

$app->post('/to-discount', function (Request $request) use ($app) {
    return to_discount($app, $request);
})->bind('to_discount');

$app->match('/checkout-discount', function (Request $request) use ($app) {
    return checkout_discount($app, $request, [
        'nextUrl' => 'upsella2'
    ]);
})->bind('checkout_discount');

$app->match('/warning', function (Request $request) use ($app) {
    return warning($app, $request, ['nextUrl' => 'select-payment']);
})->bind('warning');

$app->match('/declined', function (Request $request) use ($app) {
    return declined($app, ['nextUrl' => 'report']);
})->bind('declined');

$app->match('/select-payment', function (Request $request) use ($app) {
    return select_payment($app, $request, ['nextUrl' => 'new-or-existing']);
})->bind('select-payment');

$app->match('/new-or-existing', function (Request $request) use ($app) {
    return new_or_existing($app, $request, ['nextUrl' => 'feedback']);
})->bind('new-or-existing');

$app->match('/feedback', function (Request $request) use ($app) {
    return feedback($app, $request, ['nextUrl' => 'checkout']);
})->bind('feedback');

$app->match('/reportest', function (Request $request) use ($app) {
    return report($app, $request, ['nextUrl' => 'homepage', 'test'=>true]);
})->bind('reportp')->requireHttp();

$app->match('/report', function (Request $request) use ($app) {
    return report($app, $request, ['nextUrl' => 'homepage']);
})->bind('report')->requireHttp();

$app->match('/homeinsurance', function (Request $request) use ($app) {
    return homeinsurance($app, $request, []);
})->bind('homeinsurance')->requireHttp();

$app->match('/reportupgrade1', function (Request $request) use ($app) {
    return reportupgrade1($app, $request, []);
})->bind('reportupgrade1');
$app->match('/reportupgrade2', function (Request $request) use ($app) {
    return reportupgrade2($app, $request, []);
})->bind('reportupgrade2');
function reportupgrade1(Application $app, Request $request, $options)
{
    return $app['twig']->render("upsell_split/reportupgrade1.html.twig", []);
}
function reportupgrade2(Application $app, Request $request, $options)
{
    return $app['twig']->render("upsell_split/reportupgrade2.html.twig", []);
}

$app->match('/mortgage-rates', function (Request $request) use ($app) {
    return mortgage_rates($app, $request, []);
})->bind('mortgage_rates')->requireHttp();

$app->match('/upsell2bb', function (Request $request) use ($app) {
    return upsell2bb($app, $request, ['nextUrl' => 'requesting']);
})->bind('upsell2bb');

$app->match('/checkout1', function (Request $request) use ($app) {
    return checkout_upsell2bb($app, $request, ['nextUrl' => 'bonusoffer', 'template'=>'checkout3.html.twig']);
})->bind('checkout1')->requireHttps();

$app->match('/checkout2', function (Request $request) use ($app) {
    return checkout_upsell2bb($app, $request, ['nextUrl' => 'bonusoffer', 'template'=>'checkout2.html.twig']);
})->bind('checkout2')->requireHttps();

$app->match('/about', function (Request $request) use ($app) {
    return about($app, $request, ['nextUrl' => 'homepage1']);
})->bind('about');

function about(Application $app, Request $request, Array $options)
{    
    return $app['twig']->render('about.html.twig', ['options' => $options]);    
}

function checkout_upsell2bb(Application $app, Request $request, Array $options)
{
    if ($app['security']->isGranted('ROLE_USER')) {
        $app['session']->set('order', true);
        return $app->redirect('report');
    }

    if (!$app['session']->get('searchData')) {
        return redirectHome($app);
    }

    if ($app['session']->get('order')) {
        return $app->redirect($options['nextUrl']);
    }

    if ($app['session']->get('to_discount', false)) {
        return $app->redirect($app['url_generator']->generate('checkout_discount'));
    }

    $app['session']->set('checkout_discount', null);

    $form = $app['form.factory']
        ->createBuilder(new CM\Form\OrderForm2())
        ->getForm();

    if ($options['template'] == 'checkout3.html.twig') {
        $form->get('amount')->setData('1.20');
    } else if ($options['template'] == 'checkout2.html.twig') {
        $form->get('amount')->setData('19.95');
    }

    if ('POST' == $request->getMethod()) {
        $form->submit($request);

        if ($form->isValid()) {

            $data = $form->getData();
            $app['session']->set('checkout_data', $data);

            $api = new TriangleApi($app);
            // if successful, sets 'prospectID' session var
            $api->setProspectID($data);

            $result = $api->simpleOrder($data, $app['session']->get('searchString'));

            if ($result->State == 'Error') {
                return $app['twig']->render('checkout3.html.twig', ['order_form' => $form->createView(), 'error' => 'Something went wrong', 'options' => $options]);
            }

            $app['session']->set('customerData', $data);
            $app['session']->set('order', $result);

            return $app->redirect($options['nextUrl']); 
        }
    }
    return $app['twig']->render($options['template'], ['order_form' => $form->createView(), 'options' => $options]);
}

function upsell2bb(Application $app, Request $request, $options)
{
    if ('POST' == $request->getMethod()) {
    
        $amount = $request->get('price');
        $r = new RedirectResponse($app['url_generator']->generate($options['nextUrl']));        

        switch ($amount) {
        case '1.20':
            return $app->redirect('checkout1');
            break;
        case '19.95': 
            return $app->redirect('checkout2');
            break;
        default:
            var_dump('upsell2bb error');die();
            break;
        }

        return $r;
    }
    
    $html = $app['twig']->render('upsell_split/upsell2bb.html.twig', ['options' => $options]);
    $r = new Response($html);

    return $r;
}

function crm_fail($page, $result)
{
    global $app;
    $fails = new \Database\TriangleFailures($app);
    try {
        $fails->addRow($page, $app['session']->get('searchString'), $result);
    } catch (Exception $e) {}
}

function to_discount($app, $request)
{
    $app['session']->set('to_discount', true);
    return new Response();
}

function checkout_discount($app, $request, $options)
{
    $form = $app['form.factory']
        ->createBuilder(new OrderForm())
        ->getForm();

    $app['session']->set('checkout_discount', true);

    if ('POST' == $request->getMethod()) {
        $form->submit($request);

        if ($form->isValid()) {

            $data = $form->getData();
            $app['session']->set('checkout_data', $data);

            $api = new TriangleApi($app);
            // if successful, sets 'prospectID' session var
            $api->setProspectID($data);
            $result = $api->simpleOrderCheckoutDiscount($data, $app['session']->get('searchString'));

            if ($result->State == 'Error') {
                return $app['twig']->render('checkout_discount.html.twig', ['order_form' => $form->createView(), 'error' => 'Something went wrong', 'options' => $options]);
            }

            $app['session']->set('to_discount', false);
            $app['session']->set('customerData', $data);
            $app['session']->set('order', $result);

            return $app->redirect($app['url_generator']->generate($options['nextUrl']));
        }
    }

    return $app['twig']->render('checkout_discount.html.twig', ['order_form' => $form->createView(), 'options' => $options]);
}

function redirectHome(Application $app)
{
    $session = $app['session'];

    if ($funnel = $session->get('funnel')) {
        if ($funnel == 'propertyrecord77') {
            return $app->redirect($app['url_generator']->generate('homepage'));
        } else {
            return $app->redirect($app['url_generator']->generate('homepage1'));
        }
    } else {
        return $app->redirect($app['url_generator']->generate('homepage1'));
    }
}

function login($app, $request)
{
    $form = $app['form.factory']
        ->createBuilder(new LoginForm())
        ->getForm();

    // get the login error if there is one
    if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
        $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
    } else {
        $error = $app['session']->get(SecurityContext::AUTHENTICATION_ERROR);
        $app['session']->remove(SecurityContext::AUTHENTICATION_ERROR);
    }
    if ($error) {
        $error = $error->getMessage();
    }

    return $app['twig']->render('login.html.twig', array(
        'form' => $form->createView(),
        'error' => $error,
        'last_username' => $app['session']->get(SecurityContext::LAST_USERNAME),
    ));
}

function forgot($app, $request)
{
    if ($app['security']->isGranted('ROLE_USER') && !($app['security']->isGranted('ROLE_ADMIN') || $app['security']->isGranted('ROLE_SUPPORT'))) {
        return $app->redirect($app['url_generator']->generate('homepage1'));
    }

    $form = $app['form.factory']
        ->createBuilder(new ForgotForm())
        ->getForm();

    $error = '';

    if ('POST' == $request->getMethod()) {

        $form->submit($request);

        if ($form->isValid()) {
            $data = $form->getData();

            $support = false;
            $to_email = $data['email'];
            if ($app['security']->isGranted('ROLE_ADMIN') || $app['security']->isGranted('ROLE_SUPPORT')) {
                $support = true;
                $to_email = $app['config']['site_email'];
            }

            try {
                $found_user = $app['security.user_provider.subscribed']->loadUserByUsername($data['email']);
                // Send email to the user:
                $message = \Swift_Message::newInstance()
                    ->setSubject('propertystudy.org forgot password.')
                    ->setFrom(array($app['config']['site_email']))
                    ->setTo(array($to_email));
                if ($support) {
                    $message->setBody("propertystudy.org<br/><br/>username: {$found_user->getUsername()}<br/>password: " . $found_user->getPassword(), 'text/html');
                } else {
                    $message->setBody('propertystudy.org<br/><br/>password: ' . $found_user->getPassword(), 'text/html');
                }

                $app['mailer']->send($message);

                return $app->redirect($app['url_generator']->generate('homepage1'));
            } catch (UsernameNotFoundException $ex) {
                return $app['twig']->render('forgot.html.twig', [
                    'form' => $form->createView(),
                    'error' => 'Email not found.'
                ]);
            } catch (\Swift_RfcComplianceException $e) {
                $error = 'Invalid email address.';
            }
        }
    }

    return $app['twig']->render(
        'forgot.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]
    );
}

function searching(Application $app, Request $request, Array $options)
{
    $s = $app['session'];
    if (!$s->get('searchData')) {
        return redirectHome($app);
    }

    if ($s->get('funnel') == 'propertyrecord77' || $s->get('funnel') == 'affiliate1') {
        return $app['twig']->render('split/searching.html.twig', ['options' => $options]);
    } else {
        return $app['twig']->render('searching.html.twig', ['options' => $options]);
    }
}

function requesting(Application $app, Request $request, Array $options)
{
    $s = $app['session'];
    if (!$s->get('searchData')) {
        return redirectHome($app);
    }

    if ($s->get('funnel') == 'propertyrecord77' || $s->get('funnel') == 'affiliate1') {
        return $app['twig']->render('split/requesting_to_download.html.twig', ['options' => $options]);
    } else {
        return $app['twig']->render('requesting_to_download.html.twig', ['options' => $options]);
    }
}

function download(Application $app, Request $request, Array $options)
{
    $s = $app['session'];
    if (!$s->get('searchData')) {
        return redirectHome($app);
    }

    if ($s->get('funnel') == 'propertyrecord77' || $s->get('funnel') == 'affiliate1') {
        return $app['twig']->render('split/download.html.twig', ['options' => $options]);
    } else {
        return $app['twig']->render('download.html.twig', ['options' => $options]);
    }
}

function finished(Application $app, Request $request, Array $options)
{
    $s = $app['session'];
    if (!$s->get('searchData')) {
        return redirectHome($app);
    }

    if ($s->get('funnel') == 'propertyrecord77' || $s->get('funnel') == 'affiliate1') {
        $options['nextUrl'] = 'cost';
        return $app['twig']->render('split/finished_downloading.html.twig', ['options' => $options]);
    } else {
        return $app['twig']->render('finished_downloading.html.twig', ['options' => $options]);
    }
}

// cost testing code

function cost(Application $app, Request $request, Array $options)
{
    $s = $app['session'];
    if ($app['security']->isGranted('ROLE_USER')) {
        $s->set('order', true);
        return $app->redirect('report');
    }

    if ('POST' == $request->getMethod() && ($s->get('funnel') == 'propertyrecord77' || $s->get('funnel') == 'affiliate1')) {
        return $app->redirect('/warning');
    }

    $searchData = $s->get('searchData');

    if ($s->get('funnel') == 'propertyrecord77' || $s->get('funnel') == 'affiliate1') {
        $options['nextUrl'] = 'warning';
        return $app['twig']->render('split/cost_of_report.html.twig', ['options' => $options, 'zip' => $searchData['zip']]);
    } else {
        return $app['twig']->render('cost_of_report.html.twig', ['options' => $options, 'zip' => $searchData['zip']]);
    }
}

function checkout_sup(Application $app, Request $request, Array $options)
{
    if ($app['security']->isGranted('ROLE_USER')) {
        $app['session']->set('order', true);
        return $app->redirect('report');
    }

    if (!$app['session']->get('searchData')) {
        return redirectHome($app);
    }

    if ($app['session']->get('order')) {
        return $app->redirect($options['nextUrl']);
    }
    
    $app['session']->set('checkout_discount', null);
    
    $form = $app['form.factory']
        ->createBuilder(new OrderForm2())
        ->getForm();

    if ('POST' == $request->getMethod()) {

        $form->submit($request);

        if ($form->isValid()) {

            $data = $form->getData();
            $app['session']->set('checkout_data', $data);

            $api = new TriangleApi($app);
            // if successful, sets 'prospectID' session var
            $api->setProspectID($data);
            $result = $api->simpleOrder($data, $app['session']->get('searchString'));

            if ($result->State == 'Error') {
                return $app['twig']->render('checkout3.html.twig', ['order_form' => $form->createView(), 'error' => 'Something went wrong', 'options' => $options]);
            }

            $app['session']->set('customerData', $data);
            $app['session']->set('order', $result);

            return $app->redirect($options['nextUrl']);
        }
    }

    return $app['twig']->render('checkout3.html.twig', ['order_form' => $form->createView(), 'options' => $options]);
}

function checkout(Application $app, Request $request, Array $options)
{
    $s = $app['session'];
    if ($app['security']->isGranted('ROLE_USER')) {
        $s->set('order', true);
        return $app->redirect('report');
    }
    if (!$s->get('searchData')) {
        return redirectHome($app);
    }
    if ($s->get('order')) {
        return $app->redirect($options['nextUrl']);
    }
    
    $s->set('checkout_discount', null);
    
    $form = $app['form.factory']->createBuilder(new OrderForm())->getForm();

    if ('POST' == $request->getMethod()) {

        $form->submit($request);

        if ($form->isValid()) {

            $data = $form->getData();
            $s->set('checkout_data', $data);

            $api = new TriangleApi($app);
            // if successful, sets 'prospectID' session var
            $api->setProspectID($data);
            $result = $api->simpleOrder($data, $s->get('searchString'));

            if ($result->State == 'Error') {

                crm_fail('/checkout', $result);
                    
                return $app['twig']->render('checkout.html.twig', ['order_form' => $form->createView(), 'error' => 'Something went wrong', 'options' => $options]);
            }

            $s->set('customerData', $data);
            $s->set('order', $result);
            $s->set('upsell_decline', false);

            //return $app->redirect($options['nextUrl']);
            //return $app->redirect('upsella.1');
            if ($s->get('landing') == 'emailcamp') {
                $s->set('landing', null);
                return $app->redirect('report');
            }
            return $app->redirect('upsella.2');
        }
    }

    return $app['twig']->render('checkout.html.twig', ['order_form' => $form->createView(), 'options' => $options]);
}

function upsell(Application $app, Request $request, Array $options)
{
    $s = $app['session'];
    if ($app['security']->isGranted('ROLE_USER')) {
        $s->set('order', true);
        return $app->redirect('report');
    }

    /* if (!$s->get('order')) { */
    /*     return $app->redirect('checkout'); */
    /* } */

    /* if ($s->get('upsellOrder')) { */
    /*     return $app->redirect('report'); */
    /* } */

    $data = $s->get('customerData');

//    if ($data) {
    $form = $app['form.factory']->createBuilder(new $options['form']($data['emailAddress']))->getForm();

    if ('POST' == $request->getMethod()) {

        $form->submit($request);

        if ($form->isValid()) {

            $api = new TriangleApi($app);
            $data = array_merge($data, $form->getData());

            if (!$api->isCCDupe($data['ccNumber'])) {

                $result = $api->subscribe($data, $s->get('searchString'));

                if ($result->State == 'Error') {
                   return $app['twig']->render($options['template'], [
                        'upsell_form' => $form->createView()
                    ]);
                }

                $prospectId = $s->get('prospectID');
                $app['security.user_provider.subscribed']->createUser($data, $prospectId);
                $app['session']->set('upsellOrder', $result);
            }

            return $app->redirect('activation');
        }
    }

    return $app['twig']->render($options['template'], ['upsell_form' => $form->createView()]);
//    }

    return $app['twig']->render($options['template'], ['upsell_form' => $form->createView()]);
    //return $app->redirect('report');
}

function activation(Application $app, Request $request)
{
    if (!$app['session']->get('searchData')) {
        return redirectHome($app);
    }

    /* if (!$app['session']->get('upsellOrder')) { */
    /*     return redirectHome($app); */
    /* } */

    $next = $app['url_generator']->generate('report');
    
    return $app['twig']->render('activation.html.twig', ['next'=>$next]);
}

function parseMelissaData($data)
{
    if (!setlocale(LC_MONETARY, 'en_US')) {
        setlocale(LC_MONETARY, 'en_US.UTF-8');
    }
    foreach ((array) $data->Record->Values as $k => $v) {
        if (stristr($k, 'code') || stristr($k, 'year'))
            continue;
        $data->Record->Values->$k = str_replace('USD ', '$', money_format('%i', (double) $v));
    }
    $data->Record->CurrentSale->RecordingDate = date('Y/m/d', strtotime($data->Record->CurrentSale->RecordingDate));
    $data->Record->CurrentSale->SaleDate = date('Y/m/d', strtotime($data->Record->CurrentSale->SaleDate));
    $data->Record->CurrentSale->MortgageDate = date('Y/m/d', strtotime($data->Record->CurrentSale->MortgageDate));
    $data->Record->PriorSale->RecordingDate = date('Y/m/d', strtotime($data->Record->PriorSale->RecordingDate));
    $data->Record->PriorSale->SaleDate = date('Y/m/d', strtotime($data->Record->PriorSale->SaleDate));

    $data->Record->CurrentSale->SalePrice = str_replace('USD ', '$', money_format('%i', (double) $data->Record->CurrentSale->SalePrice));
    $data->Record->PriorSale->SalePrice = str_replace('USD ', '$', money_format('%i', (double) $data->Record->PriorSale->SalePrice));
    $data->Record->PriorSale->MortgageAmount = str_replace('USD ', '$', money_format('%i', (double) $data->Record->PriorSale->MortgageAmount));

    $suf = ' sq ft';
    $data->Record->Lot->SquareFootage = number_format((int) $data->Record->Lot->SquareFootage) . $suf;
    $data->Record->SquareFootage->UniversalBuilding = number_format((int) $data->Record->SquareFootage->UniversalBuilding) . $suf;
    $data->Record->SquareFootage->BuildingArea = number_format((int) $data->Record->SquareFootage->BuildingArea) . $suf;
    $data->Record->SquareFootage->LivingSpace = number_format((int) $data->Record->SquareFootage->LivingSpace) . $suf;

    $garageCodes = [
        '001' => 'Undefined Type',
        '002' => 'Undefined Type – 2 Car',
        '003' => 'Undefined Type – 3 Car',
        '004' => 'Undefined Type – 4 Car',
        '005' => 'Undefined Type – 5 Car',
        '006' => 'Undefined Type – 6 Car',
        '010' => 'Attached Garage/Carport',
        '020' => 'Attached Basement',
        '030' => 'Detached Basement',
        '040' => 'Detached Garage/Carport',
        '050' => 'Enclosed Brick Garage/Carport',
        '060' => 'Basement Finished',
        '061' => 'Finished Basement – 1 Car',
        '062' => 'Finished Basement – 2 Car',
        '063' => 'Finished Basement – 3 Car',
        '064' => 'Finished Basement – 4 Car',
        '070' => 'Finished Built In',
        '080' => 'Unfinished Basement',
        '081' => 'Unfinished Basement – 1 Car',
        '082' => 'Unfinished Basement – 2 Car',
        '083' => 'Unfinished Basement – 3 Car',
        '084' => 'Unfinished Basement – 4 Car',
        '090' => 'Unfinished Built In'
    ];
    if (array_key_exists((string) $data->Record->Building->GarageCode, $garageCodes)) {
        $data->Record->Building->GarageType = $garageCodes[$data->Record->Building->GarageCode];
    } else {
        $data->Record->Building->GarageType = 'Undefined Type';
    }

    $parkingCode = [
        '001' => 'Type Unknown',
        '002' => 'Undefined Type – 2 Car Garage',
        '003' => 'Undefined Type – 3 Car Garage',
        '004' => 'Undefined Type – 4 Car Garage',
        '005' => 'Undefined Type – 5 Car Garage',
        '006' => 'Undefined Type – 6 Car Garage',
        '010' => 'Attached Garage/Carport',
        '020' => 'Attached Basement Garage',
        '030' => 'Detached Basement Garage',
        '040' => 'Detached Garage/Carport',
        '050' => 'Enclosed Brick Garage/Carport',
        '060' => 'Basement Finished Garage',
        '061' => 'Finished Basement – 1 Car Garage',
        '062' => 'Finished Basement – 2 Car Garage',
        '063' => 'Finished Basement – 3 Car Garage',
        '064' => 'Finished Basement – 4 Car Garage',
        '070' => 'Finished Built In Garage',
        '080' => 'Unfinished Basement Garage',
        '081' => 'Unfinished Basement – 1 Car Garage',
        '082' => 'Unfinished Basement – 2 Car Garage',
        '083' => 'Unfinished Basement – 3 Car Garage',
        '084' => 'Unfinished Basement – 4 Car Garage',
        '090' => 'Unfinished Built In Garage',
        '100' => 'Prefab Garage',
        '110' => 'Basement Garage',
        '111' => 'Basement – 1 Car Garage',
        '112' => 'Basement – 2 Car Garage',
        '113' => 'Basement – 3 Car Garage',
        '114' => 'Basement – 4 Car Garage',
        '115' => 'Basement – 5 Car Garage',
        '116' => 'Basement – 6 Car Garage',
        '120' => 'Built-In Garage',
        '121' => 'Built-In – 1 Car Garage',
        '122' => 'Built-In – 2 Car Garage',
        '130' => 'Built Under Garage',
        '140' => 'Garage/Carport',
        '150' => 'Cinderblock Garage',
        '160' => 'Asbestos Garage',
        '200' => 'Finished Attached Masonry Garage',
        '210' => 'Unfinished Attached Masonry Garage'
    ];

    if (array_key_exists((string) $data->Record->Building->ParkingCode, $parkingCode)) {
        $data->Record->Building->ParkingType = $parkingCode[(string) $data->Record->Building->ParkingCode];
    } else {
        $data->Record->Building->ParkingType = 'Type Unknown';
    }

    return $data;
}

function mortgage(Application $app, Request $request)
{
    /** @var Form $form */
    $mortgageForm = $app['form.factory']
        ->createBuilder(new MortgageForm(null, range(0, 2)))
        ->getForm();

    $session = $app['session'];
    $property = $session->get('property');
    $zillow = new ZillowApi($app['zwsid']);


    $mortgageForm->bind($request);

    if ($mortgageForm->isValid()) {

        $data = $mortgageForm->getData();

        $mortgageXml = $zillow->GetMonthlyPayments([
            'zpid' => $property->zpid,
            'price' => $data['cost'],
            'down' => $data['percentDown'],
            'zip' => $property->address->zipcode
        ]);

        $response = new Response();

        if ($mortgageXml->xpath('response')) {

            $mortgage = json_decode(json_encode($mortgageXml->xpath('response')[0]));

            if ($mortgage) {

                $app['mortgage'] = $mortgage;
                $app['mortgage']->payment = $mortgage->payment[$data['loanType']];

                $response = new Response(json_encode($app['mortgage']));
            }
        }

        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    return invalidFormResponse($app, $mortgageForm);
}

function report_missing_info(Application $app, Request $request)
{
    /** @var Form $form */
    $reportMissingInfoForm = $app['form.factory']
        ->createBuilder(new ReportMissingInfoForm())
        ->getForm();

    $reportMissingInfoForm->submit($request);

    $result = [];
    if ($reportMissingInfoForm->isValid()) {

        try {
            $message = \Swift_Message::newInstance()
                ->setSubject('propertystudy.org report missing info.')
                ->setFrom(array($app['session']->get('customerData')['emailAddress']))
                ->setTo(array('contact@propertystudy.org'))
                ->setBody($reportMissingInfoForm->getData()['message']);

            $app['mailer']->send($message);
        } catch (Exception $e) {
            if (get_class($e) === 'Swift_RfcComplianceException') {
                $result[] = ['key' => 'Email', 'val' => 'Invalid email address.'];
            }
        }
        $response = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');
    } else {
        foreach ($reportMissingInfoForm->all() as $key => $child) {
            if ($child->getErrors()) {
                $result[] = ['key' => ucfirst($key), 'val' => $child->getErrors()[0]->getMessage()];
            }
        }

        $response = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');
    }

    return $response;
}

function invalidFormResponse(Application $app, FormInterface $form, $formTmpl = null)
{
    $result = ['errors' => []];

    foreach ($form->getErrors() as $error) {
        $result['errors']['form'][] = $error->getMessage();
    }

    $result['errors'] += getChildErrors($form);

    if ($extra = $form->getExtraData()) {
        $result['errors']['extraData'] = $extra;
    }

    if ($formTmpl) {
        $result['rendered'] = $app['twig']->render($formTmpl, ['form' => $form->createView()])->getContent();
    }

    $response = new Response(json_encode($result), 400);
    $response->headers->set('Content-Type', 'application/json');
    return $response;
}

function getChildErrors(FormInterface $form)
{

    $errors = [];

    foreach ($form->all() as $name => $child) {

        foreach ($child->getErrors() as $error) {
            $errors[$name][] = $error->getMessage();
        }

        if ($childErrors = getChildErrors($child)) {
            $errors[$name] = $childErrors;
        }
    }

    return $errors;
}

function report(Application $app, Request $request, $options)
{
    $sess = $app['session'];

    $sess->set('referring_page', null); // clear upsella popup trigger
    
    if (!isset($options['test'])) {
        if (!$sess->get('searchData')) {
            return redirectHome($app);
        }

        if (!$sess->get('order') && !$app['security']->isGranted('ROLE_USER')) {
            return redirectHome($app);
        }
    }

    // upsell2a, upsell2b track
    if ($sess->get('upsell2a_price', false) || $sess->get('upsell2b_price', false)) {
        $db = new Splits\UpsellSplit\Database\UpsellSplit($app);
        $db->logVisitor($sess->get('upsell2a_price'), $sess->get('upsell2a_visit'), $sess->get('upsell2b_price'), $sess->get('upsell2b_visit'));
        $sess->set('upsell2a_price', null);
        $sess->set('upsell2a_visit', null);
        $sess->set('upsell2b_price', null);
        $sess->set('upsell2b_visit', null);
    }
    if ($sess->get('funnel', 'affiliate')) {
        $db = new Database\AffiliateTrack($app);
        $db->logVisitor();
        $sess->set('upsella2_price', null);
        $sess->set('upsella2_visit', null);
        $sess->set('upsella_price', null);
        $sess->set('upsella_visit', null);
    }

    $reportMissingInfoForm = $app['form.factory']->createBuilder(new CM\Form\ReportMissingInfoForm())->getForm();

    if ($app['security']->isGranted('ROLE_USER')) {
        $member_order = new Database\MemberOrders($app);
        $token = $app['security']->getToken();
        $user = $token->getUser();
        if ($report_row = $member_order->getRowByUserAndAddress($user->getUsername(), $sess->get('searchData'))) {
            $report_data = unserialize($report_row['report_data']);
            $report_data['reportMissingInfoForm'] = $reportMissingInfoForm->createView();
            if ($report_data['generic']) { $tmpl = 'report_steps_generic.html.twig'; }
            else $tmpl = 'report_steps.html.twig';
            return $app['twig']->render($tmpl, $report_data);
        }
    }

    $generic = true;
    $data = $sess->get('searchData', []);
    
    $data['city'] = isset($data['city']) ? $data['city'] : 'Las Vegas';
    $data['state'] = isset($data['state']) ? $data['state'] : 'NV';
    $data['zip'] = $data['zipcode'] = isset($data['zip']) ? $data['zip'] : '89107';
    $data['street'] = isset($data['street']) ? $data['street'] : '2500 Wimbledon';
    //$data['street'] = isset($data['street']) ? $data['street'] : '1210 First avenue';

    $info = [];

    $md = new Api\MelissaData\MelissaData2('106897455');
    $mdav = $md->getAddressValidation($data);
    $onboard = new Api\Onboardinformatics\Onboardinformatics($app);
    $zillow = new ZillowApi($app['zwsid']);
    //$trulia = new Trulia('7pu5qg9smvb57qrr296krr2g');
    //$tdata = $trulia->getZipCodeStats($data['zip']);
    //$tdata = $trulia->getZipCodeStats('98121');
    //var_dump( $tdata->response );die();

    //$dm = $zillow->GetDemographics(['zip'=>$data['zip']]);
    //var_dump( $dm->response->pages );die(); 
    
    if ($mdav->success) {
        $mprop = $md->getProperty((string)$mdav->response->Record->Address->AddressKey);
        if ($mprop->success) {
            //ldd($mprop->response->Record);
            $info['bedrooms'] = $mprop->response->Record->Building->BedRooms;
            $info['bathrooms'] = $mprop->response->Record->Building->TotalBaths;
            $info['year_built'] = (string)$mprop->response->Record->Building->YearBuilt;
            $info['stories'] = $mprop->response->Record->Building->Stories;
            $info['acreage'] = $mprop->response->Record->Lot->Acreage;
            $info['square_footage'] = $mprop->response->Record->SquareFootage->UniversalBuilding;
            $info['address'] = $mprop->response->Record->PropertyAddress->Address;
            $info['city'] = $mprop->response->Record->PropertyAddress->City;
            $info['state'] = $mprop->response->Record->PropertyAddress->State;
            $info['zip'] = $mprop->response->Record->PropertyAddress->Zip;
            $info['owner'] = $mprop->response->Record->Owner->Name;
            $info['tax_amount'] = $mprop->response->Record->Values->TaxAmount;
            $info['tax_year'] = $mprop->response->Record->Values->TaxYear;
            $generic = false;
        } else {
            $generic = true;
            
            /* $resp = $onboard->avm([ */
            /*     'street' => $data['street'], */
            /*     'city' => $data['city'], */
            /*     'state' => $data['state'], */
            /*     'zip' => $data['zip'] */
            /* ]); */
        }
    }

    setlocale(LC_MONETARY, 'en_US.utf8');
    
    $searchString = $sess->get('searchString') ? $sess->get('searchString') : "{$data['street']}, {$data['city']} {$data['state']}, {$data['zip']}";
    $geocode = $onboard->getGeocode($searchString);
    $loc = $geocode[0]->geometry->location;
    $WKTString = "POINT({$loc->lng} {$loc->lat})";
    $latlong = ['latitude' => $loc->lat, 'longitude' => $loc->lng];

    //$app['melissaData'] = ['zip' => $md->getFromZip(getZipfromGeocode($geocode[0]->address_components))];
    
    $areas = $onboard->getArea('Hierarchy/Lookup', ['WKTString' => $WKTString]);
    $geo_key = pickArea($areas); // AreaId
    $community = $onboard->getCommunityAreaFull(['AreaId' => $geo_key]);

    if (!$generic) {
        $homerangepercent = $mprop->response->Record->Values->CalculatedTotalValue / 100;
        $info['homerangemedian'] = str_replace('USD ', '', money_format('%i', (double)$mprop->response->Record->Values->CalculatedTotalValue));
        $info['homerangelow'] = str_replace('USD ', '', money_format('%i', (double)$homerangepercent * 70));
        $info['homerangehigh'] = str_replace('USD ', '', money_format('%i', (double)$homerangepercent * 130));
        
        if ((double)$info['bedrooms'] == 0.0) {$info['bedrooms'] = '';}
        if ((double)$info['bathrooms'] == 0.0) {$info['bathrooms'] = '';}
        if ((double)$info['stories'] == 0.0) {$info['stories'] = '';}
        if ((string)$info['square_footage'] == '') {$info['square_footage'] = '';}
        if (((int)date('Y') - 3) > (int)$info['tax_year']) {$info['tax_amount'] = '';}
        $owner_words = explode(' ', $info['owner']);
        $info['owner'] = '';
        foreach ($owner_words as $word) {
            $info['owner'] .= ' '.ucfirst(strtolower($word));
        }
        $info['owner'] = trim($info['owner']);
    } else {
        $homerangepercent = $community->medsaleprice / 100;
        $info['homerangemedian'] = str_replace('USD ', '', money_format('%i', (double)$community->medsaleprice));
        $info['homerangelow'] = str_replace('USD ', '', money_format('%i', (double)$homerangepercent * 70));
        $info['homerangehigh'] = str_replace('USD ', '', money_format('%i', (double)$homerangepercent * 130));
    }

    $info['medsaleprice'] = str_replace('USD ', '', money_format('%i', (double)$community->medsaleprice));
    $info['hmvcymort'] = str_replace('USD ', '', money_format('%i', (double)$community->hmvcymort));

    /* $ps = $onboard->getPropertySearch([ */
    /*     'TransactionType' => 'F0000', */
    /*     'Period' => 'Annual', */
    /*     'AreaId' => $geo_key */
    /* ]); */
    /* $property_search = array_pop($ps); */

    $app['weather'] = new Weather($data['state'], $data['city']);
    
    $greatschools_key = 'shmu5qtv08rquudifxsfggxx';

    $gs = new GreatSchoolsAPI($greatschools_key);
    $app['nearbySchools'] = $gs->nearbySchools($data['city'], $data['state']);

    if ($generic) { $tmpl = 'report_steps_generic.html.twig'; }
    else $tmpl = 'report_steps.html.twig';

    $report_data = [
        'options' => $options,
        'latlong' => $latlong,
        'firstName' => $sess->get('customerData')['firstName'],
        'emailAddress' => $sess->get('customerData')['emailAddress'],
        'searchString' => $searchString,
        'data' => $data,
        'info' => $info,
        'generic' => $generic,
        'community' => $community
    ];

    if ($app['security']->isGranted('ROLE_USER')) {
        $member_order = new Database\MemberOrders($app);
        $result = $member_order->addRow('/checkout', $report_data, '1');
    }

    $report_data['reportMissingInfoForm'] = $reportMissingInfoForm->createView();
    return $app['twig']->render($tmpl, $report_data);
}

function homeinsurance(Application $app, Request $request, $options)
{
    $params = [
        'bwapsstate' => '',
        'bwapcurrentlyinsured' => '',
        'bwapsagedriver' => '',
        'bwapsincidenthistory' => '',
        'bwapshomeowner' => '',
        'bwapsmultidriverhouseholds' => ''
    ];
    $params = array_merge($params, $request->query->all());
    //ldd($params);
     
    $zipcode = $request->query->get('ZipCode');
    return $app['twig']->render('homeinsurance.html.twig', [
        'bwapsstate' => $params['bwapsstate'], 
	    'bwapszip' => $zipcode,
        'bwapcurrentlyinsured' => $params['bwapcurrentlyinsured'],
        'bwapsagedriver' => $params['bwapsagedriver'],
        'bwapsincidenthistory' => $params['bwapsincidenthistory'],
        'bwapshomeowner' => $params['bwapshomeowner'],
        'bwapsmultidriverhouseholds' => $params['bwapsmultidriverhouseholds']
    ]);
}

function mortgage_rates($app, $request, $options)
{
    $zipcode = $request->get('zipcode');
    $loan_type = $request->get('loan_type');
    $credit_score = $request->get('credit_score');
    $property_type = $request->get('property_type');
    
    return $app['twig']->render("mortgage_rates.html.twig", [
        'zipcode'=>$zipcode,
        'loan_type'=>$loan_type,
        'credit_score'=>$credit_score,
        'property_type'=>$property_type
    ]);
}

function declined(Application $app, Array $options)
{
    $api = new TriangleApi($app);
    if (!$app['session']->get('upsell_decline', false)) {
        $data = $app['session']->get('checkout_data');
        $result = $api->simpleOrderCheckout43_2($data, $app['session']->get('searchString'));
        if ($result->State == 'Error') {
            //var_dump( 'decline error' );die();
            // error
        } else {
            $app['session']->set('upsell_decline', true);
        }
    }

    if ($app['session']->get('funnel') == 'propertyrecord77') {
        return $app->redirect($app['url_generator']->generate('ebookspecialintro'));
    }
    return $app->redirect($options['nextUrl']);
}

function pickArea($areas)
{
    $geo_key = '';
    foreach ($areas as $area) {
        if ($area->type == 'Zip Code') {
            $geo_key = $area->geo_key;
        }
    }
    if (!$geo_key) {
        foreach ($areas as $area) {
            if ($area->type == 'County') {
                $geo_key = $area->geo_key;
            }
        }
    }

    return $geo_key;
}

function feedback(Application $app, Request $request, Array $options)
{
    if (!$app['session']->get('searchData')) {
        return redirectHome($app);
    }

    if ('POST' == $request->getMethod()) {
        return $app->redirect($options['nextUrl']);
    }

    return $app['twig']->render('split/feedback.html.twig', ['options' => $options]);
}

function new_or_existing(Application $app, Request $request, Array $options)
{
    if (!$app['session']->get('searchData')) {
        return redirectHome($app);
    }

    if ('POST' == $request->getMethod()) {
        return $app->redirect($options['nextUrl']);
    }

    $app['session']->set('cardtype', $request->get('cardTypeR'));

    return $app['twig']->render('split/new_or_existing.html.twig', ['options' => $options]);
}

function select_payment(Application $app, Request $request, Array $options)
{
    if (!$app['session']->get('searchData')) {
        return redirectHome($app);
    }

    if ('POST' == $request->getMethod()) {
        return $app->redirect($options['nextUrl']);
    }

    return $app['twig']->render('split/select_payment.html.twig', ['options' => $options]);
}

function warning(Application $app, Request $request, Array $options)
{
    $s = $app['session'];
    if (!$s->get('searchData')) {
        return redirectHome($app);
    }

    if ('POST' == $request->getMethod()) {
        return $app->redirect($options['nextUrl']);
    }

    return $app['twig']->render('split/warning.html.twig', ['options' => $options]);
}

function getZipfromGeocode($addr_components) {
    foreach ($addr_components as $k=>$v) {
        if (in_array('postal_code', $v->types)) {
            return $v->long_name;
        }
    }
}
