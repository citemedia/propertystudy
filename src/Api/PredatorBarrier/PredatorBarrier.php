<?php
namespace Api\PredatorBarrier;

class PredatorBarrier
{
    private $url = "http://predatorbarrier.com/api/request.php";
    private $api_key;
    
    public function __construct($api_key)
    {
        $this->api_key = $api_key;
    }
    
    public function results($zip, $city='', $state='', $limit=1)
    {
        $url = $this->url."?key={$this->api_key}&action=search&output=xml&zip=".urlencode($zip)."&limit=".urlencode($limit);
        //ldd($url);
        $xml = $this->receive($url, 'results');
        return $xml;
    }
    
    public function receive($url, $method)
    {
        $timeout = 15;
        $fh = @fopen($url, "r");
        stream_set_timeout($fh, $timeout);
        $response = '';
        while (($buffer = fgets($fh, 4096)) !== false) { $response .= $buffer; }
        if (!feof($fh)) { trigger_error("PredatorBarrier API $method timeout, $timeout seconds.", E_USER_ERROR); }
        if ($response) {
            $result = simplexml_load_string($response);
        } else {
            $result = new SimpleXMLElement("<empty></empty>", 0, true);
        }
            
        return $result;
    }
}