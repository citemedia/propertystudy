<?php
namespace Api\MelissaData;
//https://property.melissadata.net/v3/REST/Service.svc/doLookup?id=106897455&t=RestTest&addressKey=92688211282&opt=1
//https://addresscheck.melissadata.net/v2/REST/Service.svc/doAddressCheck?id=106897455&opt=true&a1=22382%20Avenida%20%20Empresa&city=Rancho%20Santa%20Margarita&state=CA&zip=92688
class MelissaData2
{
    private $customerId;

    // Melissa validation codes
    
    private $success_codes = [
        'AS01' => ['Address Fully Verified', 'The address is valid and deliverable according to official postal agencies.'],
        'AS02' => ['Street Only Match', 'The street address was verified but the suite number is missing or invalid.'],
        'AS03' => ['Non USPS Address Match', 'US Only. This US address is not serviced by the USPS but does exist and may receive mail through third party carriers like UPS.'],
        'AS09' => ['Foreign Address', 'The address is in a non-supported country.'],
        'AS10' => ['CMRA Address', 'US Only. The address is a Commercial Mail Receiving Agency (CMRA) like a Mailboxes Ect. These addresses include a Private Mail Box (PMB or #) number.'],
        'AS13' => ['Address Updated By LACS', 'US Only. The address has been converted by LACSLink® from a rural-style address to a city-style address.'],
        'AS14' => ['Suite Appended', 'US Only. A suite was appended by SuiteLinkTM using the address and company name.'],
        'AS15' => ['Apartment Appended', 'An apartment number was appended by AddressPlus using the address and last name.'],
        'AS16' => ['Vacant Address', 'US Only. The address has been unoccupied for more than 90 days.'],
        'AS17' => ['No Mail Delivery', 'US Only. The address does not currently receive mail but will likely in the near future.'],
        'AS18' => ['DPV Locked Out', 'US Only. DPV processing was terminated due to the detection of what is determined to be an artificially created address.'],
        'AS20' => ['Deliverable only by USPS', 'US Only. This address can only receive mail delivered through the USPS (ie. PO Box or a military address).'],
        'AS23' => ['Extraneous Information', 'Extraneous information not used in verifying the address was found. This has been placed in the ParsedGarbage field.']
    ];

    private $error_codes = [
        'AE01' => ['Postal Code Error', 'The Postal Code does not exist and could not be determined by the city/municipality and state/ province.'],
        'AE02' => ['Unknown Street', 'Could not match the input street to a unique street name. Either no matches or too many matches found.'],
        'AE03' => ['Component Mismatch Error', 'The combination of directionals (N, E, SW, etc) and the suffix (AVE, ST, BLVD) is not correct and produced multiple possible matches.'],
        'AE04' => ['Non-Deliverable Address', 'US Only. A physical plot exists but is not a deliverable addresses. One example might be a railroad track or river running alongside this street, as they would prevent construction of homes in that location.'],
        'AE05' => ['Multiple Match', 'The address was matched to multiple records. There is not enough information available in the address to break the tie between multiple records.'],
        'AE06' => ['Early Warning System', 'US Only. This address currently cannot be verified but was identified by the Early Warning System (EWS) as containing new streets that might be confused with other existing streets.'],
        'AE07' => ['Missing Minimum Address', 'Minimum requirements for the address to be verified is not met. Address must have at least one address line and also the postal code or the locality/administrative area.'],
        'AE08' => ['Sub Premise Number Invalid', 'The thoroughfare (street address) was found but the sub premise (suite) was not valid.'],
        'AE09' => ['Sub Premise Number Missing', 'The thoroughfare (street address) was found but the sub premise (suite) was missing.'],
        'AE10' => ['Premise Number Invalid', 'The premise (house or building) number for the address is not valid.'],
        'AE11' => ['Premise Number Missing', 'The premise (house or building) number for the address is missing.'],
        'AE12' => ['Box Number Invalid', 'The PO (Post Office Box), RR (Rural Route), or HC (Highway Contract) Box numer is invalid.'],
        'AE13' => ['Box Number Missing', 'The PO (Post Office Box), RR (Rural Route), or HC (Highway Contract) Box number is missing.'],
        'AE14' => ['PMB Number Missing', 'US Only. The address is a Commercial Mail Receiving Agency (CMRA) and the Private Mail Box (PMB or #) number is missing.'],
        'AE17' => ['Sub Premise Not Required', 'A sub premise (suite) number was entered but the address does not have secondaries.']
    ];

    private $change_codes = [
        'AC01' => ['Postal Code Change', 'The postal code was changed or added.'],
        'AC02' => ['Administrative Area Change', 'The administrative area (state, province) was added or changed.'],
        'AC03' => ['Locality Change', 'The locality (city, municipality) name was added or changed.'],
        'AC04' => ['Alternate to Base Change', 'US Only. The address was found to be an alternate record and changed to the base (preferred) version.'],
        'AC05' => ['Alias Name Change', 'US Only. An alias is a common abbreviation for a long street name, such as “MLK Blvd” for “Martin Luther King Blvd.” This change code indicates that the full street name (preferred) has been substituted for the alias.'],
        'AC06' => ['Address1/Address2 Swap', 'Address1 was swapped with Address2 because Address1 could not be verified and Address2 could be verified.'],
        'AC07' => ['Address1 & Company Swapped', 'Address1 was swapped with Company because only Company had a valid address.'],
        'AC08' => ['Plus4 Change', 'US Only. A non-empty plus4 was changed.'],
        'AC09' => ['Dependent Locality Change', 'US Only. The dependent locality (urbanization) was changed.'],
        'AC10' => ['Thoroughfare Name Change', 'The thoroughfare (street) name was changed due to a spelling correction.'],
        'AC11' => ['Thoroughfare Suffix Change', 'The thoroughfare (street) suffix was added or changed, such as from "St" to "Rd."'],
        'AC12' => ['Thouroughfare Directional Change', 'The thoroughfare (street) pre-directional or post-directional was added or changed, such as from "N" to "NW."'],
        'AC13' => ['Sub Premise Type Change', 'The sub premise (suite) type was added or changed, such as from “STE” to “APT.”'],
        'AC14' => ['Sub Premise Number Change', 'The sub premise (suite) unit number was added or changed.']
    ];

    // Melissa property search codes

    private $property_success_codes = [
        'YS01' => ['FIPS/APN Match found'],
        'YS02' => ['AddressKey Match found'],
        'YS03' => ['Basic information returned'],
        'YS04' => ['Detailed information returned']
    ];

    private $property_error_codes = [
        'YE01' => ['No FIPS/APN or AddressKey provided'],
        'YE02' => ['No match found'],
        'YE03' => ['Invalid FIPS/APN or AddressKey provided']
    ];

    private $geocoding_success = [
        'GS01' => ['Geocoded to Zip+4 (9-digit ZIP Code)'],
        'GS02' => ['Geocoded to Zip+2 Centroid (7-digit ZIP Code)'],
        'GS03' => ['Geocoded to 5-digit Zip Code'],
        'GS05' => ['Geocoded to Rooftop level'],
        'GS06' => ['Geocoded to Interpolated Rooftop level']
    ];

    private $geocoding_error = [
        'GE02' => ['Zip Code not found']
    ];

    public function __construct($key)
    {
        $this->transfer_id = 'NDP'.time();
        $this->customerId = $key;
    }
    
    public function getFromZip($zip)
    {
        $url = "https://zipsearch.melissadata.net/v2/REST/Service.svc/doZipSearch?id={$this->customerId}&zip=".urlencode($zip);
        return simplexml_load_string(file_get_contents($url));
    }

    public function getAddressValidation(Array $property)
    {
        $url = "https://addresscheck.melissadata.net/v2/REST/Service.svc/doAddressCheck?id={$this->customerId}&t=".$this->transfer_id."&a1=".urlencode($property['street'])."&city=".urlencode($property['city'])."&state=".urlencode($property['state'])."&zip=".urlencode($property['zipcode']);
        $obj =  simplexml_load_string(file_get_contents($url));

        $result = (object)['success'=>false, 'response'=>$obj, 'codes'=>(object)['success'=>[], 'error'=>[], 'change'=>[]]];

        foreach (explode(',', $obj->Record->Results) as $s) {
            if (array_key_exists($s, $this->error_codes))   { $result->codes->error[$s]   = $this->error_codes[$s]; }
            if (array_key_exists($s, $this->success_codes)) { $result->codes->success[$s] = $this->success_codes[$s]; }
            if (array_key_exists($s, $this->change_codes))  { $result->codes->change[$s]  = $this->change_codes[$s]; }
        }

        if (!empty($result->codes->success)) {
            $result->success = true;
        } 
        
        return $result;
    }

    public function getProperty($addressKey)
    {
        $url = "https://property.melissadata.net/v3/REST/Service.svc/doLookup/?id={$this->customerId}&t=".$this->transfer_id."&addressKey=".$addressKey."&opt=1";
        $obj = simplexml_load_string(file_get_contents($url));

        $result = (object)['success'=>false, 'response'=>$obj, 'codes'=>(object)['success'=>[], 'error'=>[]]];

        foreach (explode(',', $obj->Record->Result->Code) as $s) {
            if (array_key_exists($s, $this->property_error_codes))   { $result->codes->error[$s]   = $this->property_error_codes[$s]; }
            if (array_key_exists($s, $this->property_success_codes)) { $result->codes->success[$s] = $this->property_success_codes[$s]; }
        }
        
        if (!empty($result->codes->success)) {
            $result->success = true;
        }
        
        return $result;
    }
    
}