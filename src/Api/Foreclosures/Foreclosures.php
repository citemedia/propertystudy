<?php
namespace Api\Foreclosures;

class Foreclosures
{
    private $url = "http://api.foreclosurelistings.com/foreclosure";
    private $api_key;
    
    public function __construct($api_key)
    {
        $this->api_key = $api_key;
    }
    
    public function getInfo($options)
    {
        $url = $this->url."?domain={$options['domain']}&key={$this->api_key}&state=".urlencode($options['state'])."&city=".urlencode($options['city']);
        $xml = $this->receive($url);
        return $xml;
    }
    
    public function receive($url)
    {
        $xml = simplexml_load_file($url);
        return $xml;
    }
}